﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Actor : MonoBehaviour
{
    //This script handles all the universal stuff with the characters within the game
    //from the stats to the basic emotions they all have 

    //who is in the scene
    public string actorName;
    //their given stats
    public int Body;
    public int Intellect;
    public int Charisma;
    public int Will;
    //Game controller
    public GameObject controller;
    public GameObject self;
    //Appointment number
    public int appointmentNum = 0;

    //stat text script
    SetStatText statTextSetter;
    //director script
    Director director;
    //dialogue handler
    DialogueHandler dialogueHandler;

    //gets Knot Checker
    InkKnotCheck knotChecker;

    //Character emotions for portrait manipulation.
    //As far as I know, all characters
    public Sprite neutral;
    public Sprite sad;
    public Sprite annoyed_or_frustrated;
    public Sprite displeased;
    public Sprite happy;

    //bool for testing: will get rid of this when I know for sure emotions work for a given actor
    public bool test;

    //bool for determining if actor has gotten a potion yet
    public bool haveIGottenMyPotionYet;

    //starting emotion
    public Image characterEmotion;
    public void Start()
    {
        

        appointmentNum += 1;
        actorName = this.name;
        if (test == false)
        {
            if (statTextSetter == null)
            {
                statTextSetter = GameObject.FindGameObjectWithTag("Stats Tab").GetComponent<SetStatText>();
            }
            if (controller == null)
            {
                controller = GameObject.FindWithTag("GameController");
                director = controller.GetComponent<Director>();
            }
            if (self == null)
            {
                self = this.gameObject;
            }
            if (dialogueHandler == null)
            {
                dialogueHandler = controller.GetComponent<DialogueHandler>();
            }

            if (knotChecker == null)
            {
                knotChecker = GameObject.FindGameObjectWithTag("GameController").GetComponent<InkKnotCheck>();
            }

            if (this.appointmentNum >= 1)
            {
                director.setActorStats();
                director.updateCheckerStats();
                dialogueHandler.updateAppointmentCheck();
            }
            controller.GetComponent<ManaCalculator>().SetTextUIMana();
            dialogueHandler.midsectionRefreash();
            setSelfUIImage();
            haveIGottenMyPotionYet = false;
        }
        else
        {

        }
        /*
        //delete comment asterisks when finished testing emotions
        if(statTextSetter == null)
        {
            statTextSetter = GameObject.FindGameObjectWithTag("Stats Tab").GetComponent<SetStatText>();
        }
        if (controller == null)
        {
            controller = GameObject.FindWithTag("GameController");
            director = controller.GetComponent<Director>();
        }
        if(this.appointmentNum <= 1)
        {
            Body = 0;
            Intellect = 0;
            Charisma = 0;
            Will = 0;
        }
        else
        {
            director.setActorStats();
        }
        */
    }
    public Image grabSelfUIImage()
    {
        //sets the emotion
        Image gratefulImage;
        
        gratefulImage = gameObject.GetComponent<Image>();
        
        return gratefulImage;
    }

    public void setSelfUIImage()
    {
        Image grabbedSelf = grabSelfUIImage();
        characterEmotion = grabbedSelf;

    }
    public void setActorSelfImage()
    {
        
        Image grabbedSelf = characterEmotion;
        if (actorName.Contains("dante"))
        {
            DanteEmoteLogic dante = self.GetComponent<DanteEmoteLogic>();
            dante.characterEmotion = grabbedSelf;
        }
        if (actorName.Contains("indra"))
        {
            IndraEmoteLogic indra = self.GetComponent<IndraEmoteLogic>();
            indra.characterEmotion = grabbedSelf;
        }
        if (actorName.Contains("mei lin"))
        {
            MeiLinEmoteLogic mei_lin = self.GetComponent<MeiLinEmoteLogic>();
            mei_lin.characterEmotion = grabbedSelf;
        }
        if (actorName.Contains("poppy"))
        {
            PoppyEmoteLogic poppy = self.GetComponent<PoppyEmoteLogic>();
            poppy.characterEmotion = grabbedSelf;
        }
        if (actorName.Contains("zinnia"))
        {
            PoppyEmoteLogic poppy = self.GetComponent<PoppyEmoteLogic>();
            poppy.characterEmotion = grabbedSelf;
        }
    }

    public void changeActorToDispleased()
    {
        Image changingEmote = characterEmotion;
        changingEmote.sprite = displeased;
    }
    public void changeActorToNuetral()
    {
        Image changingEmote = characterEmotion;
        changingEmote.sprite = neutral;
    }
    public void changeActorToSad()
    {
        Image changingEmote = characterEmotion;
        changingEmote.sprite = sad;
    }
    public void changeActorToAnnoyedOrFurstrated()
    {
        var changingEmote = characterEmotion;
        changingEmote.sprite = annoyed_or_frustrated;
    }
    public void changeActorToHappy()
    {
        var changingEmote = characterEmotion;
        changingEmote.sprite = happy;
    }

    private void OnDestroy()
    {
        //when scene transistions, check the stats and the next appointment using the following knotChecker checkPath Functions
        if (actorName.Contains("Dante"))
        {
            knotChecker.determineDantePath();
        }
        else if (actorName.Contains("Indra"))
        {
            knotChecker.determineIndraPath();
        }
        else if (actorName.Contains("Mei Lin"))
        {
            knotChecker.determineMeiLinPath();
        }
        else if (actorName.Contains("Poppy"))
        {
            knotChecker.determinePoppyPath();
        }
    }
}
