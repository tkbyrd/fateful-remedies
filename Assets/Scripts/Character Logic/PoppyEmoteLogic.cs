﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoppyEmoteLogic : MonoBehaviour
{

    //character emotions exclusive to Poppy
    
    public Sprite only_poppy_PTSD;
    public Sprite only_poppy_Neutral;
    public Sprite only_poppy_Numb;
    //character emotions exclusive to Zinnia
    public Sprite only_zinnia_Neutral;
    public Sprite only_zinnia_Sick;
    public Sprite only_zinnia_Upset;

    //combined emotions
    public Sprite PFZN;
    public Sprite PFZU;
    public Sprite PHZN;
    public Sprite PHZU;
    public Sprite PNZU;
    public Sprite PSZN;
    public Sprite PSZU;

    Actor actor;

    public Image characterEmotion;
    public void Start()
    {
        if (actor == null)
        {
            actor = this.gameObject.GetComponent<Actor>();
        }
        if (characterEmotion == null)
        {
            characterEmotion = GetComponent<Image>();
        }
        actor.setActorSelfImage();
    }
    

    // due to the sheer amount of emotions poppy and zinnia have, their emotion functions are combined depending on who is needed
    // solely poppy emotions
    public void changeOnlyPoppy(string Emotion)
    {
        Sprite changingEmote = characterEmotion.sprite;
        switch (Emotion)
        {
            case "neutralP":
                changingEmote = only_poppy_Neutral;
                break;
            case "numb":
                changingEmote = only_poppy_Numb;
                break;
            case "PTSD":
                changingEmote = only_poppy_PTSD;
                break;
        }
        
     
        
    }
    // poppy and zinnia together 
    public void changePoppyAndZinnia(string Emotion)
    {
        Sprite changingEmote = characterEmotion.sprite;
        switch (Emotion)
        {
            
            case "frustrated_neutral":
                changingEmote = PFZN;
                break;
            case "frustrated_upset":
                changingEmote = PFZU;
                break;

            case "happy_neutral":
                changingEmote = PHZN;
                break;

            case "happy_upset":
                changingEmote = PHZU;
                break;

            case "neutral_upset":
                changingEmote = PNZU;
                break;

            case "sad_neutral":
                changingEmote = PSZN;
                break;

            case "sad_upset":
                changingEmote = PSZU;
                break;

        }
    }

    // solely Zinnia emotions
    public void changeOnlyZinnia(string Emotion)
    {
        Sprite changingEmote = characterEmotion.sprite;
        switch (Emotion)
        {
            case "neutralZ":
                changingEmote = only_zinnia_Neutral;
                break;
            case "upset":
                changingEmote = only_zinnia_Upset;
                break;
            case "sick":
                changingEmote = only_zinnia_Sick;
                break;
        }
        
    }
}
