﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DanteEmoteLogic : MonoBehaviour
{
    //This script deals with dante exlusive emotions and changing of said emotions
    //character emotions exclusive to Dante
    public Sprite dante_Adult;
    public Sprite dante_Angry;
    public Sprite dante_Boastful;
    public Sprite dante_Cornered;
    public Sprite dante_Injured;
    public Sprite dante_Nervous;
    Actor actor;

    public Image characterEmotion;
    public void Start()
    {
        if (actor == null)
        {
            actor = this.gameObject.GetComponent<Actor>();
        }
        if (characterEmotion == null)
        {
            characterEmotion = GetComponent<Image>();
        }
        actor.setActorSelfImage();
    }
        //Emotion changing functions
    public void changeDanteToAdult()
    {
        Image changingEmote = characterEmotion;
        //changes dante's emotion to adult
        changingEmote.sprite = dante_Adult;
    }

    public void changeDanteToBoastful()
    {
        Image changingEmote = characterEmotion;
        //changes dante's emotion to boastful
        changingEmote.sprite = dante_Boastful;
    }
    public void changeDanteToCornered()
    {
        Image changingEmote = characterEmotion;
        //changes dante's emotion to cornered
        changingEmote.sprite = dante_Cornered;
    }
    public void changeDanteToInjured()
    {
        Image changingEmote = characterEmotion;
        //changes dante's emotion to injured
        changingEmote.sprite = dante_Injured;
    }
    public void changeDanteToNervous()
    {
        Image changingEmote = characterEmotion;
        //changes dante's emotion to nervous
        changingEmote.sprite = dante_Nervous;
    }


}
