﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Director : MonoBehaviour
{
    // Start is called before the first frame update
    //who is in the scene
    public GameObject actorCharacterName;
    private GameObject nameTag;
    //Appointment number
    public int appointmentNum = 0;

    // Dante given stats 
    public int dante_Body;
    public int dante_Intellect;
    public int dante_Charisma;
    public int dante_Will;

    // Indra given stats 
    public int indra_Body;
    public int indra_Intellect;
    public int indra_Charisma;
    public int indra_Will;

    // Mei Lin given stats 
    public int mei_lin_Body;
    public int mei_lin_Intellect;
    public int mei_lin_Charisma;
    public int mei_lin_Will;

    // Poppy and Zinnia given stats 
    public int poppy_Body;
    public int poppy_Intellect;
    public int poppy_Charisma;
    public int poppy_Will;

    //gets nametag textMesh
    public TextMeshProUGUI nameTagText;

    //gets Knot Checker
    InkKnotCheck knotChecker;

    //Actor Script
    Actor currentActorCharacter;

    public void Start()
    {
        actorCharacterName = null;
        currentActorCharacter = null;
        if (actorCharacterName == null && currentActorCharacter == null)
        {
            actorCharacterName = GameObject.FindGameObjectWithTag("Character");
            currentActorCharacter = actorCharacterName.GetComponent<Actor>();

        }
        /*
        if (nameTagText == null)
        {
            nameTag = GameObject.FindGameObjectWithTag("nameTag");
            nameTagText = nameTag.GetComponent<TextMeshProUGUI>();
        }
        */
        if (appointmentNum == 0)
        {
            initializeMasterStats();
        }
        

        if (knotChecker == null)
        {
            knotChecker = GameObject.FindGameObjectWithTag("GameController").GetComponent<InkKnotCheck>();
        }
    }
    
    public void initializeMasterStats()
    {
        //Adds current stats from actor to respective variables

        try 
        {
            if (actorCharacterName.name == "Dante") {
                dante_Body = 5;
                dante_Intellect = 7;
                dante_Charisma = 5;
                dante_Will = 3;
            }
            else if (actorCharacterName.name == "Indra")
            {
                indra_Body = 5;
                indra_Intellect = 4;
                indra_Charisma = 7;
                indra_Will = 4;
            }
            else if (actorCharacterName.name == "Mei Lin")
            {
                mei_lin_Body = 2;
                mei_lin_Intellect = 6;
                mei_lin_Charisma = 5;
                mei_lin_Will = 7;
            }
            else if (actorCharacterName.name == "Poppy")
            {
                poppy_Body = 7;
                poppy_Intellect = 6;
                poppy_Charisma = 3;
                poppy_Will = 4;
            }
            else if (actorCharacterName.name == "Zinnia")
            {
                poppy_Body = 7;
                poppy_Intellect = 6;
                poppy_Charisma = 3;
                poppy_Will = 4;
            }
            else if (actorCharacterName.name == "Poppy and Zinnia")
            {
                poppy_Body = 7;
                poppy_Intellect = 6;
                poppy_Charisma = 3;
                poppy_Will = 4;
            }
        }
        catch (System.Exception) {
            actorCharacterName = null;
        }
    }

    public void updateMasterStats()
    {
        //Adds current stats from actor to respective variables

        try
        {
            if (actorCharacterName.name == "Dante")
            {
                dante_Body += currentActorCharacter.Body;
                dante_Intellect += currentActorCharacter.Intellect;
                dante_Charisma += currentActorCharacter.Charisma;
                dante_Will += currentActorCharacter.Will;
            }
            else if (actorCharacterName.name == "Indra")
            {
                indra_Body += currentActorCharacter.Body;
                indra_Intellect += currentActorCharacter.Intellect;
                indra_Charisma += currentActorCharacter.Charisma;
                indra_Will += currentActorCharacter.Will;
            }
            else if (actorCharacterName.name == "Mei Lin")
            {
                mei_lin_Body += currentActorCharacter.Body;
                mei_lin_Intellect += currentActorCharacter.Intellect;
                mei_lin_Charisma += currentActorCharacter.Charisma;
                mei_lin_Will += currentActorCharacter.Will;
            }
            if (actorCharacterName.name == "Poppy")
            {
                poppy_Body += currentActorCharacter.Body;
                poppy_Intellect += currentActorCharacter.Intellect;
                poppy_Charisma += currentActorCharacter.Charisma;
                poppy_Will += currentActorCharacter.Will;
            }
            else if (actorCharacterName.name == "Zinnia")
            {
                poppy_Body += currentActorCharacter.Body;
                poppy_Intellect += currentActorCharacter.Intellect;
                poppy_Charisma += currentActorCharacter.Charisma;
                poppy_Will += currentActorCharacter.Will;
            }
            else if (actorCharacterName.name == "Poppy and Zinnia")
            {
                poppy_Body += currentActorCharacter.Body;
                poppy_Intellect += currentActorCharacter.Intellect;
                poppy_Charisma += currentActorCharacter.Charisma;
                poppy_Will += currentActorCharacter.Will;
            }
            else
            {
                print("Either this is a tutorial or there is no person who needs stats here");
            }
        }
        catch (System.Exception)
        {
            actorCharacterName = null;
        }
    }

    public void setActorStats()
    {
        //Updates character scripts to reflect stored stats given to the director


        try
        {
            if (actorCharacterName.name == "Dante")
            {
                currentActorCharacter.Body = dante_Body;
                currentActorCharacter.Intellect = dante_Intellect;
                currentActorCharacter.Charisma = dante_Charisma;
                currentActorCharacter.Will = dante_Will;
            }
            else if (actorCharacterName.name == "Indra")
            {
                currentActorCharacter.Body = indra_Body;
                currentActorCharacter.Intellect = indra_Intellect;
                currentActorCharacter.Charisma = indra_Charisma;
                currentActorCharacter.Will = indra_Will;
            }
            else if (actorCharacterName.name == "Mei Lin")
            {
                currentActorCharacter.Body = mei_lin_Body;
                currentActorCharacter.Intellect = mei_lin_Intellect;
                currentActorCharacter.Charisma = mei_lin_Charisma;
                currentActorCharacter.Will = mei_lin_Will;
            }
            else if (actorCharacterName.name == "Poppy")
            {
                currentActorCharacter.Body = poppy_Body;
                currentActorCharacter.Intellect = poppy_Intellect;
                currentActorCharacter.Charisma = poppy_Charisma;
                currentActorCharacter.Will = poppy_Will;
            
            }
        }
        catch (System.Exception)
        {
            actorCharacterName = null;
        }
    }
    public void updateCheckerStats()
    {
        //Sets the checker Stats 
        var checker = knotChecker;
        try
        {
            if (actorCharacterName.name == "Dante")
            {
                checker.dante_Body_Check = dante_Body;
                checker.dante_Intellect_Check = dante_Intellect;
                checker.dante_Charisma_Check = dante_Charisma;
                checker.dante_Will_Check = dante_Will;
            }
            else if (actorCharacterName.name == "Indra")
            {
                checker.indra_Body_Check = indra_Body;
                checker.indra_Intellect_Check = indra_Intellect;
                checker.indra_Charisma_Check = indra_Charisma;
                checker.indra_Will_Check = indra_Will;
            }
            else if (actorCharacterName.name == "Mei Lin")
            {
                checker.mei_lin_Body_Check = mei_lin_Body;
                checker.mei_lin_Intellect_Check = mei_lin_Intellect;
                checker.mei_lin_Charisma_Check = mei_lin_Charisma;
                checker.mei_lin_Will_Check = mei_lin_Will;
            }
            else if (actorCharacterName.name == "Poppy")
            {
                checker.poppy_Body_Check = poppy_Body;
                checker.poppy_Intellect_Check = poppy_Intellect;
                checker.poppy_Charisma_Check = poppy_Charisma;
                checker.poppy_Will_Check = poppy_Will;
            }

            
        }
        catch (System.Exception)
        {
            actorCharacterName = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
