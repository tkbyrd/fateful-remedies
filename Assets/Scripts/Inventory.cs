﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public int charge = 8;
    
    //Mountain
    public int lavender = 0;
    public int chamomile = 0;
    public int bearberry = 0;

    //Desert
    public int chili_pepper = 1;
    public int snap_dragon = 0;
    public int mandrake = 0;

    //Forest
    public int bleeding_heart = 0;
    public int witch_hazel = 0;
    public int skeleton_flower = 0;

    //Grassland
    public int clover = 0;
    public int ginger = 0;
    public int yarrow = 0;

    void Start()
    {
        charge = 8;
    }
}
