﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class DialogueHandler : MonoBehaviour
{

    public Timeline timeline;
    public Canvas canvas;

    public Scene currentScene;

    public TextAsset danteStory;
    public TextAsset indraStory;
    public TextAsset meiLinStory;
    public TextAsset poppyStory;
    public TextAsset multiStory;
    public TextAsset tutorialStory;

    public Story story;
    public string currentKnot;
    public string nextKnot;
    public string currentEmotion;
    public string speakerCharacter; //variable exclusive to multi appointments

    public ManaCalculator manaCalculator;
    public SpawnPotions potionSpawner;
    InkKnotCheck knotChecker;

    resultManager managerOfResultsScript;

    //Character's Next Appoinments
    public string indraNextApp;
    public string danteNextApp;
    public string meilinNextApp;
    public string poppyNextApp;
    public string multiNextApp;

    public GameObject slot1;            //GO of inventory slot1
    public GameObject slot2;            //GO of inventory slot2
    public GameObject slot3;            //GO of inventory slot3
    public GameObject slot4;            //GO of inventory slot4

    //checks to see if it's the first version of the controller
    bool trueOne = false;

    //public IndraStatScript indraStats;
    public GameObject character;
    public GameObject characterInFocus; //used for setting character emotions
    public GameObject[] characters;// used for cataloguing characters for multi character appointments 

    public Text textBox;
   // public Text nameTag;
    public Button button;

    public List<GameObject> potions = new List<GameObject>();

    //variables for getting order of appoinments
    public static string t = "tutorial";
    public static string d = "Dante";
    public static string i = "Indra";
    public static string m = "Mei Lin";
    public static string p = "Poppy";
    public static string duo = "Duo";
    public static string r = "Results";

    public string canChoiceBeMade;

    public int appNum = 0;
    public int actNum = 0;

    public static string[] act1 = { /*t,*/ d, i, p, m, p, m, d, i, r};
    public static string[] act2 = { i, d, p, m, duo, d, p, i, duo, m, r};
    public static string[] act3 = { duo, d, duo, m, p, duo, duo, r};

    public string[][] acts = { act1, act2, act3 };

    public InitializeGameController initialize;

    public bool inputEnabled = true;

    // Start is called before the first frame update
    public void Start()
    {
        //print("Started");
        

        potionSpawner = this.GetComponent<SpawnPotions>();
        //*************************************************************************************
        try
        {
            timeline = GameObject.FindGameObjectWithTag("Timeline").GetComponent<Timeline>();
        }
        catch
        {

        }

        if (potionSpawner.isTutorial == false)
        {
            dontDestroy();
        }
        //potionSpawner.PotionSpawn();

        FindPotions();

        if (indraNextApp == null) {
            indraNextApp = "INDRA_APP1_INTRO";
        }

        if (danteNextApp == null) {
            danteNextApp = "DANTE_APP1_INTRO";
        }

        if (meilinNextApp == null) {
            meilinNextApp = "MEILIN_APP1_INTRO";
        }

        if (poppyNextApp == null) {
            poppyNextApp = "POPPY_APP1_INTRO";
        }

        if (textBox == null)
        {
            textBox = GameObject.Find("StoryText").GetComponent<Text>();
            //print(textBox);
        }
        
        if (manaCalculator == null)
        {
            manaCalculator = GameObject.FindGameObjectWithTag("GameController").GetComponent<ManaCalculator>();
        }
        //print("Got here");

        if (character == null) {
            //print("Character found");
            character = GameObject.FindGameObjectWithTag("Character");
        }

        GetInkyFile();

        

        initialize = this.GetComponent<InitializeGameController>();

        //gets the currentKnot variable in story
        currentKnot = (string)story.variablesState["currentKnot"];
        if (knotChecker == null)
        {
            knotChecker = GameObject.FindGameObjectWithTag("GameController").GetComponent<InkKnotCheck>();
        }
        if(managerOfResultsScript == null)
        {
            // managerOfResultsScript = GameObject.FindGameObjectWithTag("GameController").GetComponent<resultManager>();
        }

        potionSpawner.PotionSpawn();
        //makes sure if game is started from tutorial, tutorial is off after the next appointment
        isOrIsNotTutorial();
        //start the story off with the first line of text
        RefreshText();
        //setNameTag();
    }

    public int findNumberOfCharactersInScene()
    {
        //finds the number of characters in the scene so that multi appointment can work.
        int numOfCharacters = GameObject.FindGameObjectsWithTag("Character").Length;

        return numOfCharacters;
    }

    private GameObject[] fillCharactersArray() {
        //at the start of every poppy/multi appointment, characters are added to the array for manipulation
        characters = null;

        GameObject[] allCharacters = GameObject.FindGameObjectsWithTag("Character");

        return allCharacters;
        
    }


    public bool isOrIsNotTutorial()
    {
        bool tutorialDeterminator = potionSpawner.isTutorial;
        if (character.name.Contains("Mover Guy"))
        {
            tutorialDeterminator = true;
        }
        else
        {
            tutorialDeterminator = false;
        }
        return tutorialDeterminator;
    }

    private void Update()
    {
        if (inputEnabled == true) {
            if (Input.GetMouseButtonDown(0))
            {
                //Dialogue advances
                //print("Mouse Button Down");
                RefreshText();
                //print("Refresh");
            }
        }
        
        isProblemExplainedYet();
    }

    public void GetInkyFile() {
        //grabs correct inky file
        if (character.name == "Indra")
        {
            story = new Story(indraStory.text);
            //print("Assigned Indra Story");

            if (indraNextApp != "")
            {
                story.ChoosePathString(indraNextApp);
            }
        }
        else if (character.name == "Dante") {
            story = new Story(danteStory.text);
            //print("Assigned Dante Story");

            if (danteNextApp != "")
            {
                story.ChoosePathString(danteNextApp);
            }
        }
        else if (character.name == "Mei Lin")
        {
            story = new Story(meiLinStory.text);
            //print("Assigned Mei Lin Story");

            if (meilinNextApp != "")
            {
                story.ChoosePathString(meilinNextApp);
            }
        }
        else if (character.name == "Poppy")
        {
            story = new Story(poppyStory.text);
            //print("Assigned Poppy Story");

            if (poppyNextApp != "")
            {
                story.ChoosePathString(poppyNextApp);
            }
        }
        else if (character.name == "Mover Guy")
        {
            story = new Story(tutorialStory.text);
        }
        else if (findNumberOfCharactersInScene() > 1)
        {
            story = new Story(multiStory.text);
            //print("assigned multi story");

            if(multiNextApp != "")
            {
                story.ChoosePathString(multiNextApp);
            }
        }
    }
    public void updateAppointmentCheck()
    {
        if(character.name == "Dante")
            knotChecker.danteNextAppCheck = danteNextApp;
        else if (character.name == "indra")
                knotChecker.indraNextAppCheck = indraNextApp;
        else if (character.name == "Mei Lin")
            knotChecker.meilinNextAppCheck = meilinNextApp;
        else if (character.name == "Poppy" || character.name == "Zinnia")
            knotChecker.poppyNextAppCheck = poppyNextApp;
        else
        {
            print("Either this is a tutorial or there is no person who needs stats here");
        }
    }

    void dontDestroy()
    {
        //puts DH in do not destroy on load
        
         GameObject[] gameController = GameObject.FindGameObjectsWithTag("GameController");
         DontDestroyOnLoad(gameController[0]);


         // if there are more than 1 players, then we know that we have more than we need
         // so find the original one
         if (gameController.Length > 1)
            {
             foreach (GameObject player in gameController)
             {
                  if (player.GetComponent<DialogueHandler>().trueOne == false)
                  {
                       Destroy(player);
                  }
             }
         }
         else
         {
            trueOne = true; // only first one is true one
         }
         DontDestroyOnLoad(gameController[0]);
        
    }

    // This is the main function called every time the story changes. It does a few things:
    // Destroys all the old content and choices.
    // Continues over all the lines of text, then displays all the choices. If there are no choices, the story is finished!
    public void RefreshText()
    {
        //print((string)story.variablesState["tutorialDone"]);
        if ((string)story.variablesState["tutorialDone"] == "true") {
            WhoIsNext();
        }

        if (isOrIsNotTutorial() == false)
        {
            currentEmotion = (string)story.variablesState["currentEmotion"];
            // Read all the content until we can't continue any more
            if (story.canContinue)
            {
                soleCharacterPortraitChange(currentEmotion);
                // Continue gets the next line of the story
                string text = story.Continue();
                // This removes any white space from the text.
                text = text.Trim();
                // Display the text on screen!
                CreateContentView(text);
                //change the current emotion
            }
            else if (timeline.fadeScreen.alpha == 1f)
            {
                timeline.fadeIn();
                GetInkyFile();

            }
            else
            {
                //runs function that finds the next thing that needs to be done
                ContinueStory();
            }
        }
        else
        {
            // Read all the content until we can't continue any more
            if (story.canContinue)
            {
                // Continue gets the next line of the story
                string text = story.Continue();
                // This removes any white space from the text.
                text = text.Trim();
                // Display the text on screen!
                CreateContentView(text);
                //change the current emotion
            }
            else
            {
                //runs function that finds the next thing that needs to be done
                ContinueStory();
            }
        }
    }

    // When we click the choice button, tell the story to choose that choice!
    public void OnClickChoiceButton(GameObject choice)
    {
        //print("Choice made");
        //print(nextKnot);
        //story.ChoosePathString(nextKnot);
        
        if (choice.tag == "Body") {
            story.ChooseChoiceIndex(0);
            RefreshText();
        } else if (choice.tag == "Intellect") {
            story.ChooseChoiceIndex(1);
            RefreshText();
        } else if (choice.tag == "Charisma"){
            story.ChooseChoiceIndex(2);
            RefreshText();
        } else {
            story.ChooseChoiceIndex(3);
            RefreshText();
        }

        //RefreshText();
    }

    // Creates a button showing the choice text
    void CreateContentView(string text)
    {
        textBox.text = text;
    }
    
    void actorsOnSet()
    {
        //checks which actors are in a given scene

        //grabs characters needed from multiple ink
        string characterA = (string)story.variablesState["characterA"];
        string characterB = (string)story.variablesState["characterB"];
        string characterC = (string)story.variablesState["characterC"];
        string characterD = (string)story.variablesState["characterD"];
        //goes through character array to set needed characters to visible
        foreach(GameObject castMember in characters)
        {
            if (castMember.name == characterA)
            {
                castMember.SetActive(true);
            }
            else if(castMember.name == characterB)
            {
                castMember.SetActive(true);
            }
            else if (castMember.name == characterC)
            {
                castMember.SetActive(true);
            }
            else if (castMember.name == characterD)
            {
                castMember.SetActive(true);
            }
            else
            {
                castMember.SetActive(false);
            }
        }
    }

    void actorInShop()
    {
        //when a character leaves a multi appointment and there are people left, they shouldn't be active anymore.
        
        //gets the character who left
        string whoLeft = (string)story.variablesState["whoLeft"];
        //filters through the character list and finds the character who left
        //if it matches the character name, they will be set to inactive
        // if it doesn't then it passes them
        foreach (GameObject castMember in characters)
        {
            if (castMember.name == whoLeft)
            {
                castMember.SetActive(false);
            }
        }

    }

    void changeSpeaker()
    {
        //function exclusive to the first tutorial appointment,poppy appointments with zinnia in them too, and multi appointments
        //changes the name tag depending on who is talking
        string nameTagText = " ";
        var variableCheck = (string)story.variablesState["whoIsSpeaking"];
        //checks if the who is speaking variable is present throughout an appointment
        //if the "whoIsSpeaking" variable is present, the nameTagText will change as the text refreshes
        //otherwise the speaker character should be the current character

        if (variableCheck == null)
        {
            speakerCharacter = character.name;
        }
        else
        {
            speakerCharacter = variableCheck;
        }

        if (speakerCharacter == "Dante")
        {
            nameTagText = "Dante";
        }
        if (speakerCharacter == "Indra")
        {
            nameTagText = "Indra";
        }
        if (speakerCharacter == "Mei Lin")
        {
            nameTagText = "Mei Lin";
        }
        if (speakerCharacter == "Poppy")
        {
            nameTagText = "Poppy";
        }
        if (speakerCharacter == "Zinnia")
        {
            nameTagText = "Zinnia";
        }
        if (speakerCharacter == "Mover")
        {
            nameTagText = "Mover";
        }
        if (speakerCharacter == "Narrator")
        {
            nameTagText = "Narrator";
        }
        var nameText = GameObject.Find("NameText").GetComponent<Text>();

        nameText.text = nameTagText;

    }

    void ContinueStory() {
        //Once the inky story hits ->END this function runs and finds what to do next

        //gets the currentKnot variable in story
        currentKnot = (string)story.variablesState["currentKnot"];
        //gets if choice can be made in the story
        updateIfChoiceCanBeMade();

       // gets currect character in scene
        character = GameObject.FindGameObjectWithTag("Character");

        

        if (character.name == "Indra") {
            nextKnot = (string)story.variablesState["nextKnot"];
            indraNextApp = nextKnot;
        } else if (character.name == "Dante")
        {
            nextKnot = (string)story.variablesState["nextKnot"];
            danteNextApp = nextKnot;
        } else if (character.name == "Mei Lin")
        {
            nextKnot = (string)story.variablesState["nextKnot"];
            meilinNextApp = nextKnot;
        } else if (character.name == "Poppy" || character.name == "Zinnia" || character.name == "Poppy and Zinnia")
        {
            nextKnot = (string)story.variablesState["nextKnot"];
            poppyNextApp = nextKnot;
        }

        if ((string)story.variablesState["appDone"] == "true") {
            timeline.endOfApp();
        }
        
    }
 
    public void WhoIsNext() {
        //figures out which character is next in the schedule

        //print("Before: " + appNum);

        if (character.name != "Mover Guy")
        {
            appNum += 1;
        }
        
        if (appNum > acts[actNum].Length) {
            appNum = 0;
            actNum += 1;
        }
        try
        {
            //print(appNum);
            if (acts[actNum][appNum] == i)
            {
                SceneManager.LoadScene("indra_1");

            }
            else if (acts[actNum][appNum] == d)
            {
                SceneManager.LoadScene("dante_1");

            }
            else if (acts[actNum][appNum] == m)
            {
                SceneManager.LoadScene("meilin_1");

            }
            else if (acts[actNum][appNum] == p)
            {
                SceneManager.LoadScene("poppy_1");

            }
            else if (acts[actNum][appNum] == duo)
            {
                SceneManager.LoadScene("multi_1");

            }
            else if (acts[actNum][appNum] == r)
            {
                SceneManager.LoadScene("results");
            }
            else if (acts[actNum][appNum] == t)
            {
                SceneManager.LoadScene("tutorial");
            }
        }
        catch {
            
        }
        
    }
    
    //checks if customer explained the problem yet.
    //if the problem is not explained when potion is clicked
    //potion will not be given
    public bool isProblemExplainedYet()
    {
        //picks up if problem is explained yet from ink script first 

        //bool that gets returned
        bool isExplained = false;
        //potion checks dilogue handler if potion can be given
        if (canChoiceBeMade.Contains("yes"))
        {
            isExplained = true;
            togglePotionsOn();
        }
        else
        {
            isExplained = false;
            togglePotionsOff();
        }
        return isExplained;
    }
    
    void updateIfChoiceCanBeMade()
    {
        //on function call, grab canChoiceBeMade from ink script
        canChoiceBeMade = (string)story.variablesState["canChoiceBeMade"];
    }

    void FindPotions() {
        //Finds slots in scene
        slot1 = GameObject.FindGameObjectWithTag("Slot1");
        slot2 = GameObject.FindGameObjectWithTag("Slot2");
        slot3 = GameObject.FindGameObjectWithTag("Slot3");
        slot4 = GameObject.FindGameObjectWithTag("Slot4");

        //Finds potions in scene
        //print("Potions found");
        //adds empty slots to emptySlots list
        if (slot1.transform.childCount < 1)
        {
            potions.Add(slot1);
        }
        if (slot2.transform.childCount < 1)
        {
            potions.Add(slot2);
        }
        if (slot3.transform.childCount < 1)
        {
            potions.Add(slot3);
        }
        if (slot3.transform.childCount < 1)
        {
            potions.Add(slot4);
        }
    }

    void togglePotionsOn()
    {
        //turns potions on
        foreach (GameObject potion in potions)
        {
            potion.SetActive(true);
        }
    }

    void togglePotionsOff()
    {
        //turns potions off
        foreach (GameObject potion in potions)
        {
            potion.SetActive(false);
        }
    }
    public GameObject[] AddItemToArray(GameObject[] original, GameObject itemToAdd) {
        GameObject[] output = new GameObject[original.Length + 1];
        for (int i = 0; i <= original.Length - 1; i++) {
            if (original.Length > 0)
            {
                output[i] = original[i];
            }
            else
            {
                output[0] = itemToAdd;
                return output;
            }
        }
        output[output.Length - 1] = itemToAdd;
        return output;
    }
    
    public void midsectionRefreash()
    {
        //using the appointments and the act variables
        //midway through each act, mana should reset
        if(actNum == 0)
        {
            //since there are no duos, appNum should be fine for indicatorS
            if(appNum == 4)
            {
                manaCalculator.resetManaLeft();
            }
        }
        if (actNum == 1)
        {
            if (appNum == 0)
            {
                manaCalculator.resetManaLeft();
            }
            else if (appNum == 4)
            {
                manaCalculator.resetManaLeft();
            }
            else if (appNum == 7)
            {
                manaCalculator.resetManaLeft();
            }

        }
        if (actNum == 2)
        {
            if (appNum == 0)
            {
                manaCalculator.resetManaLeft();
            }
            if (appNum == 4)
            {
                manaCalculator.resetManaLeft();
            }
            
        }
    }

    void checkWhoNeedsToBeChanged()
    {
        //allows for portrait manuipulation for multiple characters in a given character
        // for now it makes sure the character speaking isn't highlighted.
        foreach (GameObject customer in characters)
        {
            CanvasGroup customerCanvasGroup = customer.GetComponent<CanvasGroup>();
            if (speakerCharacter != customer.name)
            {
                timeline.StartCoroutine(timeline.FadeCanvasGroup(customerCanvasGroup, customerCanvasGroup.alpha, 0.5f));
            }
            else if (speakerCharacter != "Zinnia")
            {
                timeline.StartCoroutine(timeline.FadeCanvasGroup(customerCanvasGroup, customerCanvasGroup.alpha, 0.5f));
            }
            else
            {
                timeline.StartCoroutine(timeline.FadeCanvasGroup(customerCanvasGroup, customerCanvasGroup.alpha, 0f));
            }
        }
    }
    
    void multipleharacterPortraitChange(string givenEmotion, string characterName)
    {
        //function extension for cases of multiple characters in the scene
        //plug in the given emotion and given name 
        //if it's a character in the game the function will find the speakerCharacter and then shift focus to that character
        characterInFocus = GameObject.Find(characterName);// sets the character in focus to the speaker character

        character = characterInFocus;// should shift the current character over to the one who is currently speaking

        soleCharacterPortraitChange(givenEmotion, characterName);// plugs in the current emotion and the character name into the sole character portrait function.

    }

    void soleCharacterPortraitChange(string givenEmotion, string characterName = null)
    {
        //This function will change the character's emotions based on what we need during a certain scenario
        //some emotions will be exclusive to some characters, so some will be locked behind if statements specific to the character in the scene
        //
        
        var characterActorScript = character.GetComponent<Actor>();
        switch (givenEmotion)
        {
            case "neutral":
                characterActorScript.changeActorToNuetral();
                break;

            case "sad":
                characterActorScript.changeActorToSad();
                break;

            case "annoyed":
                characterActorScript.changeActorToAnnoyedOrFurstrated();
                break;

            case "frustrated":
                characterActorScript.changeActorToAnnoyedOrFurstrated();
                break;

            case "displeased":
                characterActorScript.changeActorToDispleased();
                break;
            case "shock":
                characterActorScript.changeActorToDispleased();
                break;

            case "angry":
                characterActorScript.changeActorToDispleased();
                break;
            case "mad":
                characterActorScript.changeActorToDispleased();
                break;

            case "happy":
                characterActorScript.changeActorToHappy();
                break;

            case "content":
                characterActorScript.changeActorToHappy();
                break;

            case null:
                characterActorScript.changeActorToNuetral();
                break;

        } 
        if (characterName == null)
        {
            characterName = character.name;
        }
        switch (characterName)
        {
            //section of function does emotions based on specific characters
            case "Dante":
                
                var danteEmote = character.GetComponent<DanteEmoteLogic>();
                if (givenEmotion.Contains("adult"))
                {
                    danteEmote.changeDanteToAdult();
                }
                else if (givenEmotion.Contains("boastful"))
                {
                    danteEmote.changeDanteToBoastful();
                }
                else if (givenEmotion.Contains("fucked"))
                {
                    danteEmote.changeDanteToCornered();
                }
                else if (givenEmotion.Contains("injured"))
                {
                    danteEmote.changeDanteToInjured();
                }
                else if (givenEmotion.Contains("nervous"))
                {
                    danteEmote.changeDanteToNervous();
                }
                break;

        
            case "Indra":
            
                var indraEmote = character.GetComponent<IndraEmoteLogic>();
                if (givenEmotion.Contains("boastful"))
                {
                    indraEmote.changeIndraToBoastful();
                }
                else if (givenEmotion.Contains("frightened"))
                {
                    indraEmote.changeIndraToFrightened();
                }
                else if (givenEmotion.Contains("shellshocked"))
                {
                    indraEmote.changeIndraToShellshocked();
                }
                else if (givenEmotion.Contains("surprised"))
                {
                    indraEmote.changeIndraToSurprised();
                }

                break;

            case "Mei Lin":
            
                var meiLinEmote = character.GetComponent<MeiLinEmoteLogic>();
                if (givenEmotion.Contains("nervous"))
                {
                    meiLinEmote.changeMeiLinToNervous();
                }
                break;

            case "Poppy":

                var poppyEmote = character.GetComponent<PoppyEmoteLogic>();
                if (givenEmotion == "neutralP")
                {
                    poppyEmote.changeOnlyPoppy(givenEmotion);
                }
                if (givenEmotion == "neutralZ")
                {
                    poppyEmote.changeOnlyZinnia(givenEmotion);
                }
                else if (givenEmotion.Contains("numb"))
                {
                    poppyEmote.changeOnlyPoppy(givenEmotion);
                }
                else if (givenEmotion.Contains("ptsd"))
                {
                    poppyEmote.changeOnlyPoppy(givenEmotion);
                }
                else if (givenEmotion.Contains("sick"))
                {
                    poppyEmote.changeOnlyZinnia(givenEmotion);
                }
                else if (givenEmotion.Contains("upset"))
                {
                    poppyEmote.changeOnlyZinnia(givenEmotion);
                }
                else if (givenEmotion.Contains("_") ){
                    poppyEmote.changePoppyAndZinnia(givenEmotion);
                }
                break;
        }
    }

}
