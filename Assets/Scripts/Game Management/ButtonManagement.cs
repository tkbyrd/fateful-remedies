﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManagement : MonoBehaviour
{
    //Might need this portion of script for later version of game,
    // for now comment out and update later:
    //flowchart that variables are taken from

    //variables
    public string worstOption;

    //director 
    Director director;

    //dialogue handler
    DialogueHandler dialogueHandler;

    //Keep this set of variables and functions in case we want to have the ability to save the game later.
    //variables we want saved
    private int indra_body = 0;
    private int indra_charisma = 0;
    private int indra_intellect = 0;
    private int indra_will = 0;
    public string indra_Appointment = "";
    private int dante_body = 0;
    private int dante_charisma = 0;
    private int dante_intellect = 0;
    private int dante_will = 0;
    public string dante_Appointment = "";
    private int meiLin_body = 0;
    private int meiLin_charisma = 0;
    private int meiLin_intellect = 0;
    private int meiLin_will = 0;
    public string meiLin_Appointment = "";
    private int poppy_body = 0;
    private int poppy_charisma = 0;
    private int poppy_intellect = 0;
    private int poppy_will = 0;
    public string poppy_Appointment = "";
    public string current_Appointment = "";



    void Start()
    {
        //gets the variables we want
        dante_Appointment = dialogueHandler.danteNextApp;
        dante_body = director.dante_Body;
        dante_charisma = director.dante_Charisma;
        dante_intellect = director.dante_Intellect;
        dante_will = director.dante_Will;
        //indra_Appointment =  dialogueHandler.indraNextApp
        indra_body = director.indra_Body;
        indra_charisma = director.indra_Charisma;
        indra_intellect = director.indra_Intellect;
        indra_will = director.indra_Will;
        //meiLin_Appointment =  dialogueHandler.danteNextApp
        meiLin_body = director.mei_lin_Body;
        meiLin_charisma = director.mei_lin_Charisma;
        meiLin_intellect = director.mei_lin_Intellect;
        meiLin_will = director.mei_lin_Will;
        //poppy_Appointment = flowchart.GetStringVariable("poppy_Appointment");
        poppy_body = director.poppy_Body;
        poppy_charisma = director.poppy_Charisma;
        poppy_intellect = director.poppy_Intellect;
        poppy_will = director.poppy_Will;


    }
    

    public void Save()
    {
        //saves the variables we want saved into player preferences
        PlayerPrefs.SetString("indraKey", indra_Appointment);
        PlayerPrefs.SetString("danteKey", dante_Appointment);
        PlayerPrefs.SetString("meiLinKey", meiLin_Appointment);
        PlayerPrefs.SetString("poppyKey", poppy_Appointment);
        PlayerPrefs.SetInt("indraKeyB", indra_body);
        PlayerPrefs.SetInt("indraKeyC", indra_charisma);
        PlayerPrefs.SetInt("indraKeyI", indra_intellect);
        PlayerPrefs.SetInt("indraKeyW", indra_will);
        PlayerPrefs.SetInt("danteKeyB", dante_body);
        PlayerPrefs.SetInt("danteKeyC", dante_charisma);
        PlayerPrefs.SetInt("danteKeyI", dante_intellect);
        PlayerPrefs.SetInt("danteKeyW", dante_will);
        PlayerPrefs.SetInt("meiLinKeyB", meiLin_body);
        PlayerPrefs.SetInt("meiLinKeyC", meiLin_charisma);
        PlayerPrefs.SetInt("meiLinKeyI", meiLin_intellect);
        PlayerPrefs.SetInt("meiLinKeyW", meiLin_will);
        PlayerPrefs.SetInt("poppyKeyB", poppy_body);
        PlayerPrefs.SetInt("poppyKeyC", poppy_charisma);
        PlayerPrefs.SetInt("poppyKeyI", poppy_intellect);
        PlayerPrefs.SetInt("poppyKeyW", poppy_will);
    }
    public void Load()
    {
        //loads saved varables into game if the keys currently exist
        indra_Appointment = PlayerPrefs.GetString("indraKey", "");
        dante_Appointment = PlayerPrefs.GetString("danteKey", "");
        indra_body = PlayerPrefs.GetInt("indraKeyB", 0);
        indra_charisma = PlayerPrefs.GetInt("indraKeyC", 0);
        indra_intellect = PlayerPrefs.GetInt("indraKeyI", 0);
        indra_will = PlayerPrefs.GetInt("indraKeyW", 0);
        dante_body = PlayerPrefs.GetInt("danteKeyB", 0);
        dante_charisma = PlayerPrefs.GetInt("danteKeyC", 0);
        dante_intellect = PlayerPrefs.GetInt("danteKeyI", 0);
        dante_will = PlayerPrefs.GetInt("danteKeyW", 0);

        /*
        // sets flowchart variables to saved counterparts 
        
        flowchart.SetStringVariable("indra_Appointment", indra_Appointment);
        flowchart.SetIntegerVariable("indra_Body", indra_body);
        flowchart.SetIntegerVariable("indra_Charisma", indra_charisma);
        flowchart.SetIntegerVariable("indra_Intellect", indra_intellect);
        flowchart.SetIntegerVariable("indra_Will", indra_will);
        flowchart.SetStringVariable("dante_Appointment", dante_Appointment);
        flowchart.SetIntegerVariable("dante_Body", dante_body);
        flowchart.SetIntegerVariable("dante_Charisma", dante_charisma);
        flowchart.SetIntegerVariable("dante_Intellect", dante_intellect);
        flowchart.SetIntegerVariable("dante_Will", dante_will);
        */
        

    }
    public void Delete()
    {
        //deletes key data
        PlayerPrefs.DeleteKey("indraKey");
        PlayerPrefs.DeleteKey("danteKey");
        PlayerPrefs.DeleteKey("indraKeyB");
        PlayerPrefs.DeleteKey("indraKeyC");
        PlayerPrefs.DeleteKey("indraKeyI");
        PlayerPrefs.DeleteKey("indraKeyW");
        PlayerPrefs.DeleteKey("danteKeyB");
        PlayerPrefs.DeleteKey("danteKeyC");
        PlayerPrefs.DeleteKey("danteKeyI");
        PlayerPrefs.DeleteKey("danteKeyW");
    }
    

    public void denyOnClick()
    {
        //once the option is clicked, the button will set the potion given boolean to true
        //afterwards it will set the worst possible option boolean to true (Or let the character's current highest stat determine what results appointment they go to)

    }

}

