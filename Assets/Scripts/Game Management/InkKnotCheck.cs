﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InkKnotCheck : MonoBehaviour
{

    /// <summary>
    //Script should utilize the ink files and the stats within the director script in order to
    //check what knot each character will go to. This will also determine what each character's ending is.
    /// </summary>
    //object reference
    public GameObject controller;
    //script references
    public DialogueHandler dialogue;
    public Director director;

    // Dante given stats 
    public int dante_Body_Check = 0;
    public int dante_Intellect_Check = 0;
    public int dante_Charisma_Check = 0;
    public int dante_Will_Check = 0;

    // Indra given stats 
    public int indra_Body_Check = 0;
    public int indra_Intellect_Check = 0;
    public int indra_Charisma_Check = 0;
    public int indra_Will_Check = 0;

    // Mei Lin given stats 
    public int mei_lin_Body_Check = 0;
    public int mei_lin_Intellect_Check = 0;
    public int mei_lin_Charisma_Check = 0;
    public int mei_lin_Will_Check = 0;

    // Poppy and Zinnia given stats 
    public int poppy_Body_Check = 0;
    public int poppy_Intellect_Check = 0;
    public int poppy_Charisma_Check = 0;
    public int poppy_Will_Check = 0;

    //Given next appointments;
    public string indraNextAppCheck;
    public string danteNextAppCheck;
    public string meilinNextAppCheck;
    public string poppyNextAppCheck;

    //

    // Start is called before the first frame update
    void Start()
    {
        if (controller == null)
        {
            controller = GameObject.FindGameObjectWithTag("GameController");
        }
        dialogue = controller.GetComponent<DialogueHandler>();
        director = controller.GetComponent<Director>();

    }
    public void determineDantePath()
    {
        //when function is called it will check Dante's current path and determine what appointment variation comes next.
        if (danteNextAppCheck == "DANTE_APP3" && !(danteNextAppCheck.Contains("3A")|| danteNextAppCheck.Contains("3B")))
        {
            if(dante_Intellect_Check == 9 || dante_Charisma_Check == 6)
            {
                dialogue.danteNextApp = "DANTE_APP3A";
            }
            else
            {
                dialogue.danteNextApp = "DANTE_APP3B";
            }
        }
        else if (danteNextAppCheck == "DANTE_APP4" && !(danteNextAppCheck.Contains("4A") || danteNextAppCheck.Contains("4B")))
        {
            if (dante_Intellect_Check == 10 || dante_Charisma_Check == 7)
            {
                dialogue.danteNextApp = "DANTE_APP4A";
            }
            else
            {
                dialogue.danteNextApp = "DANTE_APP4B";
            }
        }
        else if (danteNextAppCheck == "DANTE_APP6" && !(danteNextAppCheck.Contains("6A") || danteNextAppCheck.Contains("6B") || danteNextAppCheck.Contains("6C")))
        {
            if (dante_Intellect_Check == 12)
            {
                dialogue.danteNextApp = "DANTE_APP6A";
            }
            else if (dante_Charisma_Check == 9)
            {
                dialogue.danteNextApp = "DANTE_APP6B";
            }
            else if (dante_Will_Check == 6)
            {
                dialogue.danteNextApp = "DANTE_APP6C";
            }
            else
            {
                dialogue.danteNextApp = "DANTE_APP6C";
            }
        }
    }

    public void determineIndraPath()
    {
        //when function is called it will check Indra's current path and determine what appointment variation comes next.
        if (indraNextAppCheck == "INDRA_APP3" && !(indraNextAppCheck.Contains("3A") || indraNextAppCheck.Contains("3B")))
        {
            if (indra_Intellect_Check == 6 || indra_Charisma_Check == 8)
            {
                dialogue.indraNextApp = "INDRA_APP3A";
            }
            else
            {
                dialogue.indraNextApp = "INDRA_APP3B";
            }
        }
        else if (indraNextAppCheck == "INDRA_APP4" && !(indraNextAppCheck.Contains("4A") || indraNextAppCheck.Contains("4B")))
        {
            if (indra_Intellect_Check == 8 || indra_Charisma_Check == 10)
            {
                dialogue.indraNextApp = "INDRA_APP4A";
            }
            else
            {
                dialogue.indraNextApp = "INDRA_APP4B";
            }
        }
        else if (indraNextAppCheck == "INDRA_APP6" && !(indraNextAppCheck.Contains("6A") || indraNextAppCheck.Contains("6B") || indraNextAppCheck.Contains("6C")))
        {
            if (indra_Intellect_Check == 9)
            {
                dialogue.indraNextApp = "INDRA_APP6A";
            }
            else if (indra_Charisma_Check == 11)
            {
                dialogue.indraNextApp = "INDRA_APP6B";
            }
            else if (indra_Will_Check == 7)
            {
                dialogue.indraNextApp = "INDRA_APP6C";
            }
            else
            {
                dialogue.indraNextApp = "INDRA_APP6C";
            }
        }
    }
    public void determineMeiLinPath()
    {
        //when function is called it will check Mei Lin's current path and determine what appointment variation comes next.
        if (meilinNextAppCheck == "MEILIN_APP3" && !(meilinNextAppCheck.Contains("3A") || meilinNextAppCheck.Contains("3B")))
        {
            if (mei_lin_Body_Check == 9 || mei_lin_Charisma_Check == 4)
            {
                dialogue.meilinNextApp = "MEILIN_APP3A";
            }
            else
            {
                dialogue.meilinNextApp = "MEILIN_APP3B";
            }
        }
        else if (meilinNextAppCheck == "MEILIN_APP5" && !(meilinNextAppCheck.Contains("4A") || meilinNextAppCheck.Contains("4B")))
        {
            if (mei_lin_Body_Check == 10 || mei_lin_Charisma_Check == 5)
            {
                dialogue.meilinNextApp = "MEILIN_APP5A";
            }
            else
            {
                dialogue.meilinNextApp = "MEILIN_APP5B";
            }
        }
        else if (meilinNextAppCheck == "MEILIN_APP7" && !(meilinNextAppCheck.Contains("7A") || meilinNextAppCheck.Contains("7B") || meilinNextAppCheck.Contains("7C")))
        {
            if (mei_lin_Charisma_Check == 11)
            {
                dialogue.meilinNextApp = "MEILIN_APP7A";
            }
            else if (mei_lin_Charisma_Check == 10)
            {
                dialogue.meilinNextApp = "MEILIN_APP7B";
            }
            else if (mei_lin_Body_Check == 6)
            {
                dialogue.meilinNextApp = "MEILIN_APP7C";
            }
            else
            {
                dialogue.meilinNextApp = "MEILIN_APP7C";
            }
        }
    }

    public void determinePoppyPath()
    {
        //when function is called it will check Poppy's current path and determine what appointment variation comes next.
        if (poppyNextAppCheck == "POPPY_APP3" && !(poppyNextAppCheck.Contains("3A") || poppyNextAppCheck.Contains("3B")))
        {
            if (poppy_Body_Check == 9 || poppy_Charisma_Check == 4)
            {
                dialogue.poppyNextApp = "POPPY_APP3A";
            }
            else
            {
                dialogue.poppyNextApp = "POPPY_APP3B";
            }
        }
        else if (poppyNextAppCheck == "POPPY_APP4" && !(poppyNextAppCheck.Contains("4A") || poppyNextAppCheck.Contains("4B")))
        {
            if (poppy_Body_Check == 10 || poppy_Charisma_Check == 5)
            {
                dialogue.poppyNextApp = "POPPY_APP4A";
            }
            else
            {
                dialogue.poppyNextApp = "POPPY_APP4B";
            }
        }
        else if (poppyNextAppCheck == "POPPY_APP7" && !(poppyNextAppCheck.Contains("7A") || poppyNextAppCheck.Contains("7B") || poppyNextAppCheck.Contains("7C")))
        {
            if (poppy_Body_Check == 12)
            {
                dialogue.poppyNextApp = "POPPY_APP7A";
            }
            else if (poppy_Charisma_Check == 8)
            {
                dialogue.poppyNextApp = "POPPY_APP7B";
            }
            else if (poppy_Will_Check == 6)
            {
                dialogue.poppyNextApp = "POPPY_APP7C";
            }
            else
            {
                dialogue.poppyNextApp = "POPPY_APP7C";
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
