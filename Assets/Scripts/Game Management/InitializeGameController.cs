﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class InitializeGameController : MonoBehaviour
{

    public GameObject gameController;
    //timelineSpawner TimelineSpawner;
    public SpawnPotions spawnPotions;
    public RandomPotionMaker randomPotionMaker;
    public SetStatText setStatText;
    public givePotion givePotion;
    public DialogueHandler dialogueHandler;
    public Director director;
    public Timeline timeline;

    //variables for getting order of appoinments
    public static string d = "Dante";
    public static string i = "Indra";
    public static string m = "Mei Lin";
    public static string p = "Poppy";
    public static string duo = "Duo";

    public bool initialized = false;

    private void Start()
    {
        if (initialized == false) {
            Intialize();
        }
    }

    public void Intialize()
    {
        //Intializes GameController on Scene load
        gameController = GameObject.FindGameObjectWithTag("GameController");

        //TimelineSpawner = gameController.GetComponent<timelineSpawner>();

        spawnPotions = gameController.GetComponent<SpawnPotions>();
        randomPotionMaker = gameController.GetComponent<RandomPotionMaker>();
        setStatText = gameController.GetComponent<SetStatText>();
        givePotion = gameController.GetComponent<givePotion>();
        dialogueHandler = gameController.GetComponent<DialogueHandler>();
        director = gameController.GetComponent<Director>();

        //TimelineSpawner = gameController.GetComponent<timelineSpawner>();

        //TimelineSpawner.Start();

        //*************************************************************************************
        try
        {
            timeline = GameObject.FindGameObjectWithTag("Timeline").GetComponent<Timeline>();
        }
        catch
        {

        }

        spawnPotions.emptySlots.Clear();
        spawnPotions.Start();

        //*************************************************************************************
        try
        {
            timeline.fadeScreen = GameObject.FindGameObjectWithTag("Shop Canvas").GetComponentInChildren<CanvasGroup>();
        }
        catch
        {

        }

        randomPotionMaker.intialed = false;
        randomPotionMaker.Start();

        director.Start();

        setStatText.Start();

        givePotion.Start();

        if (dialogueHandler.appNum > 0)
        {
            dialogueHandler.potions.Clear();
        }
        dialogueHandler.Start();
        dialogueHandler.currentScene = SceneManager.GetActiveScene();
        //print(dialogueHandler.currentScene.name);

        if (dialogueHandler.acts[dialogueHandler.actNum][dialogueHandler.appNum] == i)
        {
            if (dialogueHandler.indraNextApp != "")
            {
                dialogueHandler.story.ChoosePathString(dialogueHandler.indraNextApp);
            }
        }
        else if (dialogueHandler.acts[dialogueHandler.actNum][dialogueHandler.appNum] == d)
        {
            if (dialogueHandler.danteNextApp != "")
            {
                dialogueHandler.story.ChoosePathString(dialogueHandler.danteNextApp);
            }
        }
        else if (dialogueHandler.acts[dialogueHandler.actNum][dialogueHandler.appNum] == m)
        {
            if (dialogueHandler.meilinNextApp != "")
            {
                dialogueHandler.story.ChoosePathString(dialogueHandler.meilinNextApp);
            }
        }
        else if (dialogueHandler.acts[dialogueHandler.actNum][dialogueHandler.appNum] == p)
        {
            if (dialogueHandler.poppyNextApp != "")
            {
                dialogueHandler.story.ChoosePathString(dialogueHandler.poppyNextApp);
            }
        }
        else if (dialogueHandler.acts[dialogueHandler.actNum][dialogueHandler.appNum] == duo)
        {
            if (dialogueHandler.multiNextApp != "")
            {
                dialogueHandler.story.ChoosePathString(dialogueHandler.multiNextApp);
            }
        }


        //*****************************************************************
        try
        {
            timeline.setUp();
        }
        catch
        {

        }    

        initialized = true;
    }
}

