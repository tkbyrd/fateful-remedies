﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;//allows for textmesh pro functions to be used

public class SetStatText : MonoBehaviour
{
    public GameObject character;
    public GameObject controller;
    public TextMeshProUGUI meshBodyText;
    public TextMeshProUGUI meshCharisText;
    public TextMeshProUGUI meshIntText;
    public TextMeshProUGUI meshWillText;

    private static int indra_body = 0;
    private static int indra_charisma = 0;
    private static int indra_intellect = 0;
    private static int indra_will = 0;

    private static int dante_body = 0;
    private static int dante_charisma = 0;
    private static int dante_intellect = 0;
    private static int dante_will = 0;

    private static int meilin_body = 0;
    private static int meilin_charisma = 0;
    private static int meilin_intellect = 0;
    private static int meilin_will = 0;

    private static int poppy_body = 0;
    private static int poppy_charisma = 0;
    private static int poppy_intellect = 0;
    private static int poppy_will = 0;

    //actor scipt
    Actor characterScript;

    //director script
    Director director;

    // Start is called before the first frame update
    public void Start()
    {
        if (character == null)
        {
            character = GameObject.FindWithTag("Character");
            characterScript = character.GetComponent<Actor>();
        }
        if (controller == null)
        {
            controller = GameObject.FindWithTag("GameController");
            director = controller.GetComponent<Director>();
        }


        findTextMesh();
    }

    //function finds the text of the current character's stat page
    //and then sets the in script accesors to them
    public void findTextMesh()
    {
        meshBodyText = GameObject.FindWithTag("Stat TextB").GetComponent<TextMeshProUGUI>();
        meshCharisText = GameObject.FindWithTag("Stat TextC").GetComponent<TextMeshProUGUI>();
        meshIntText = GameObject.FindWithTag("Stat TextI").GetComponent<TextMeshProUGUI>();
        meshWillText = GameObject.FindWithTag("Stat TextW").GetComponent<TextMeshProUGUI>();
        meshBodyText.text = "";
        meshCharisText.text = "";
        meshIntText.text = "";
        meshWillText.text = "";
        getStatsForChar();
    }

    public void getStatsForChar()
    {
        if (character.name == "Indra")
        {
            //print("Found indra");

            //gets stats from actor script
            indra_body += director.indra_Body;
            indra_charisma += director.indra_Charisma;
            indra_intellect += director.indra_Intellect;
            indra_will += director.indra_Will;

        }
        else if (character.name == "Dante")
        {
            //sets stats based on tag of which character object is in the scene
            //if no tag it will say so

            dante_body += director.dante_Body;
            dante_charisma += director.dante_Charisma;
            dante_intellect += director.dante_Intellect;
            dante_will += director.dante_Will;

        }
        else if (character.name == "Poppy" || character.name == "Zinnia"||character.name == "Poppy and Zinnia")
        {
            //sets stats based on tag of potion object
            //if no tag it will say so
            //print("Found Poppy");
            
            poppy_body += director.poppy_Body;
            poppy_charisma += director.poppy_Charisma;
            poppy_intellect += director.poppy_Intellect;
            poppy_will += director.poppy_Will;
        }
        else if (character.name == "Mei Lin")
        {
            //sets stats based on tag of potion object
            //if no tag it will say so
            //print("Found Mei Lin");
            
            meilin_body += director.mei_lin_Body;
            meilin_charisma += director.mei_lin_Charisma;
            meilin_intellect += director.mei_lin_Intellect;
            meilin_will += director.mei_lin_Will;
        }
        setTextMesh();
    }

    public void setTextMesh() 
    {
        //function sets the stats tab to reflect the current stats of the character
        if (character.name == "Indra")
        {
            //print("Found Indra");
            meshBodyText.text = indra_body.ToString();
            meshCharisText.text = indra_charisma.ToString();
            meshIntText.text = indra_intellect.ToString();
            meshWillText.text = indra_will.ToString();
        }
        else if (character.name == "Dante")
        {
            //sets stats based on tag of potion object
            //if no tag it will say so
            //print("Found Dante");
            meshBodyText.text = dante_body.ToString();
            meshCharisText.text = dante_charisma.ToString();
            meshIntText.text = dante_intellect.ToString();
            meshWillText.text = dante_will.ToString();


        }
        else if ((character.name == "Poppy") || (character.name == "Zinnia"))
        {
            //sets stats based on tag of potion object
            //if no tag it will say so
            //print("Found Poppy");
            meshBodyText.text = poppy_body.ToString();
            meshCharisText.text = poppy_charisma.ToString();
            meshIntText.text = poppy_intellect.ToString();
            meshWillText.text = poppy_will.ToString();

        }
        else if (character.name == "Mei Lin")
        {
            //sets stats based on tag of potion object
            //if no tag it will say so
            //print("Found Mei Lin");
            meshBodyText.text = meilin_body.ToString();
            meshCharisText.text = meilin_charisma.ToString();
            meshIntText.text = meilin_intellect.ToString();
            meshWillText.text = meilin_will.ToString();

        }
        
    }
    
}
