﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timeline : MonoBehaviour
{

    public CanvasGroup fadeScreen;

    public GameObject DantePortrait;
    public GameObject IndraPortrait;
    public GameObject MeiLinPortrait;
    public GameObject PoppyPortrait;

    public GameObject ResultPortrait;

    public DialogueHandler dialogueHandler;

    //public MySceneLoader sceneLoader;

    public GameObject character;

    public GameObject timeline;
    public GameObject[] weekdays;
    public GameObject[] markers;

    public int daysInWeek = 7;

    string[] stringOfWeekdays = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
    public List<string> nextWeek;
    public GameObject[] portraits;

    //public Flowchart flowchart;
    public int currentDay = 0;
    public bool appDone = false;
    public bool weekDone = false;
    //public bool resultApp = false;

    public static string t = "tutorial";
    public static string d = "Dante";
    public static string i = "Indra";
    public static string m = "Mei Lin";
    public static string p = "Poppy";
    public static string duo = "Duo";
    public static string e = "Empty";
    public static string r = "Results";

    public static string[] act1Schedule = { t, d, i, e, p, m, e, e, p, e, m, d, i, e, e,e,e,e,e, r };
    public static string[] act2Schedule = {};
    public static string[] act3Schedule = { };

    public string[][] acts = { act1Schedule, act2Schedule, act3Schedule };

    public int dayCounter = 0;
    public int characterCounter = 0;

    //checks to see if it's the first version of the controller
    public bool trueOne = false;

    bool timelineSet = false;

    private void Start()
    {
        dontDestroy();

        dialogueHandler = GameObject.FindGameObjectWithTag("GameController").GetComponent<DialogueHandler>();

        setUp();
    }

    /*private void Update()
    {
        print(dayCounter);
    }*/

    void OnSceneLoaded() {
        setUp();
    }

    public void setUp() {
        //sets up neccesary variables
        timeline = GameObject.FindGameObjectWithTag("Timeline");
        dialogueHandler = GameObject.FindGameObjectWithTag("GameController").GetComponent<DialogueHandler>();
        fadeScreen = GameObject.FindGameObjectWithTag("FadeScreen").GetComponent<CanvasGroup>();

        portraits = GameObject.FindGameObjectsWithTag("Portrait");

        foreach (GameObject character in portraits)
        {
            if (character.name == "DantePortrait")
            {
                DantePortrait = character;
            }
            else if (character.name == "IndraPortrait")
            {
                IndraPortrait = character;
            }
            else if (character.name == "MeiLinPortrait")
            {
                MeiLinPortrait = character;
            }
            else if (character.name == "PoppyPortrait")
            {
                PoppyPortrait = character;
            }
            else
            {
                ResultPortrait = character;
            }
        }

        if (character == null)
        {
            character = GameObject.FindWithTag("Character");
        }


        weekdays = GameObject.FindGameObjectsWithTag("Day");
        markers = GameObject.FindGameObjectsWithTag("Marker");

        if (currentDay == 0) {
            setTimeline();
        }

        allMarkersInvisible();

        moveMarker();
    }

    void dontDestroy()
    {
        //makes sure timeline is not destroyed in shift between appointments
        GameObject[] timelineParent = GameObject.FindGameObjectsWithTag("TimelineCanvas");
        DontDestroyOnLoad(timelineParent[0]);
        GameObject[] portraitsInTimeline = GameObject.FindGameObjectsWithTag("Portrait");

        // if there are more than 1 players, then we know that we have more than we need
        // so find the original one
        if (timelineParent.Length > 1)
        {
            foreach (GameObject player in timelineParent)
            {
                
                if (player.GetComponentInChildren<Timeline>().trueOne == false)
                {
                    //Destroy(player);
                    player.SetActive(false);
                }
            }
        }
        else
        {
            trueOne = true; // only first one is true one
        }

        if (portraitsInTimeline.Length > 4)
        {
            foreach (GameObject portrait in portraitsInTimeline)
            {
                if (portrait.GetComponentInChildren<PortraitScript>().activePortaits == false && !(portrait.name == "ResultPortrait"))
                {
                    portrait.SetActive(false);
                }
            }
        }
        else
        {
            foreach(GameObject portrait in portraits)
            { 
         
                portrait.GetComponentInChildren<PortraitScript>().activePortaits = true;
            }
        }

        DontDestroyOnLoad(timelineParent[0]);
    }

    public void endOfApp() {

        //resultApp = false;

        //print("Got here");
            
        dayTracker();
        //setNextApp();
        //moveMarker();

       /*if ((bool)dialogueHandler.story.variablesState["weekDone"] == true) //weekDone needs to be set in Ink scripts ****************************
       {
           print("Ran Timeline");
           clearTimeline();
           setTimeline();
       }*/
        

        //sceneLoader.checkMarker();
    }

    void dayTracker() {
        if ((string)dialogueHandler.story.variablesState["appDone"] == "true" && (string)dialogueHandler.story.variablesState["resultDone"] == "false" /*&& weekdays[currentDay].transform.childCount >= 3*/)
        {
            currentDay += 1;
            dayCounter += 1;
            //moveMarker(); *********************************
            fadeOut();
        }
        else if ((string)dialogueHandler.story.variablesState["appDone"] == "true" && (string)dialogueHandler.story.variablesState["resultDone"] == "true" && weekdays[currentDay].transform.childCount <= 2 && characterCounter < 3) {
            characterCounter += 1;
            currentDay += 1;
            dayCounter += 1;
            //moveMarker(); *********************************
            dialogueHandler.WhoIsNext();
        }
        else {
            characterCounter += 1;
            if (characterCounter == 4)
            {
                if (stringOfWeekdays[currentDay] == "Monday") {
                    dayCounter += 6;
                }
                else if (stringOfWeekdays[currentDay] == "Tuesday")
                {
                    dayCounter += 5;
                }
                else if (stringOfWeekdays[currentDay] == "Wednesday")
                {
                    dayCounter += 4;
                }
                else if (stringOfWeekdays[currentDay] == "Thursday")
                {
                    dayCounter += 3;
                }
                else if (stringOfWeekdays[currentDay] == "Friday")
                {
                    dayCounter += 2;
                }
                else if (stringOfWeekdays[currentDay] == "Saturday")
                {
                    dayCounter += 1;
                }
                timelineSet = false;
                setTimeline();
            }
            else {
                if (act1Schedule[dayCounter] == "Empty")
                {
                    currentDay += 1;
                    dayCounter += 1;
                    moveMarker();
                }
            }
            dialogueHandler.WhoIsNext();
        }
    }


    public void fadeOut()
    {
        //fades canvas group out
        StartCoroutine(FadeCanvasGroup(fadeScreen, fadeScreen.alpha, 1f));
    }

    public void fadeIn()
    {
        //fades canvas group in
        StartCoroutine(FadeCanvasGroup(fadeScreen, fadeScreen.alpha, 0f));
    }

    public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 0.5f)
    {
        dialogueHandler.inputEnabled = false;

        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;

        while (true) {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);

            fadeScreen.alpha = currentValue;

            if (percentageComplete >= 1) break;

            yield return new WaitForEndOfFrame();
        }

        //print("done");
        dialogueHandler.inputEnabled = true;
    }

    public void moveMarker() { 

        allMarkersInvisible();
        markers[currentDay].GetComponent<Image>().enabled = true;

    }

    void allMarkersInvisible() {

        foreach (GameObject obj in markers)
        {
            obj.GetComponent<Image>().enabled = false;
        }
    }

    void resetWeek() {
        currentDay = 0;
        nextWeek.Clear();
    }

    void clearTimeline() {
        GameObject[] portraits = GameObject.FindGameObjectsWithTag("Portrait");

        foreach (GameObject obj in portraits) {
            GameObject.Destroy(obj);
        }
        setTimeline();
    }



    void setTimeline() {
        int counter = 0;
        currentDay = 0;

        nextWeek.Add(acts[dialogueHandler.actNum][dayCounter]);
        nextWeek.Add(acts[dialogueHandler.actNum][dayCounter + 1]);
        nextWeek.Add(acts[dialogueHandler.actNum][dayCounter + 2]);
        nextWeek.Add(acts[dialogueHandler.actNum][dayCounter + 3]);
        nextWeek.Add(acts[dialogueHandler.actNum][dayCounter + 4]);
        nextWeek.Add(acts[dialogueHandler.actNum][dayCounter + 5]);
        nextWeek.Add(acts[dialogueHandler.actNum][dayCounter + 6]);

        /*foreach (string i in nextWeek) {
            print(i);
        }*/

        bool firstApp = true;

        if (timelineSet == false) {
            while (counter < 7)
            {
                if (nextWeek[counter] == d)
                {
                    var GO = DantePortrait;
                    var spawnLocation = GO.transform.parent = weekdays[counter].transform;
                    GO.transform.position = new Vector3(spawnLocation.transform.position.x, (spawnLocation.transform.position.y) + 5, spawnLocation.transform.position.z);

                    if (firstApp == true)
                    {
                        markers[counter].GetComponent<Image>().enabled = true;
                        currentDay += counter;
                        firstApp = false;
                    }

                    counter += 1;
                }
                else if (nextWeek[counter] == i)
                {
                    var GO = IndraPortrait;
                    var spawnLocation = GO.transform.parent = weekdays[counter].transform;
                    GO.transform.position = new Vector3(spawnLocation.transform.position.x, (spawnLocation.transform.position.y) + 5, spawnLocation.transform.position.z);

                    if (firstApp == true)
                    {
                        markers[counter].GetComponent<Image>().enabled = true;
                        currentDay += counter;
                        firstApp = false;
                    }

                    counter += 1;
                }
                else if (nextWeek[counter] == m)
                {
                    var GO = MeiLinPortrait;
                    var spawnLocation = GO.transform.parent = weekdays[counter].transform;
                    GO.transform.position = new Vector3(spawnLocation.transform.position.x, (spawnLocation.transform.position.y) + 5, spawnLocation.transform.position.z);

                    if (firstApp == true)
                    {
                        markers[counter].GetComponent<Image>().enabled = true;
                        currentDay += counter;
                        firstApp = false;
                    }

                    counter += 1;
                }
                else if (nextWeek[counter] == p)
                {
                    var GO = PoppyPortrait;
                    var spawnLocation = GO.transform.parent = weekdays[counter].transform;
                    GO.transform.position = new Vector3(spawnLocation.transform.position.x, (spawnLocation.transform.position.y) + 5, spawnLocation.transform.position.z);

                    if (firstApp == true)
                    {
                        markers[counter].GetComponent<Image>().enabled = true;
                        currentDay += counter;
                        firstApp = false;
                    }

                    counter += 1;
                }
                else if (nextWeek[counter] == r)
                {
                    var GO = ResultPortrait; //********************************* Change Portrait
                    var spawnLocation = GO.transform.parent = weekdays[counter].transform;
                    GO.transform.position = new Vector3(spawnLocation.transform.position.x, (spawnLocation.transform.position.y) + 5, spawnLocation.transform.position.z);

                    if (firstApp == true)
                    {
                        markers[counter].GetComponent<Image>().enabled = true;
                        currentDay += counter;
                        firstApp = false;
                    }

                    counter += 1;
                }
                else
                {
                    counter += 1;
                }
            }
        }
        

        nextWeek.Clear();
        characterCounter = 0;
        timelineSet = true;
    }

}