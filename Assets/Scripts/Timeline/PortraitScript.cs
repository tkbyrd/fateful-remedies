﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortraitScript : MonoBehaviour
{
    public string nextApp = "";
    //public bool checkLoad = false;

    public bool activePortaits = false;

    //public Flowchart flowchart;
    GameObject character;

    //public strings for nextApp
    public string Dante_NextApp;
    public string Indra_NextApp;
    public string MeiLin_NextApp;
    public string Poppy_NextApp;

    private void Start()
    {
        //flowchart = GameObject.FindGameObjectWithTag("Flowchart").GetComponent<Flowchart>();
        if (character == null)
        {
            character = GameObject.FindWithTag("Character");
        }

        getCharacterApp();
    }

    void getCharacterApp()
    {
        if (this.name == "DantePortrait")
        {
            nextApp = Dante_NextApp;
            //print(this + "Got Dante");
            //print(flowchart.GetStringVariable("Dante_NextApp"));
        }
        else if (this.name == "IndraPortrait")
        {
            nextApp = Indra_NextApp;
            //print(this + "Got Indra");
            //print(flowchart.GetStringVariable("Indra_NextApp"));
        }
        else if (this.name == "MeiLinPortrait")
        {
            nextApp = MeiLin_NextApp;
            //print(this + "Got Mei Lin");
            //print(flowchart.GetStringVariable("MeiLin_NextApp"));
        }
        else if (this.name == "PoppyPortrait")
        {
            nextApp = Poppy_NextApp;
            //print(this + "Got Poppy");
            //print(flowchart.GetStringVariable("Poppy_NextApp"));
        }
        else
        {

        }
    }

}
