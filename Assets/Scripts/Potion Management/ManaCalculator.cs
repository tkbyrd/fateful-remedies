﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ManaCalculator: MonoBehaviour
{
    

    //variables for determining total Mana Available
    public int playerMana;
    public int manaInt;
    public bool duoApppointment;
    // boolean for determining if a potion has been given or not
    public bool potionGiven;
    // grabs potion + controller game objects
    public GameObject controller;
    public TextMeshProUGUI manaText;
    public TextMeshProUGUI invisText;
    //strings for comparing variables in script to grabbed vairables variables 
    public string primary;
    public string secondary;
    //for dialogue handler stuff
    DialogueHandler dialogueHandler;


    // Start is called before the first frame update
    void Start()
    {
        
        if (controller == null)
        {
            controller = GameObject.FindWithTag("GameController"); // gets the gamecontroller to manage the scripts
        }
        dialogueHandler = controller.GetComponent<DialogueHandler>(); //gets the dialogue handler for
        // Find the matching text UI components
        if (manaText == null)
        {
            manaText = GameObject.FindWithTag("Mana").GetComponent<TMPro.TextMeshProUGUI>();
        }
        if (invisText == null)
        { 
            invisText = GameObject.Find("Invis").GetComponent<TMPro.TextMeshProUGUI>();
        }
       //Take care of the rest here
       SetTextUIMana();
       invisText.text = " ";
    }
    public void SetTextUIMana()
    {
        // Save new current mana in Text UI

        if (manaText == null)
        {
            manaText = GameObject.FindWithTag("Mana").GetComponent<TMPro.TextMeshProUGUI>(); // the visual indicator for the mana the player has left
        }

        manaText.text = " "; // clears current text to make way for the new
        manaText.text = playerMana.ToString(); //sets the mana text equal to the current player mana
    }
    

    public void SubtractTextUIMana(int subtractMana)
    {

        if (manaText == null)
        {
            manaText = GameObject.FindWithTag("Mana").GetComponent<TMPro.TextMeshProUGUI>();
        }

        // Get the string stored in it and convert to an int
        manaInt = int.Parse(manaText.text);
        //subtract only if manaInt is greater than subtracted Mana
        if (manaInt < subtractMana)
        {
            // just resets the manaInt to mana text
            //lets the Player mana version manipulate the invisible text
            manaInt = int.Parse(manaText.text);
            manaText.text = manaInt.ToString();
        }
        else
        {
            // Increment accordingly
            manaInt -= subtractMana;

            // Save new current mana in Text UI
            manaText.text = manaInt.ToString();
        }
    }

    public int subtractPlayerMana(int subtractMana)
    {

        //Function subtracts player mana when activated
        // does not subtract if subtract mana is greater than player mana

        if (invisText == null)
        {
            invisText = GameObject.Find("Invis").GetComponent<TMPro.TextMeshProUGUI>();
        }

        if (playerMana < subtractMana)
        {
            //lets player know they don't have enough
            invisText.text = " ";
            invisText.text = "Not Enough Mana";
            return playerMana;
        }
        else
        {
            invisText.text = " ";
            playerMana -= subtractMana;
            return playerMana;
        }
    }

    public void onePerCustomer()
    {
        //lets player know that they already gave the customer a potion
        //prevents player from using all their mana on one customer
        invisText.text = " ";
        invisText.text = "Potion Already Given";
    }
    public void noMoMana()
    {
        //lets player know that they already gave the customer a potion
        //prevents player from using all their mana on one customer
        invisText.text = " ";
        invisText.text = "Not Enough Mana";
    }

    public void customerNotDoneYet()
    {
        //lets player know that the customer isn't finished yet
        //prevents player from selecting Potions while problem is being explained.
        invisText.text = " ";
        invisText.text = "Not Enough Mana";
    }

    public void resetManaLeft()
    {
        
        //Debug.Log("Reset Mana Left Activated");
        //depending on the act the maximum amount of mana will change
        var actNumber = dialogueHandler.actNum;
        if(actNumber == 0)
        {
            playerMana = 200;
            manaInt = playerMana;
            manaText.text = manaInt.ToString();
        }
        if (actNumber == 1)
        {
            playerMana = 175;
            manaInt = playerMana;
            manaText.text = manaInt.ToString();
        }
        if (actNumber == 2)
        {
            playerMana = 135;
            manaInt = playerMana;
            manaText.text = manaInt.ToString();
        }
        
        
    }

    public int checkPlayerMana()
    {
        return playerMana;
    }
    
}
