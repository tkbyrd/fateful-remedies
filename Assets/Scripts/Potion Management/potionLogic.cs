﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class potionLogic : MonoBehaviour
{
    //potion's primary stat for 
    //simple name because primary is indicated from the beginning
    public int primaryStat;

    // secondary stat needs better labling for when adding onto character stats happens
    public int secondaryStat;

    //strings for comparing variables in script to grabbed vairables variables 
    public string primary;
    public string secondary;

    //Variable for holding mana cost
    public int manaCost = 0;


    //character array
    public GameObject[] characters = new GameObject[4];
    //characters to manipulate
    public GameObject characterA;
    public GameObject characterB;
    public GameObject characterC;
    public GameObject characterD;
    //actor scripts
    Actor actorScriptA;
    Actor actorScriptB;
    Actor actorScriptC;
    Actor actorScriptD;
    //object and script list
    List<GameObject> characterList;
    List<Actor> actorScriptList;

    //replacement bool for potion variables
    public bool bodyGiven;
    public bool intGiven;
    public bool charGiven;
    public bool willGiven;

    //object references
    public GameObject controller;
    public GameObject character;
    Actor actorScript;
    ManaCalculator manaCalculator;
    SetStatText statTextSetter;
    public TextMeshProUGUI potionTypeTextUIComp;
    public TextMeshProUGUI potionCostTextUIComp;
    AudioScript audioScript;
    AudioSource audioSource;
    DialogueHandler dialogueHandler;
    Director director;


    string target = "";
    string body_str_var = "";
    string int_str_var = "";
    string char_str_var = "";
    string will_str_var = "";
    //stats we want to change

    private int character_body = 0;
    private int character_charisma = 0;
    private int character_intellect = 0;
    private int character_will = 0;

    //duo appointment boolean
    bool isDuoAppointment;

    //booleans
    bool canGivePotionAgain;
    bool potionHasBeenGiven;

    public int customersInShop;

    void Start()
    {
        potionHasBeenGiven = false;
        canGivePotionAgain = false;

        dialogueHandler = GameObject.FindGameObjectWithTag("GameController").GetComponent<DialogueHandler>();

        bodyGiven = false;
        intGiven = false;
        charGiven = false;
        willGiven = false;
        if (controller == null)
        {
            controller = GameObject.FindWithTag("GameController");
        }
        if (character == null)
        {
            character = GameObject.FindWithTag("Character");
        }
        if (actorScript == null)
        {
            actorScript = character.GetComponent<Actor>();
        }
        if (characters == null)
        {
            characters = GameObject.FindGameObjectsWithTag("Character");
        }
        customersInShop = characters.Length;

        //grabs the mana calculator from the game controller
        statTextSetter = GameObject.FindGameObjectWithTag("Stats Tab").GetComponent<SetStatText>();
        //grabs the random potion maker from the game controller
        manaCalculator = controller.GetComponent<ManaCalculator>();
        audioScript = controller.GetComponent<AudioScript>();
        director = controller.GetComponent<Director>();
        SetPotionTypeText();
        SetPotionCostText();

        audioSource = GetComponent<AudioSource>();
        dialogueHandler.isOrIsNotTutorial();
    }


    void OnMouseDown()
    {
        // this object was clicked - Set stats to character
        //currently not working
        setStatsToCharacter();
        Destroy(this.gameObject);
    }


    public void SetPotionTypeText()
    {
        // Find the matching text UI component
        var potionTypeTextUIComp = this.transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>(); //GameObject.Find(nameFinder).GetComponent<TMPro.TextMeshProUGUI>();
        potionTypeTextUIComp.text = " ";
        if ((secondaryStat != 0) && (this.tag == "Will"))
        {
            potionTypeTextUIComp.text = "Tranquility:" + primaryStat + "/" + secondary + ": " + secondaryStat;
            potionTypeTextUIComp.text = potionTypeTextUIComp.text.Replace("/", System.Environment.NewLine);
        }
        else if ((secondaryStat != 0) && (this.tag != "Will") && (secondary != "Will"))
        {
            potionTypeTextUIComp.text = primary + ": " + primaryStat + "/" + secondary + ": " + secondaryStat;
            potionTypeTextUIComp.text = potionTypeTextUIComp.text.Replace("/", System.Environment.NewLine);
        }
        else if ((secondaryStat != 0) && (this.tag != "Will") && (secondary == "Will"))
        {
            potionTypeTextUIComp.text = primary + ": " + primaryStat + "/" + "Tranquility:" + secondaryStat;
            potionTypeTextUIComp.text = potionTypeTextUIComp.text.Replace("/", System.Environment.NewLine);
        }
        else if ((secondaryStat == 0) && (this.tag == "Will"))
        {
            potionTypeTextUIComp.text = "Tranquility:" + primaryStat;
        }
        else
        {
            potionTypeTextUIComp.text = primary + ": " + primaryStat;
        }
        //potionTypeTextUIComp.text = primary + ":" + primaryStat + "/" + secondary + ":" + secondaryStat;
        //potionTypeTextUIComp.text = potionTypeTextUIComp.text.Replace("/", System.Environment.NewLine);
    }
    public void failSafeSetPotionTypeText()
    {
        //occurs when multiple of the same potion type appear
        //helps set those potions names

    }
    
   

    public void SetPotionCostText()
    {
        // Find the matching text UI component
        var potionCostTextUIComp = this.transform.GetChild(1).GetComponent<TMPro.TextMeshProUGUI>(); //GameObject.Find(costFinder).GetComponent<TMPro.TextMeshProUGUI>();

        potionCostTextUIComp.text = " ";
        potionCostTextUIComp.text = "Costs: " + manaCost;
    }




    // when called, potion will copy its stats over to the character's stats by utilizing te  
    public void setStatsToCharacter()
    {

        dialogueHandler.OnClickChoiceButton(this.gameObject);
        if (character.name == "Indra")
        {
            target = "indra";
            body_str_var = target + "_Body";
            int_str_var = target + "_Intellect";
            char_str_var = target + "_Charisma";
            will_str_var = target + "_Will";
            whichGiveFunction();
        }
        else if (character.name == "Dante")
        {
            target = "dante";
            body_str_var = target + "_Body";
            int_str_var = target + "_Intellect";
            char_str_var = target + "_Charisma";
            will_str_var = target + "_Will";

            whichGiveFunction();

        }
        else if (character.name == "Mei Lin")
        {
            target = "meilin";
            body_str_var = target + "_Body";
            int_str_var = target + "_Intellect";
            char_str_var = target + "_Charisma";
            will_str_var = target + "_Will";

            whichGiveFunction();

        }
        else if (character.name == "Poppy" ||character.name == "Zinnia" || character.name == "Poppy and Zinnia")
        {
            print("Found Poppy");
            target = "poppy";
            body_str_var = target + "_Body";
            int_str_var = target + "_Intellect";
            char_str_var = target + "_Charisma";
            will_str_var = target + "_Will";

            whichGiveFunction();

        }
        
    }
    public bool checkIfCustomerListIsNotComplete()
    {
        bool resetter = false;
        foreach (Actor actorScript in actorScriptList)
        {
            if (actorScript.haveIGottenMyPotionYet == false)
            {
                resetter = false;
                switchTargetActor();
            }
            else
            {
                resetter = true;
            }
        }
        return resetter;
    }

    public void switchTargetActor()
    {
        //makes sure we aren't altering the same actor twice
        //loops through actor list and switches current actor 
        //to whoever is next on the list that hasn't gotten a potion yet.
        for (int i = 0; i < characters.Length; i++)
        {
            bool servedCustomer = actorScriptList[i].haveIGottenMyPotionYet;
            if (servedCustomer == false)
            {
                character = characterList[i];
                break;
            }
        }
    }
    void setCharacterObjects(int numberOfCustomers)
    {
        //sets the character references depending on number of characters in scene

        //switch statement that changes the list size depending on the number of characters in the scene

        switch (customersInShop)
        {
            case 1:
                characterList = new List<GameObject> { characterA };
                actorScriptList = new List<Actor> { actorScriptA };
                break;
            case 2:
                characterList = new List<GameObject> { characterA, characterB };
                actorScriptList = new List<Actor> { actorScriptA, actorScriptB };
                break;
            case 3:
                characterList = new List<GameObject> { characterA, characterB, characterC };
                actorScriptList = new List<Actor> { actorScriptA, actorScriptB, actorScriptC };
                break;
            default:
                characterList = new List<GameObject> { characterA, characterB, characterC, characterD };
                actorScriptList = new List<Actor> { actorScriptA, actorScriptB, actorScriptC, actorScriptD };
                break;

        }
        for (int i = 0; i < characterList.Count; i++)
        {
            characterList[i] = characters[i];

            actorScriptList[i] = characters[i].GetComponent<Actor>();
        }


    }

    public void whichGiveFunction()
    {
        //determines which give logic will be used based on number of characters
        //if else statement will select based on customers in shop 
        if (customersInShop == 1)
        {
            repeatCustomer();
        }
        else
        {
            multipleCustomers();
        }
    }

    void multipleCustomers()
    {

        var potionDeterminer = potionHasBeenGiven;
        var manaDeterminer = manaCalculator.checkPlayerMana();
        canGivePotionAgain = checkIfCustomerListIsNotComplete();
        if (canGivePotionAgain == true)
        {
            potionDeterminer = true;
        }
        else
        {
            potionDeterminer = false;
        }
        switch (potionDeterminer)
        {
            case true:
                if ((manaDeterminer > 0) && (manaDeterminer >= manaCost))
                {
                    potionHasBeenGiven = true;
                    //potionGiven();
                }
                else if ((manaDeterminer > 0) && (manaDeterminer < manaCost))
                {
                    manaCalculator.noMoMana();
                }
                else if ((manaDeterminer <= 0) && (manaDeterminer < manaCost))
                {
                    manaCalculator.noMoMana();
                }
                break;

            default:
                manaCalculator.onePerCustomer();
                break;
        }

    }

    void potionGiven()
    {
        //sets stats based on tag of potion object
        //if no tag it will say so
        if (this.tag == "Body")
        {
            character_body += primaryStat; //Sets body stat first

            if (secondary == "Intellect")
                character_intellect += secondaryStat; // sets intellect stat second if set to that
            else if (secondary == "Charisma")
                character_charisma += secondaryStat; // sets charisma stat second if set to that
            else if (secondary == "Will")
                character_will += secondaryStat; // sets will stat second if set to that

        }
        else if (this.tag == "Intellect")
        {
            character_intellect += primaryStat;

            if (secondary == "Body")
                character_body += secondaryStat;
            else if (secondary == "Charisma")
                character_charisma += secondaryStat;
            else if (secondary == "Will")
                character_will += secondaryStat;
        }
        else if (this.tag == "Charisma")
        {
            character_charisma += primaryStat;

            if (secondary == "Body")
                character_body += secondaryStat;
            else if (secondary == "Intellect")
                character_intellect += secondaryStat;
            else if (secondary == "Will")
                character_will += secondaryStat;
        }
        else if (this.tag == "Will")
        {
            character_will += secondaryStat;

            if (secondary == "Body")
                character_body += secondaryStat;
            else if (secondary == "Intellect")
                character_intellect += secondaryStat;
            else if (secondary == "Charisma")
                character_charisma += secondaryStat;
        }

        else
        {
            print("No tag detected");
        }
        //checks if mana is present so not to break scenes without manacalculator
        //inside of the controller
        if (manaCalculator != null)
        {
            manaCalculator.subtractPlayerMana(manaCost);
            manaCalculator.SubtractTextUIMana(manaCost);
        }
        //immediately sets the customer's stats after they're set.
        actorScript.Body = character_body;
        actorScript.Intellect = character_intellect;
        actorScript.Charisma = character_charisma;
        actorScript.Will = character_will;

        director.updateMasterStats();

        statTextSetter.getStatsForChar();

        //play the potion collection sound
        audioSource.Play();
    }
    void repeatCustomer()
    {
        //checks potion given variable before moving forward
        //if potion has been given already, invis text lets the player know
        //if potion has not been given yet, proceed to potionGiven
        var potionDeterminer = potionHasBeenGiven;
        var manaDeterminer = manaCalculator.checkPlayerMana();
        if ((potionDeterminer == false) && (manaDeterminer > 0) && (manaDeterminer >= manaCost))
        {
            if (this.tag == "Body")
            {
                //prevents scipt from breaking if manacalculator isn't present
                bodyGiven = true;

            }
            else if (this.tag == "Intellect")
            {
                //prevents scipt from breaking if manacalculator isn't present
                intGiven = true;

            }
            else if (this.tag == "Charisma")
            {
                //prevents scipt from breaking if manacalculator isn't present
                charGiven = true;

            }
            else if (this.tag == "Will")
            {
                //prevents scipt from breaking if manacalculator isn't present
                willGiven = true;
            }
            potionHasBeenGiven = true;
            potionGiven();
        }
        else if ((potionDeterminer == false) && (manaDeterminer > 0) && (manaDeterminer < manaCost))
        {

            manaCalculator.noMoMana();

        }
        else if ((potionDeterminer == false) && (manaDeterminer <= 0) && (manaDeterminer < manaCost))
        {

            manaCalculator.noMoMana();

        }
        else
        {

            manaCalculator.onePerCustomer();

        }
    }
}
