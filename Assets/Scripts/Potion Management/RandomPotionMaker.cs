﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPotionMaker : MonoBehaviour
{
    List<string> list = new List<string>();

    // Start is called before the first frame update
    //Will add Potion variables to list when game first statrs
    //starts potion constructor function
    public GameObject Body;         // body potion object
    public GameObject Intellect;    // intellect potion object
    public GameObject Charisma;     // charisma potion object
    public GameObject Will;         // will potion object
    DialogueHandler dialogueHandler;
    Tags Tags;
    potionLogic bodyPotion;
    potionLogic intellectPotion;
    potionLogic willPotion;
    potionLogic charismaPotion;
    //ManaCalculator manaCalculator;
    public GameObject Controller;         // Controller potion object

    public int primaryStat = 0;         //first potion stat
    public int secondaryStat = 0;       //second potion stat

    public int oneOfEach = 0;  //for playtesting the one of each potion thing. May or May Not stay depending on what the team wants.

    public string primary;          //first potion variable from list
    public string secondary;        //second potion variable from list

    int primeIndex;                 //first potion index to be selected
    int secondIndex;                //second potion index to be selected

    int primaryMin = 1;             //min stat boost for primary
    int primaryMax = 3;             //max stat boost for primary

    int secondaryMin = 1;           //min stat boost for secondary
    int secondaryMax = 3;           //max stat boost for secondary

    int manaCost = 0;               //mana cost for potion

    public bool intialed = false;


    public void Start()
    {
        if (intialed == false) {
            Intialize();
        }
    }

    void Intialize() {
        Tags = Controller.GetComponent<Tags>();
        //adds tags to list
        list.Add(Tags.body);
        list.Add(Tags.intellect);
        list.Add(Tags.will);
        list.Add(Tags.charisma);
        //adds shortcuts to get potionLogic components
        bodyPotion = Body.GetComponent<potionLogic>();
        intellectPotion = Intellect.GetComponent<potionLogic>();
        willPotion = Will.GetComponent<potionLogic>();
        charismaPotion = Charisma.GetComponent<potionLogic>();
        //manaCalculator = GameObject.FindWithTag("GameController").GetComponent<ManaCalculator>();
        backToZero();
        if (oneOfEach > 4)
        {
            oneOfEach = 0;
        }
        dialogueHandler = GameObject.FindGameObjectWithTag("GameController").GetComponent<DialogueHandler>();
        intialed = true;

        //randomConstructor();
    }

    public void randomConstructor()
    {
        if (intialed == false)
        {
            Intialize();
        }

        //Function will select variables from the list
        // Will distribute values depending on the stat selected
        
        primeIndex = oneOfEach;// sets the primary index equal to an incrementing variable
        oneOfEach += 1;
        secondIndex = Random.Range(0, 4);

        primary = list[primeIndex];// grabs main potion stat for potion determination (Ex: Body)
        secondary = list[secondIndex]; // grabs secondary potion stat for potion determination (Ex:Charisma)
        

        primaryStat = Random.Range(primaryMin, primaryMax); // how much potion alters primary stat
        secondaryStat = Random.Range(secondaryMin, secondaryMax); // how much potion alters secondary stat
        if (primary == secondary)
        {
            primaryStat += secondaryStat; //if primary and secondary potion stats are the same (Ex: Body/Body) it just adds together
            secondaryStat = 0;
        }
    }

    public void backToZero()
    {
        if (primaryStat > 0 || secondaryStat > 0)
        {
            bodyPotion.secondaryStat = 0;
            intellectPotion.secondaryStat = 0;
            charismaPotion.secondaryStat = 0;
            willPotion.secondaryStat = 0;
        }
    }

    public void potionSetter()
    {
        // function sets potion variables depending on the primary and secondary potion stat
        //ends by activating mana var 
        if (primary == "Body") //will set the tags and stats of the body potion if the primary tag is body
        {
            bodyPotion.primaryStat = primaryStat;
            bodyPotion.primary = primary;

            bodyPotion.secondaryStat = secondaryStat;
            bodyPotion.secondary = secondary;

            bodyPotion.manaCost = manaCalculator();

        }
        else if (primary == "Intellect")
        {
            intellectPotion.primaryStat = primaryStat;
            intellectPotion.primary = primary;

            intellectPotion.secondaryStat = secondaryStat;
            intellectPotion.secondary = secondary;

            intellectPotion.manaCost = manaCalculator();

        }
        else if (primary == "Charisma")
        {
            charismaPotion.primaryStat = primaryStat;
            charismaPotion.primary = primary;

            charismaPotion.secondaryStat = secondaryStat;
            charismaPotion.secondary = secondary;

            charismaPotion.manaCost = manaCalculator();

        }
        else if (primary == "Will")
        {
            willPotion.primaryStat = primaryStat;
            willPotion.primary = primary;

            willPotion.secondaryStat = secondaryStat;
            willPotion.secondary = secondary;

            willPotion.manaCost = manaCalculator();
        }

    }

    public int manaCalculator()
    {
        
        if (secondaryStat == 0)
        {
            if(primaryStat >= 1 || primaryStat <= 2)
            {
                manaCost = Random.Range(25,45);
            }
            else if (primaryStat >=3 || primaryStat <= 4)
            {
                manaCost = Random.Range(40, 60);
            }
            else if (primaryStat >= 5 || primaryStat <=6)
            {
                manaCost = Random.Range(55, 75);
            }
        }
        else
        {
            if (primaryStat == 1)
            {
                manaCost = Random.Range(25, 45);
            }
            else if (primaryStat == 2)
            {
                manaCost = Random.Range(40, 60);
            }
            else if (primaryStat == 3)
            {
                manaCost = Random.Range(55, 75);
            }
        }
        return manaCost;
    }


}
