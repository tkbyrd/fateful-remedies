﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class givePotion : MonoBehaviour
{

    public GameObject slot1;            //GO of inventory slot1
    public GameObject slot2;            //GO of inventory slot2
    public GameObject slot3;            //GO of inventory slot3
    public GameObject slot4;            //GO of inventory slot4

    public potionLogic potionLogic;

    /*
    // Start is called before the first frame update
    public GameObject Slot;
    public GameObject child;
    potionLogic slotPotion;

    void Start()
    {
        //finds the slots
        if(Slot == null)
        {
            //finds the child
        }
        

        //adds shortcuts to get potionLogic components of child
        slotPotion = Slot1.GetComponent<potionLogic>();
        


        //randomConstructor();
    }
    */

    public void Start()
    {
        //Finds slots in scene
        slot1 = GameObject.FindGameObjectWithTag("Slot1");
        slot2 = GameObject.FindGameObjectWithTag("Slot2");
        slot3 = GameObject.FindGameObjectWithTag("Slot3");
        slot4 = GameObject.FindGameObjectWithTag("Slot4");
    }

    //accesses the potions within the slot
    //to utilize their script and have the player
    //set the characters stats

    private void OnMouseDown()
    {
        give();
    }

    public void give()
    {
        //checks when player presses down on mouse key and if they clicked on an object
        //Once they click an object, it will check to see if that object is valid and go through the
        //appropriate steps

        if (Input.GetMouseButtonDown(0)) {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

            //checks to see if object has one of the correct tags and then runs its setStatsToCharacter function
            if (hit.collider.gameObject.tag == "Body" || hit.collider.gameObject.tag == "Charisma") {
                if (hit.collider.gameObject.tag == "Intellect" || hit.collider.gameObject.tag == "Will") {
                    potionLogic = hit.collider.gameObject.GetComponent<potionLogic>();
                    potionLogic.setStatsToCharacter();
                }
            }
        }
    }
}
