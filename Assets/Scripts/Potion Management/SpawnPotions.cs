﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SpawnPotions : MonoBehaviour
{
    public int numOfPotions;        //number of potions
    public int maxPotions;          //max number of potions

    public GameObject body;             //GO of body potion visual
    public GameObject charisma;         //GO of charisma potion visual
    public GameObject will;             //GO of will potion visual
    public GameObject intellect;        //GO of intellect potion visual

    public GameObject slot1;            //GO of inventory slot1
    public GameObject slot2;            //GO of inventory slot2
    public GameObject slot3;            //GO of inventory slot3
    public GameObject slot4;            //GO of inventory slot4

    public List<GameObject> emptySlots = new List<GameObject>();
    RandomPotionMaker potionMaker;
    DialogueHandler dialogueHandler;

    //booleon checks whether or not this is the tutorial scene or not.
    //if true it should be set to false after tutorial is over.
    public bool isTutorial = false;


    // Start is called before the first frame update
    public void Start()
    {
        //Grabs RandomPotionMaker script form GameController GO
        potionMaker = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<RandomPotionMaker>();
        dialogueHandler = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<DialogueHandler>();
        if(isTutorial == true)
        {
            numOfPotions = 0;
            maxPotions = 1;
            tutorialGetEmptySlots();
        }
        else
        {
            numOfPotions = 0;
            maxPotions = 4;
            GetEmptySlots();
        }
        
    }
    void tutorialGetEmptySlots()
    {
        //function activates during tutorial and finds the first slot only.
        slot1 = GameObject.FindGameObjectWithTag("Slot1");
        if (slot1.transform.childCount < 1)
        {
            emptySlots.Add(slot1);
            //print("Slot 1 added");
            if (numOfPotions < maxPotions)
            {
                numOfPotions += 1;
            }
        }
    }

    void GetEmptySlots() {
        
        //Finds slots in scene
        slot1 = GameObject.FindGameObjectWithTag("Slot1");
        slot2 = GameObject.FindGameObjectWithTag("Slot2");
        slot3 = GameObject.FindGameObjectWithTag("Slot3");
        slot4 = GameObject.FindGameObjectWithTag("Slot4");

        //adds empty slots to emptySlots list
        if (slot1.transform.childCount < 1)
        {
            emptySlots.Add(slot1);
            //print("Slot 1 added");
            if (numOfPotions < maxPotions)
            {
                numOfPotions += 1;
            }
        }
        if (slot2.transform.childCount < 1)
        {
            emptySlots.Add(slot2);
            //print("Slot 2 added");
            if (numOfPotions < maxPotions)
            {
                numOfPotions += 1;
            }
        }
        if (slot3.transform.childCount < 1)
        {
            emptySlots.Add(slot3);
            //print("Slot 3 added");
            if (numOfPotions < maxPotions)
            {
                numOfPotions += 1;
            }
        }
        if (slot4.transform.childCount < 1)
        {
            emptySlots.Add(slot4);
            //print("Slot 4 added");
            if (numOfPotions < maxPotions)
            {
                numOfPotions += 1;
            }
        }
    }

    public void PotionSpawn()
    {
        if(isTutorial == false)
        {
            GetEmptySlots();
        }
        else
        {
            tutorialGetEmptySlots();
        }
        while (numOfPotions > 0)
        {
            //Grabs RandomPotionMaker script form GameController GO
            potionMaker = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<RandomPotionMaker>();

            //checks how many potions are avaiable to the player and fills empty slots in inventory
            potionMaker.randomConstructor();
            potionMaker.potionSetter();

            //not getting to these breakpoints *********************************************
            if (body.CompareTag(potionMaker.primary))
            {
                var GO = Instantiate(body);
                var spawnLocation = GO.transform.parent = emptySlots[0].transform;
                GO.transform.position = new Vector3(spawnLocation.transform.position.x, spawnLocation.transform.position.y, spawnLocation.transform.position.z);
                //var newPotion = Instantiate(GO, new Vector3(0, 0, 0), Quaternion.identity);
                //var newPotion = Instantiate(GO, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
                emptySlots.Remove(emptySlots[0]);
                numOfPotions -= 1;
            }

            if (will.CompareTag(potionMaker.primary))
            {
                var GO = Instantiate(will);
                var spawnLocation = GO.transform.parent = emptySlots[0].transform;
                GO.transform.position = new Vector3(spawnLocation.transform.position.x, spawnLocation.transform.position.y, spawnLocation.transform.position.z);
                //var newPotion = Instantiate(GO, new Vector3(0, 0, 0), Quaternion.identity);
                //var newPotion = Instantiate(GO, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
                emptySlots.Remove(emptySlots[0]);
                numOfPotions -= 1;
            }

            if (charisma.CompareTag(potionMaker.primary))
            {
                var GO = Instantiate(charisma);
                var spawnLocation = GO.transform.parent = emptySlots[0].transform;
                GO.transform.position = new Vector3(spawnLocation.transform.position.x, spawnLocation.transform.position.y, spawnLocation.transform.position.z);
                //var newPotion = Instantiate(GO, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
                //var newPotion = Instantiate(GO, new Vector3(0, 0, 0), Quaternion.identity);
                emptySlots.Remove(emptySlots[0]);
                numOfPotions -= 1;
            }

            if (intellect.CompareTag(potionMaker.primary))
            {
                var GO = Instantiate(intellect);
                var spawnLocation = GO.transform.parent = emptySlots[0].transform;
                GO.transform.position = new Vector3(spawnLocation.transform.position.x, spawnLocation.transform.position.y, spawnLocation.transform.position.z);
                //var newPotion = Instantiate(GO, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
                //var newPotion = Instantiate(GO, new Vector3(0,0,0), Quaternion.identity);
                emptySlots.Remove(emptySlots[0]);
                numOfPotions -= 1;
            }
            //print(numOfPotions);
        }
    }
    
}