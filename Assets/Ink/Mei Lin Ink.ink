->MEILIN_APP1_INTRO

VAR currentKnot = "none"
VAR nextKnot = "none"
VAR currentEmotion = "none"
VAR appDone = "false"
VAR resultDone = "false"
VAR canChoiceBeMade = "no"
VAR weekDone = "false"

==MEILIN_APP1_INTRO==

~ currentKnot = "MEILIN_APP1_INTRO"
~ resultDone = "false"
~currentEmotion = "neutral"
Hello, I’m Mei Lin. I brought you a bouquet from my flower shop. It’s just around the corner, we’ve been in business for over forty years.

I’ve lived in this neighborhood my whole life, so if you ever need anything at all you can come to me or my wife, Anya.

I can’t wait to get to know you better! You must lead such an interesting life as an apothecary. I can’t imagine all the wonderful places you’ve been.

I would love to hear what kinds of plants you work with sometime!

Oh, I got so excited I almost forgot, I need to ask you for something for my joints. I’ve been having pain, especially in my hands, that has been interfering with my ability to garden and knit.

I’d love to try one of your potions. Do you have one that can soothe these old bones?


~ nextKnot = "APP1_Con"

->APP1_Con

==APP1_Con==

~ currentKnot = "APP1_Con"
~ canChoiceBeMade = "yes"

*   This body potion should ease the bones...
    ~ currentEmotion = "happy"
    Thank you! What a great addition you’ll be to the community! /*happy*/
    ~ nextKnot = "MEILIN_APP1_RESULT_A"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   I guess I do need to think about things clearly!
    ~ currentEmotion = "happy"
    Thank you! What a great addition you’ll be to the community! /*happy*/
    ~ nextKnot = "MEILIN_APP1_RESULT_B"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Maybe Tranquility is what I need! is what I need!
    ~ currentEmotion = "happy"
    Thank you! What a great addition you’ll be to the community! /*happy*/
    ~ nextKnot = "MEILIN_APP1_RESULT_C"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A boost in charisma this time then?
    ~ currentEmotion = "happy"
    Thank you! What a great addition you’ll be to the community! /*happy*/
    ~ nextKnot = "MEILIN_APP1_RESULT_D"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[This body potion should ease the bones...: Appointment 1 Result A]
[I guess I do need to think about things clearly!: Appointment 1 Result B]
[Maybe Tranquility is what I need!: Appointment 1 Result C]
[A boost in charisma this time then?: Appointment 1 Result D]
*/


==MEILIN_APP1_RESULT_A==

~ currentKnot = "MEILIN_APP1_RESULT_A"
~ currentEmotion = "none"
~ appDone = "true"



I’ve been moving around so easily since I took your potion! I haven’t felt any pain at all.  /*happy*/

I’ve finally been able to get back in the garden and I still feel well enough to make progress on my knitting.

I can’t wait to tell everyone how wonderful your potions are!

~ weekDone = "true"
~ nextKnot = "MEILIN_APP2"
~ resultDone = "true"

->END

==MEILIN_APP1_RESULT_B==

~ currentKnot = "MEILIN_APP1_RESULT_B"
~ currentEmotion = "none"
~ appDone = "true"

~ currentEmotion = "happy"
Hello again! I devised a method for gardening that is less strenuous on my joints. 

Thank you for providing me the inspiration through your potion! 

I’m feeling some relief at the end of the day, definitely glad to have less pain.

I’ll be sure to bring some beautiful flowers soon!


~ weekDone = "true"
~ nextKnot = "MEILIN_APP2"
~ resultDone = "true"

->END


==MEILIN_APP1_RESULT_C==

~ currentKnot = "MEILIN_APP1_RESULT_C"
~ currentEmotion = "none"
~ appDone = "true"

Hello again. Your potion did not seem to do very much for me this time. My joints are still aching, but I’ve been able to keep my mind off it.

I’m still not able to give my work and hobbies as much attention as I would like, but I’ve managed to make some progress with my knitting.


~ weekDone = "true"
~ nextKnot = "MEILIN_APP2"
~ resultDone = "true"

->END

==MEILIN_APP1_RESULT_D==

~ currentKnot = "MEILIN_APP1_RESULT_D"
~ currentEmotion = "none"
~ appDone = "true"

~ currentEmotion = "neutral"
Hello again.  I’m still moving slow, but I can’t neglect my gardening. Though I appreciate you trying to help with your potions. 

A few neighborhood kids stopped by while I worked, and I was able to convince them to assist me in pulling weeds with a friendly competition. 


~ currentEmotion = "happy"
The winner brought home a free bouquet of flowers for their mom. These kids are really something. 

Normally they’re so busy running around they don’t give time to old ladies, but they seemed to have fun. 



~ weekDone = "true"
~ nextKnot = "MEILIN_APP2"
~ resultDone = "true"

->END




==MEILIN_APP2==

~ currentKnot = "MEILIN_APP2"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "none"
~ weekDone = "false"

~ currentEmotion = "happy"
Good morning. I hope you’ve been able to settle in and feel at home in our city. 

~ currentEmotion = "nervous"

I’m leading a community initiative, but it hasn’t gotten as much traction as I hoped it would. 


This neighborhood lacks a communal gathering place. I believe I can provide this with a community garden. 

Not many people in the city know how to garden, and it is such a fulfilling pastime, as well as a nice change of pace from bustling city life.

Tomorrow is the opening day for the garden. There is an event to begin planting, but no one has indicated they will come. 
~ currentEmotion = "neutral"

Do you think you could help me find a way to get more people interested?



~ nextKnot = "APP2_Con"

->APP2_Con

==APP2_Con==

~ currentKnot = "APP2_Con"
~ canChoiceBeMade = "yes"

*   This body potion should ease the bones...
    ~ currentEmotion = "happy"
    Thank you! Let me know if there is anything you need to brew your potions! /*happy*/  
    ~ nextKnot = "MEILIN_APP2_RESULT_A" 

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   I guess I do need to think about things clearly!
    ~ currentEmotion = "happy"
    Thank you! Let me know if there is anything you need to brew your potions! /*happy*/
    ~ nextKnot = "MEILIN_APP2_RESULT_B"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Maybe Tranquility is what I need!
    ~ currentEmotion = "happy"
    Thank you! Let me know if there is anything you need to brew your potions!  /*happy*/
    ~ nextKnot = "MEILIN_APP2_RESULT_C"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A boost in charisma this time then?    
    ~ currentEmotion = "happy"
    Thank you! Let me know if there is anything you need to brew your potions! /*happy*/
    ~ nextKnot = "MEILIN_APP2_RESULT_D"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[This body potion should ease the bones...: Appointment 2 Result A]
[I guess I do need to think about things clearly!: Appointment 2 Result B]
[Maybe Tranquility is what I need!: Appointment 2 Result C]
[A boost in charisma this time then?: Appointment 2 Result D]
*/


==MEILIN_APP2_RESULT_A==

~ currentKnot = "MEILIN_APP2_RESULT_A"
~ currentEmotion = "none"
~ nextKnot = "MEILIN_APP3"
~ appDone = "true"

~ currentEmotion = "happy"
Your potion made me feel decades younger! When no one showed up for opening day, I decided not to wait around any longer and tackle the garden myself. /*happy*/

 ~ currentEmotion = "mad"
 Unfortunately, your potion couldn’t last forever. My strength wore out, so I was only able to get half of the garden planted. /*mad*/

  ~ currentEmotion = "neutral"
It’s still more than I would have been able to accomplish if I didn’t have you help. /*neutral*/

~ currentEmotion = "happy"
But this was a good start to get people interested. Seeing the progress I made will help the community visualize the full potential of the garden. /*happy*/



~ resultDone = "true"

->END

==MEILIN_APP2_RESULT_B==

~ currentKnot = "MEILIN_APP2_RESULT_B"
~ currentEmotion = "neutral"
~ nextKnot = "MEILIN_APP3"
~ appDone = "true"

After taking your potion, I sat down to think of a solution to the weak reception to my community garden. /*neutral*/

I started offering a discount for my shop for anyone who turned up at the garden’s opening day to assist in planting.
~ currentEmotion = "happy"
This turned out to be a great motivator. Many came to help, and we were able to plant the whole garden! /*happy*/



I’m so glad to see people getting excited about our community again. 

~ resultDone = "true"

->END


==MEILIN_APP2_RESULT_C==

~ currentKnot = "MEILIN_APP2_RESULT_C"
~ currentEmotion = "neutral"
~ nextKnot = "MEILIN_APP3"
~ appDone = "true"

~ currentEmotion = "nervous"
Reception hasn’t improved for the community garden since I last visited. I haven’t even made much progress on the garden myself. /*nervous*/

~ currentEmotion = "mad"
Since I took your potion, I’ve felt a lack of ambition. I’m shocked, it’s out of character for me. 

 ~ currentEmotion = "neutral"
 It’s been a slow start for the garden, but I won’t give up on it. I have confidence it will become successful and be a great service for the community. /*neutral*/


~ resultDone = "true"

->END

==MEILIN_APP2_RESULT_D==

~ currentKnot = "MEILIN_APP2_RESULT_D"
~ currentEmotion = "neutral"
~ nextKnot = "MEILIN_APP3"
~ appDone = "true"

~ currentEmotion = "happy"
When I left after my last visit, I set out on a social campaign for the community garden, I went door to door to recruit participants for the opening day.

I was slow, but I wasn’t going to let that stop me.

You definitely helped people listen to an old lady ramble on. It turned out to be a success; everyone I talked to showed up for the festivities.

It was a great way to start building community connections! I’m glad you’ve joined us here. 



~ resultDone = "true"

->END






==MEILIN_APP3A==

~ currentKnot = "MEILIN_APP3A"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "sad"


I hope you’re doing well. I haven’t been able to rest easy since war was declared, though my wife will probably say I’ve never rested easy. 

~ currentEmotion = "mad"

We still have to be cautious. The fighting is miles away from the city, but we have to be prepared for anything. We can get through this tumultuous time if we stick together. 

~ currentEmotion = "neutral"

That’s why I’ve decided to take my garden initiative to the next step and create an organization to aid the community during wartime. 

~ currentEmotion = "happy"

I’m calling it our  Homefront Action Movement! Our goal is to make sure everyone is taken care of.  We want them to have food, shelter, and we will do anything that can provide comfort. 

The response from the community has been encouraging! So many people are eager to participate. We’ve started meeting weekly at the garden. 

~ currentEmotion = "nervous"

I’m happy to know so many people care to be involved, but the movement has grown so fast it’s overwhelming. There are so many issues to address, I can’t keep track of them. 

~ currentEmotion = "neutral"

This is far beyond the scale of anything I’ve ever managed before, even my shop. I have to spend all day and night meeting with people to ensure everything gets done. 

Can you give me anything to help manage the workload?


~ nextKnot = "APP3A_Con"

->APP3A_Con

==APP3A_Con==

~ currentKnot = "APP3A_Con"
~ canChoiceBeMade = "yes"

*   This body potion should ease the bones...

~ currentEmotion = "happy"

    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_A"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   I guess I do need to think about things clearly!

~ currentEmotion = "happy"

    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_B"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Maybe Tranquility is what I need!

~ currentEmotion = "happy"

    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_C"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A boost in charisma this time then?

~ currentEmotion = "happy"

    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_D"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[This body potion should ease the bones...: Appointment 3A Result A]
[I guess I do need to think about things clearly!: Appointment 3A Result B]
[Maybe Tranquility is what I need!: Appointment 3A Result C]
[A boost in charisma this time then?: Appointment 3A Result D]
*/


==MEILIN_APP3A_RESULT_A==

~ currentKnot = "MEILIN_APP3A_RESULT_A"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m still up all night, but now I have a boost of endurance to sustain myself and continue working through the day and the nights beyond.

~ currentEmotion = "happy"

There’s so much to be done. My team has made great progress, and I won’t let a single call for aid go unanswered!



->END

==MEILIN_APP3A_RESULT_B==

~ currentKnot = "MEILIN_APP3A_RESULT_B"
~ currentEmotion = "neutral"
~ appDone = "true"

I’ve taken a step back from having a hand in everything myself. Instead, now the Homefront Action Movement has a system of delegation. 

We now have designated team leaders to handle each of our significant tasks. There are teams for farming, food distribution, and childcare. 

~ currentEmotion = "happy"

Now everything can get done and we’ll be able to create more teams for any other needs that should arise. 


->END


==MEILIN_APP3A_RESULT_C==

~ currentKnot = "MEILIN_APP3A_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m no longer overwhelmed. My mind is focused, and I can tackle issues one at a time without being distracted by other pressing matters. 

~ currentEmotion = "happy"

We’ve made a lot of progress, and I have you to thank. 



->END

==MEILIN_APP3A_RESULT_D==

~ currentKnot = "MEILIN_APP3A_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

Things are less chaotic. Everyone is working cooperatively and I am managing the teams without issue. 

~ currentEmotion = "happy"

It’s great to feel the comradery of the community again! We’ve got so many people helping all over the city, I still can’t keep track of it all. 



->END






==MEILIN_APP3B==

~ currentKnot = "MEILIN_APP3B"
~ appDone = "false"
~ currentEmotion = "neutral"

I hope you’re doing well. I haven’t been able to rest easy since war was declared, though my wife will probably say I’ve never rested easy. 

~ currentEmotion = "mad"

We still have to be cautious. The fighting is miles away from the city, but we have to be prepared for anything. We can get through this tumultuous time if we stick together. 

~ currentEmotion = "neutral"

That’s why I’ve decided to take my garden initiative to the next step and create an organization to aid the community during wartime. 

~ currentEmotion = "happy"

I’m calling it our  Homefront Action Movement! Our goal is to make sure everyone is taken care of.  We want them to have food, shelter, and we will do anything that can provide comfort. 

We meet every week in the garden. Recruitment has been slow. Those who are involved are enthusiastic, but there still aren’t enough members to tend to everyone’s needs. 

~ currentEmotion = "neutral"

This is far beyond the scale of anything I’ve ever managed before, even my shop. I have to spend all day and night meeting with people to ensure everything gets done. 

Can you give me anything to help manage the workload?



~ nextKnot = "APP3B_Con"

->APP3B_Con

==APP3B_Con==

~ currentKnot = "APP3B_Con"
~ canChoiceBeMade = "yes"

*   This body potion should ease the bones...

~ currentEmotion = "happy"

    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_A"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   I guess I do need to think about things clearly!

~ currentEmotion = "happy"

    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_B"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Maybe Tranquility is what I need!

~ currentEmotion = "happy"

    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_C"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A boost in charisma this time then?

~ currentEmotion = "happy"

    Thank You! Don’t hesitate to let me know if there is anything the Home Action Movement can do for you! /*happy*/
    ~ nextKnot = "MEILIN_APP3A_RESULT_D"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[This body potion should ease the bones...: Appointment 3B Result A]
[I guess I do need to think about things clearly!: Appointment 3B Result B]
[Maybe Tranquility is what I need!: Appointment 3B Result C]
[A boost in charisma this time then?: Appointment 3B Result D]
*/


==MEILIN_APP3B_RESULT_A==

~ currentKnot = "MEILIN_APP3B_RESULT_A"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m still up all night, but now I have a boost of endurance to sustain myself and continue working through the day and the nights beyond. 

~ currentEmotion = "nervous"

There’s so much to be done. I’m constantly running errands, even in the dead of night. 

~ currentEmotion = "neutral"

We must continue to manage until the Homefront Action Movement gains more recruits. I won’t let a single call for aid go unanswered!




->END

==MEILIN_APP3B_RESULT_B==

~ currentKnot = "MEILIN_APP3B_RESULT_B"
~ currentEmotion = "neutral"
~ appDone = "true"

I’ve taken a step back from having a hand in everything myself. Instead, now the Homefront Action Movement has embraced a system of delegation.

I want there to be leaders for each of our committees, but unfortunately, we still don’t have enough recruits to fill these roles. 

We know where and how many people need help but we still can’t meet all of the community’s needs. 


->END


==MEILIN_APP3B_RESULT_C==

~ currentKnot = "MEILIN_APP3B_RESULT_C"
~ currentEmotion = "happy"
~ appDone = "true"

I’m no longer overwhelmed. My mind is focused, and I can tackle issues one at a time without being distracted by other pressing matters. 

There’s still so much, but I’m making progress! 



->END

==MEILIN_APP3B_RESULT_D==

~ currentKnot = "MEILIN_APP3B_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

I spent some time socializing at the marketplace this week, and I was able to recruit more for the Homefront Action Movement. 

~ currentEmotion = "happy"

Now I have enough members to take over some responsibilities.



->END

/*
Appointment 4 as multiple people
*/


==MEILIN_APP5A==

~ currentKnot = "MEILIN_APP5A"
~ appDone = "false"
~ currentEmotion = "happy"

I could barely make it down the street. It was crowded with people celebrating! We’ve gained land and pushed enemy troops away from the city! 

Morale is as strong as ever in the Homefront Action Movement!

~ currentEmotion = "neutral"

The war isn’t over yet though. The celebration has been a relief, but we still have a long road ahead of us.  We can’t lose focus now.  /*neutral*/

~ currentEmotion = "mad"

We must never forget this victory was made possible through sacrifice. Many people lost loved ones.  /*mad*/

~ currentEmotion = "sad"

I’m on my way to visit a family who lost their father in the battle. I hope there is something I can do to bring some comfort.  /*sad*/



~ nextKnot = "APP5A_Con"

->APP5A_Con

==APP5A_Con==

~ currentKnot = "APP5A_Con"
~ canChoiceBeMade = "yes"

*   This body potion should ease the bones...
    ~ currentEmotion = "neutral"

    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5A_RESULT_A"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   I guess I do need to think about things clearly!
    ~ currentEmotion = "neutral"

    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5A_RESULT_B"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Maybe Tranquility is what I need!
    ~ currentEmotion = "neutral"

    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5A_RESULT_C"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A boost in charisma this time then?
    ~ currentEmotion = "neutral"
    
    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5A_RESULT_D"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[This body potion should ease the bones...: Appointment 5A Result A]
[I guess I do need to think about things clearly!: Appointment 5A Result B]
[Maybe Tranquility is what I need!: Appointment 5A Result C]
[A boost in charisma this time then?: Appointment 5A Result D]
*/


==MEILIN_APP5A_RESULT_A==

~ currentKnot = "MEILIN_APP5A_RESULT_A"
~ currentEmotion = "sad"
~ appDone = "true"

I was able to help our neighbors by completing housework. It wasn’t much, but it provided relief.  You helped me feel recharged after a long day of work.  /*sad*/


->END

==MEILIN_APP5A_RESULT_B==

~ currentKnot = "MEILIN_APP5A_RESULT_B"
~ currentEmotion = "sad"
~ appDone = "true"

It’s impossible to completely escape grief, no matter how hard any of us tries. It must run its course. 

As hard as it is to accept, all I can do is be a comforting presence for our neighbors.



->END


==MEILIN_APP5A_RESULT_C==

~ currentKnot = "MEILIN_APP5A_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"

I spent all night long talking with our neighbors. Thanks to you, my mind was clear of other worries, allowing me to be a comforting presence by listening.


->END

==MEILIN_APP5A_RESULT_D==

~ currentKnot = "MEILIN_APP5A_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

I was able to assist our neighbors by keeping the children entertained by telling stories while the mom was able to spend time with her sister.


->END


==MEILIN_APP5B==

~ currentKnot = "MEILIN_APP5B"
~ appDone = "false"
~ currentEmotion = "happy"

I could barely make it down the street. It was crowded with people celebrating! We’ve gained land and pushed enemy troops away from the city! /*happy*/

~ currentEmotion = "neutral"

The war isn’t over yet though. The celebration has been a relief, but we still have a long road ahead of us.  We can’t lose focus now. 

~ currentEmotion = "nervous"

My volunteers are distracted and have relaxed on their duties. I hope it won’t continue. We are needed now more than ever.  

~ currentEmotion = "sad"

We must never forget this victory was made possible through sacrifice. Many people lost loved ones. We need to be there for our community.

I’m on my way to visit a family who lost their father in the battle. I hope there is something I can do to bring some comfort. 



~ nextKnot = "APP5B_Con"

->APP5B_Con

==APP5B_Con==

~ currentKnot = "APP5B_Con"
~ canChoiceBeMade = "yes"

*   This body potion should ease the bones...
    ~ currentEmotion = "neutral"

    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5B_RESULT_A"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   I guess I do need to think about things clearly!
    ~ currentEmotion = "neutral"

    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5B_RESULT_B"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Maybe Tranquility is what I need!
    ~ currentEmotion = "neutral"

    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5B_RESULT_C"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A boost in charisma this time then?
    ~ currentEmotion = "neutral"

    Thank you! Stay safe. /*neutral*/
    ~ nextKnot = "MEILIN_APP5B_RESULT_D"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[This body potion should ease the bones...: Appointment 5B Result A]
[I guess I do need to think about things clearly!: Appointment 5B Result B]
[Maybe Tranquility is what I need!: Appointment 5B Result C]
[A boost in charisma this time then?: Appointment 5B Result D]
*/


==MEILIN_APP5B_RESULT_A==

~ currentKnot = "MEILIN_APP5B_RESULT_A"
~ currentEmotion = "neutral"
~ appDone = "true"

I was able to help our neighbors by completing housework. It wasn’t much, but it provided relief.  You helped me feel recharged after a long day of work. 


->END

==MEILIN_APP5B_RESULT_B==

~ currentKnot = "MEILIN_APP5B_RESULT_B"
~ currentEmotion = "sad"
~ appDone = "true"

It’s impossible to completely escape grief, no matter how hard any of us tries. It must run its course. 
As hard as it is to accept, all I can do is be a comforting presence for our neighbors. 




->END


==MEILIN_APP5B_RESULT_C==

~ currentKnot = "MEILIN_APP5B_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"

It’s understandable my volunteers were excited by the good news. You helped me have patience.  

They took time to celebrate, then joined me to comfort our neighbors.

We were able to complete household chores and cook dinner for the family.

 


->END

==MEILIN_APP5B_RESULT_D==

~ currentKnot = "MEILIN_APP5B_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

I was able to assist our neighbors by keeping the children entertained by telling stories while the mom was able to spend time with her sister.


->END

==MEILIN_APP7A==

~ currentKnot = "MEILIN_APP7A"
~ appDone = "false"
~ currentEmotion ="nervous"

Have you heard the news yet? Attacks are getting closer, whole villages outside city limits are gone.  

We’ve started a new division of the Homefront Action Movement to mobilize in case enemy troops should enter the city. 

We’ve been gathering weapons and devising strategies to defend ourselves. We are planning escape routes out of the city, but I won’t give up on my city without a fight. 

Now with the ration on the city’s water it looks like we could lose our gardens. They’ve become the primary food source for so many people. 

~ currentEmotion = "sad"

We are prioritizing our water for our people, but things are changing so quickly. We can’t handle losing access to food too. 

We need help to find reliable information and plan our next move. How do we manage our priorities?




~ nextKnot = "APP7A_Con"

->APP7A_Con

==APP7A_Con==

~ currentKnot = "APP7A_Con"
~ canChoiceBeMade = "yes"

*   This body potion should ease the bones...
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7A_RESULT_A"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   I guess I do need to think about things clearly!
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7A_RESULT_B"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Maybe Tranquility is what I need!
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7A_RESULT_C"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A boost in charisma this time then?
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7A_RESULT_D"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[This body potion should ease the bones...: Appointment 7A Result A]
[I guess I do need to think about things clearly!: Appointment 7A Result B]
[Maybe Tranquility is what I need!: Appointment 7A Result C]
[A boost in charisma this time then?: Appointment 7A Result D]
*/


==MEILIN_APP7A_RESULT_A==

~ currentKnot = "MEILIN_APP7A_RESULT_A"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m glad to see you’re safe. 

I’ve been having difficulties getting enough water. We have to determine which crops to let go and which to continue to maintain. At least we won't lose everything. 



->END

==MEILIN_APP7A_RESULT_B==

~ currentKnot = "MEILIN_APP7A_RESULT_B"
~ currentEmotion = "sad"
~ appDone = "true"

I’m glad to see you’re safe.

I’ve been able to sleep easier now that we figured out a way to maintain our gardens. Thanks to the potion, I devised a new irrigation system that allows us to use water more efficiently. 

Now we are able to water the crops and still have water for our families.  



->END


==MEILIN_APP7A_RESULT_C==

~ currentKnot = "MEILIN_APP7A_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m glad to see you’re safe. The community has come together to volunteer any water they can spare. I’m so proud of them. 

We aren’t able to maintain our gardens at the same capacity as before, but at least they won’t be gone completely 


->END

==MEILIN_APP7A_RESULT_D==

~ currentKnot = "MEILIN_APP7A_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m glad to see you’re safe.

Thankfully I’ve built a lot of connections through my business over the years. I’ve worked with many assistants in the Senate when they order flowers for the offices. 

I was able to bribe an assistant for access to additional water. It's just a stop gap for now, but at least we have plenty for the garden. 


->END

==MEILIN_APP7B==

~ currentKnot = "MEILIN_APP7B"
~ appDone = "false"
~ currentEmotion = "nervous"

Have you heard the news yet? Attacks are getting closer, whole villages outside city limits are gone. 

We’ve started a new division of the Home Action Movement to mobilize in case enemy troops should enter the city. 

We’ve been gathering weapons and devising strategies to defend ourselves. We are planning escape routes out of the city, but I don’t plan on giving up without a fight. 

~ currentEmotion = "mad"

All was going well, but last night our storehouse was robbed. Weeks worth of food and medicine were taken. It’s been a heavy blow to morale. 

Now with the implementation of water rationing that will affect our gardens, I don’t know how we’ll build our stores back up. We have so many families depending on us. 

~ currentEmotion = "sad"

Without the storehouse, we might not survive occupation. 

How can I lift morale and replenish supplies? 



~ nextKnot = "APP7B_Con"

->APP7B_Con

==APP7B_Con==

~ currentKnot = "APP7B_Con"
~ canChoiceBeMade = "yes"

*   This body potion should ease the bones...
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7B_RESULT_A"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   I guess I do need to think about things clearly!
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7B_RESULT_B"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Maybe Tranquility is what I need!
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7B_RESULT_C"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A boost in charisma this time then?
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7B_RESULT_D"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[This body potion should ease the bones...: Appointment 7B Result A]
[I guess I do need to think about things clearly!: Appointment 7B Result B]
[Maybe Tranquility is what I need!: Appointment 7B Result C]
[A boost in charisma this time then?: Appointment 7B Result D]
*/


==MEILIN_APP7B_RESULT_A==

~ currentKnot = "MEILIN_APP7B_RESULT_A"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m glad to see you’re safe.

I’ve been having difficulties getting enough water. We have to determine which crops to let go and which to continue to maintain. At least we won't lose everything. 


->END

==MEILIN_APP7B_RESULT_B==

~ currentKnot = "MEILIN_APP7B_RESULT_B"
~ currentEmotion = "sad"
~ appDone = "true"

I’m glad to see you’re safe.

I’ve been able to sleep easier now that we figured out a way to maintain our gardens. Thanks to the potion, I devised a new irrigation system that allows us to use water more efficiently. 

Now we are able to water the crops and still have water for our families.  



->END


==MEILIN_APP7B_RESULT_C==

~ currentKnot = "MEILIN_APP7B_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m glad to see you’re safe. The community has come together to volunteer any water they can spare. I’m so proud of them. 

We aren’t able to maintain our gardens at the same capacity as before, but at least they won’t be gone completely. 



->END

==MEILIN_APP7B_RESULT_D==

~ currentKnot = "MEILIN_APP7B_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m glad to see you’re safe.

Thankfully I’ve built a lot of connections through my business over the years. I’ve worked with many assistants in the Senate when they order flowers for the offices. 

I was able to bribe an assistant for access to additional water. It's just a stop gap for now, but at least we have plenty for the garden. 



->END

==MEILIN_APP7C==

~ currentKnot = "MEILIN_APP7C"
~ appDone = "false"
~ currentEmotion = "nervous"

Have you heard the news yet? Attacks are getting closer, whole villages outside city limits are gone.  

We’ve started a new division of the Home Action Movement to mobilize in case enemy troops should enter the city. 

We’ve been gathering weapons and devising strategies to defend ourselves. We are planning escape routes out of the city, but I don’t plan on giving up on the city without a fight. 

Unfortunately everything is coming to a halt. Our supplies are being depleted as people in the community are getting sick. There aren’t enough healthy people to maintain schedules.

~ currentEmotion = "sad"

Now with the implementation of water rationing. I’m worried we’ll be left vulnerable and unable to recover. 

How can we stretch what little resources we have? 


~ nextKnot = "APP7C_Con"

->APP7C_Con

==APP7C_Con==

~ currentKnot = "APP7C_Con"
~ canChoiceBeMade = "yes"

*   This body potion should ease the bones...
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7C_RESULT_A"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   I guess I do need to think about things clearly!
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7C_RESULT_B"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Maybe Tranquility is what I need!
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7C_RESULT_C"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A boost in charisma this time then?
    ~ currentEmotion = "neutral"

    Thank you. The Homefront Action Movement is always here if you need anything. 
    ~ nextKnot = "MEILIN_APP7C_RESULT_D"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[This body potion should ease the bones...: Appointment 7C Result A]
[I guess I do need to think about things clearly!: Appointment 7C Result B]
[Maybe Tranquility is what I need!: Appointment 7C Result C]
[A boost in charisma this time then?: Appointment 7C Result D]
*/


==MEILIN_APP7C_RESULT_A==

~ currentKnot = "MEILIN_APP7C_RESULT_A"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m glad to see you’re safe.

I’ve been having difficulties getting enough water. We have to determine which crops to let go and which to continue to maintain. At least we won't lose everything. 


->END

==MEILIN_APP7C_RESULT_B==

~ currentKnot = "MEILIN_APP7C_RESULT_B"
~ currentEmotion = "sad"
~ appDone = "true"

I’m glad to see you’re safe.

I’ve been able to sleep easier now that we figured out a way to maintain our gardens. Thanks to the potion, I devised a new irrigation system that allows us to use water more efficiently. 

Now we are able to water the crops and still have water for our families.  



->END


==MEILIN_APP7C_RESULT_C==

~ currentKnot = "MEILIN_APP7C_RESULT_C"
~ currentEmotion = "neutral"
~ appDone = "true"


I’m glad to see you’re safe. The community has come together to volunteer any water they can spare. I’m so proud of them. 

We aren’t able to maintain our gardens at the same capacity as before, but at least they won’t be gone completely 



->END

==MEILIN_APP7C_RESULT_D==

~ currentKnot = "MEILIN_APP7C_RESULT_D"
~ currentEmotion = "neutral"
~ appDone = "true"

I’m glad to see you’re safe.

Thankfully I’ve built a lot of connections through my business over the years. I’ve worked with many assistants in the Senate when they order flowers for the offices. 

I was able to bribe an assistant for access to additional water. It's just a stop gap for now, but at least we have plenty for the garden. 


->END