->INDRA_APP1_INTRO

VAR currentKnot = "none"
VAR nextKnot = "none"
VAR currentEmotion = "none"
VAR appDone = "false"
VAR resultDone = "false"
VAR canChoiceBeMade = "no"
VAR weekDone = "false"

==INDRA_APP1_INTRO==

~ currentKnot = "INDRA_APP1_INTRO"
~ appDone = "false"
~ resultDone = "false"

~ currentEmotion = "angry"
Do you know who that was?! 

That man calls himself a ‘seer’, yet he’s nothing but a fraud, yelling ‘visions of the future’ from street corners for anyone senseless enough to listen. /*angry*/

I’d be careful, you don’t want people to associate him with your business.
~ currentEmotion = "neutral"

I should introduce myself. I’m Indra So'ikeer Khenaj ran Alamet-Mateirci, but you can just call me Indra. /*neutral*/
~ currentEmotion = "boastful"

You’re new to town, but I see you’ve already made an impression on the community. A friend of mine told me that you are THE authority on all things potions./*boastful*/

~ currentEmotion = "neutral"
I’d like to enlist your services in a trial run. I’m desperate for something to soothe my nerves./*neutral*/
My husband is an important diplomat, he can’t help but bring his work home with him, but lately it’s become increasingly taxing. Nothing to worry about I assure you. 

I do my best to make a comfortable home for him. I take care of the staff, plan our meals, I do everything I can to ensure he can come home and relax, to recharge for the next day.
I try to keep my home an oasis amongst the chaos.
~ currentEmotion = "sad"

I hate to say it, but I feel like I am failing. I find Zaremir up at all hours of the night, neither of us can sleep. /*sad*/
~ currentEmotion = "neutral"

He barely eats, and the staff is so tense nothing gets done without something getting broken. Is there anything you can give me that help me command my home?

We could all use some peace of mind. /*neutral*/

~ nextKnot = "APP1_Con"
~ currentEmotion = "neutral"

->APP1_Con

==APP1_Con==

~ currentKnot = "APP1_Con"
~ canChoiceBeMade = "yes"

*   A Body potion?
    ~ currentEmotion = "boastful"
    Excellent! Thank you very much! This will be a great help. /*boastful*/
    ~ nextKnot = "INDRA_APP1_RESULT_A"
    

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect is the key this time then...
    ~ currentEmotion = "boastful"
    Excellent! Thank you very much! This will be a great help. /*boastful*/
    ~ nextKnot = "INDRA_APP1_RESULT_B"
    

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility? I need to be calm for this?
    ~ currentEmotion = "boastful"
    Excellent! Thank you very much! This will be a great help. /*boastful*/
    ~ nextKnot = "INDRA_APP1_RESULT_C"
    

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   So the key is to be Charismatic?
    ~ currentEmotion = "boastful"
    Excellent! Thank you very much! This will be a great help. /*boastful*/
    ~ nextKnot = "INDRA_APP1_RESULT_D"
    

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body potion?: Appointment 1 Result A]
[Intellect is the key this time then...: Appointment 1 Result B]
[Tranquility? I need to be calm for this?: Appointment 1 Result C]
[So the key is to be Charismatic?: Appointment 1 Result D]
*/


==INDRA_APP1_RESULT_A==

~ currentKnot = "INDRA_APP1_RESULT_A"
~ currentEmotion = "content"
~ appDone = "true"

I’ve had more energy, I’ve been able to take on more tasks at home that have been going unattended, and I’ve been able to assist in Zaremir’s task. /*content*/
He’s been working late into the night, but with my help he is able to set some time aside to relax.

~ resultDone = "true"
~ nextKnot = "INDRA_APP2"

->END

==INDRA_APP1_RESULT_B==

~ currentKnot = "INDRA_APP1_RESULT_B"
~ currentEmotion = "content"
~ appDone = "true"

Your potion was a success!

Whatever you gave me provided clarity, I was finally able to calm my mind down and focus. I sat down with Zaremir, he told me he was overwhelmed with the weight of his duties. 

I helped him organize his thoughts, we formed a logical plan to accomplish all his tasks and how I could assist with the house. Everything is now in perfect shape! 

~ resultDone = "true"
~ nextKnot = "INDRA_APP2"

->END


==INDRA_APP1_RESULT_C==

~ currentKnot = "INDRA_APP1_RESULT_C"
~ currentEmotion = "content"
~ appDone = "true"

Whatever you gave me filled me with confidence and patience. I was able to reestablish myself as the emotional cornerstone of the house. /*content*/
I guided my staff, patiently reviewing forgotten protocols, assuring everyone their jobs were safe in light of recent mishaps, easing the tension.
I stayed up talking all night with Zaremir, working through his fears. He is doing much better.
I appreciate your help with this. 

~ resultDone = "true"
~ nextKnot = "INDRA_APP2"

->END

==INDRA_APP1_RESULT_D==

~ currentKnot = "INDRA_APP1_RESULT_D"
~ currentEmotion = "content"
~ appDone = "true"

With your help, I’ve been able to get through to Zaremir. We spent all night talking. /*content*/
He’s been overwhelmed with the weight of his duties, and refuses to take a break.
Thankfully I was able to persuade him to reach out to other Senators he trust to lighten his work load. 
I appreciate your help with this. 

~ resultDone = "true"
~ nextKnot = "INDRA_APP2"

->END


==INDRA_APP2==

~ currentKnot = "INDRA_APPOINTMENT_2"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "neutral"

You've really helped me and husband out and I'm hoping you can do so again. 
Zaremir and I are hosting an extravagant dinner party in a few nights. I’m in the middle of preparations now. 

We’re sparing no expense, the meal, the décor – I get the most beautiful floral arrangements from a quaint little shop just around the corner.
~ currentEmotion = "smug"
Everything will be of the most preeminent quality. Especially the guest list. All of high society is expected to attend, including influential members of the senate. 
~ currentEmotion = "neutral"

I must confess the thought of entertaining such significant figures makes me nervous, I’d hate to make a bad impression for Zaremir.

Is there anything you can give me to ensure I’m the most attentive host as possible? /*neutral*/

~ nextKnot = "APP2_Con"

->APP2_Con

==APP2_Con==

~ currentKnot = "APP2_Con"
~ canChoiceBeMade = "yes"

*   A Body potion?
    ~ currentEmotion = "content"
    Ah, you're a wonder. Thank you kindly for your assistance.
    I'll be back with the news of the event. I know you'll want to hear it! /*content*/
    ~ nextKnot = "INDRA_APP2_RESULT_A"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect is the key this time then...
    ~ currentEmotion = "content"
    Ah, you're a wonder. Thank you kindly for your assistance. /*contnent*/
    I'll be back with the news of the event. I know you'll want to hear it!  
    ~ nextKnot = "INDRA_APP2_RESULT_B"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility? I need to be calm for this?
    ~ currentEmotion = "content"
    Ah, you're a wonder. Thank you kindly for your assistance. /*content*/
    I'll be back with the news of the event. I know you'll want to hear it!  
    ~ nextKnot = "INDRA_APP2_RESULT_C"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   So the key is to be Charismatic?
    ~ currentEmotion = "content"
    Ah, you're a wonder. Thank you kindly for your assistance. /*content*/
    I'll be back with the news of the event. I know you'll want to hear it!  
    ~ nextKnot = "INDRA_APP2_RESULT_D"
    
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END


/*
[A Body potion?: Appointment 2 Result A]
[Intellect is the key this time then...: Appointment 2 Result B]
[Tranquility? I need to be calm for this?: Appointment 2 Result C]
[So the key is to be Charismatic?: Appointment 2 Result D]
*/

==INDRA_APP2_RESULT_A==

~ currentKnot = "INDRA_APP2_RESULT_A"
~ currentEmotion = "angry"
~ appDone = "true"

The potion had no discernible affect this time. I did my best to tend to our guest, but the night wasn’t without a few mishaps. I mixed up names, I spilled drinks. /*angry*/
~ currentEmotion = "neutral"
I was overwhelmed with stress, but I managed to keep my head above water. /*neutral*/
~ currentEmotion = "sad"
I just hope that my mistakes come across as endearing,  and didn’t negatively impact Zaremir’s reputation. /*sad*/
~ currentEmotion = "neutral"
Well, it’s no matter. That part of the evening was soon forgotten. /*neutral*/
~ currentEmotion = "surprised"
It was confirmed last night, war will be declared with one of our greatest trade partners

Zaremir has been in the minority against the war the whole time. He’s made every effort to sway the senate to his side, but it’s been no use.
~ currentEmotion = "frightened"
I fear there is nothing we can do about it now. Our guest met the news with great enthusiasm, eagerly discussing strategy the rest of the night. /*frightened*/
~ currentEmotion = "sad"
I suppose we'll just have to keep our heads about us. Hopefully such a war will pass quickly./*sad*/
~ currentEmotion = "neutral"
Ah, but look at me yammering about. I must be off. I'll be back soon, I'm sure. /*neutral*/
I hope you'll be able to support me through this. I will appreciate all the help you can give me.
Goodbye until next time.

~ weekDone = "true"
~ nextKnot = "INDRA_APP3A"
~ resultDone = "true"

->END

==INDRA_APP2_RESULT_B==

~ currentKnot = "INDRA_APP2_RESULT_B"
~ currentEmotion = "boastful"
~ appDone = "true"

The dinner was a great success. I impressed all of our guest! /*boastful*/
Especially those who tend to look down on the spouses who are adept at running a house instead of choosing to find an occupation elsewhere. 
I kept up with all of them, no matter how fast they shifted between topics. Art, geography, politics. I was proficient in every subject.
~ currentEmotion = "neutral"
Well, it’s no matter. That part of the evening was soon forgotten. /*neutral*/
~ currentEmotion = "surprised"
It was confirmed last night, war will be declared with one of our greatest trade partners. /*surprised*/
Zaremir has been in the minority against the war the whole time. He’s made every effort to sway the senate to his side, but it’s been no use.
~ currentEmotion = "frightened"
I fear there is nothing we can do about it now. Our guest met the news with great enthusiasm, eagerly discussing strategy the rest of the night. /*frightened*/
"I suppose we'll just have to keep our heads about us. Hopefully such a war will pass quickly."
~ currentEmotion = "neutral"
Ah, but look at me yammering about. I must be off. I'll be back soon, I'm sure. You've been invaluable help. /*neutral*/

I hope you'll be able to work your wizardry in conflict as well as you have in peacetime! Farewell until next time!

~ weekDone = "true"
~ nextKnot = "INDRA_APP3A"
~ resultDone = "true"

->END


==INDRA_APP2_RESULT_C==

~ currentKnot = "INDRA_APP2_RESULT_C"
~ currentEmotion = "content"
~ appDone = "true"

The dinner was a success. I’m not sure if I made a strong impression, but I was able to be patient and relax, I didn’t feel the need to control everything. /*content*/
This poise helped set the tone for my staff, and enabled them to do what they do best. 
~ currentEmotion = "neutral"
Well, it’s no matter. That part of the evening was soon forgotten. /*neutral*/
~ currentEmotion = "surprised"
It was confirmed last night, war will be declared with one of our greatest trade partners. /*surprised*/
Zaremir has been in the minority against the war the whole time. He’s made every effort to sway the senate to his side, but its been no use. 
~ currentEmotion = "frightened"
I fear there is nothing we can do about it now. Our guest met the news with great enthusiasm, eagerly discussing strategy the rest of the night. /*frightened*/
I suppose we'll just have to keep our heads about us. Hopefully such a war will pass quickly.
~ currentEmotion = "neutral"
Ah, but look at me yammering about. I must be off. I'll be back soon, I'm sure. You've been invaluable help. /*neutral*/

I hope you'll be able to work your wizardry in conflict as well as you have in peacetime! Farewell until next time!

~ weekDone = "true"
~ nextKnot = "INDRA_APP3A"
~ resultDone = "true"

->END


==INDRA_APP2_RESULT_D==

~ currentKnot = "INDRA_APP2_RESULT_D"
~ currentEmotion = "boastful"
~ appDone = "true"

The dinner was a great success. I made an outlasting impression with my presence. Not once did I waiver under the pressure of everyone’s attention. /*boastful*/
Instead, I always knew exactly what to say, and I was able to facilitate connections between my guest.
~ currentEmotion = "neutral"
Well, it’s no matter. That part of the evening was soon forgotten. /*neutral*/
~ currentEmotion = "surprise"
It was confirmed last night, war will be declared with one of our greatest trade partners. /*surprised*/
Zaremir has been in the minority against the war the whole time. He’s made every effort to sway the senate to his side, but its been no use.
~ currentEmotion = "frightened"
I fear there is nothing we can do about it now. Our guest met the news with great enthusiasm, eagerly discussing strategy the rest of the night. /*frightened*/
I suppose we'll just have to keep our heads about us. Hopefully such a war will pass quickly.
~ currentEmotion = "neutral"
Ah, but look at me yammering about. I must be off. I'll be back soon, I'm sure. You've been invaluable help. /*neutral*/

I hope you'll be able to work your wizardry in conflict as well as you have in peacetime! Farewell until next time!

~ weekDone = "true"
~ nextKnot = "INDRA_APP3A"
~ resultDone = "true"

->END

==INDRA_APP3A==

~ currentKnot = "INDRA_APPOINTMENT_3A"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "neutral"
~ weekDone = "false"

Greetings, potion-seller. How do you fare in this conflict?

Us? We get along better than most, as you might imagine. The capital's far away from the fighting, after all.

I’m on my way home from the Senate. I met with a Senator and he invited me for a tour after attending our dinner party. No doubt to boast his knowledge of the architectural marvel.

I made sure to give a boost to his ego, ‘Oohing’ and ‘Aahing’ at the appropriate intervals. Still, I managed to earn his respect with architectural proficiency of my own.  

~ currentEmotion = "angry"

It was enough for him to feel comfortable to confide in me. And you won’t believe it. The generals have enlisted Dante to provide guidance in war matters! 
 /*angry*/

~ currentEmotion = "annoyed"

He lunches with them daily! They think he’ll direct them to victory! /*annoyed*/

~ currentEmotion = "neutral"

Now, Zaremir and I are gaining momentum in solidifying goodwill with other parliament members. So, I’m sure it will be nothing to worry about soon. /*neutral*/

~ currentEmotion = "sad"

But, Zaremir continues to work himself tirelessly. I appreciate his dedication, but no other member is anywhere near as concerned of our situation as he is. /*sad*/

I’m worried about his health. Do you have anything that can help him?


~ nextKnot = "APP3A_Con"

->APP3A_Con

==APP3A_Con==

~ currentKnot = "APP3A_Con"
~ canChoiceBeMade = "yes"

*   A Body potion?

    ~ currentEmotion = "content"

    I appreciate this greatly. You’ve yet to let me down. 
    ~ nextKnot = "INDRA_APP3A_RESULT_A" 

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect is the key this time then...

    ~ currentEmotion = "content"

    I appreciate this greatly. You’ve yet to let me down. 
    ~ nextKnot = "INDRA_APP3A_RESULT_B"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility? I need to be calm for this?

    ~ currentEmotion = "content"

    I appreciate this greatly. You’ve yet to let me down.
    ~ nextKnot = "INDRA_APP3A_RESULT_C" 

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   So the key is to be Charismatic?

    ~ currentEmotion = "content"

    I appreciate this greatly. You’ve yet to let me down. 
    ~ nextKnot = "INDRA_APP3A_RESULT_D" 

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body potion?: Appointment 3A Result A]
[Intellect is the key this time then...: Appointment 3A Result B]
[Tranquility? I need to be calm for this?: Appointment 3A Result C]
[So the key is to be Charismatic?: Appointment 3A Result D]
*/


==INDRA_APP3A_RESULT_A==

~ currentKnot = "INDRA_APP3A_RESULT_A"
~ currentEmotion = "sad"
~ appDone = "true"

The potion revived Zaremir’s body. Unfortunately, this has enabled him to spend more time on his work. He’s more productive than ever. 

I wish he took his resting as seriously as he takes his work. At least his health is no longer deteriorating. 

~ nextKnot = "INDRA_APP4"
~ resultDone = "true"

->END

==INDRA_APP3A_RESULT_B==

~ currentKnot = "INDRA_APP3A_RESULT_B"
~ currentEmotion = "sad"
~ appDone = "true"

It seems even your handiwork cannot thwart Zaremir's mountain of burden.

He stays later at that accursed Senate building now with endless meetings. And he gets more and more haggard with every passing day!


~ nextKnot = "INDRA_APP4"
~ resultDone = "true"

->END


==INDRA_APP3A_RESULT_C==

~ currentKnot = "INDRA_APP3A_RESULT_C"
~ currentEmotion = "content"
~ appDone = "true"

Zaremir fell right asleep once he finally relented to taking the potion. Now he sleeps peacefully through the night… and into most of the morning. 

Zaremir is bothered at the cut to his productivity at work, but I think this will be good for him. Hopefully this will help him find balance. 


~ nextKnot = "INDRA_APP4"
~ resultDone = "true"

->END

==INDRA_APP3A_RESULT_D==

~ currentKnot = "INDRA_APP3A_RESULT_D"
~ currentEmotion = "content"
~ appDone = "true"

Zaremir finally came to his senses and asked a colleague of his in the senate to help take care of matters.  Zaremir is satisfied that work is still being pushed forward.

He's resting back at home now. I’m sure he’ll be appreciative once he returns to work refreshed and productive. 

Hopefully this will help him find more balance. 


~ nextKnot = "INDRA_APP4"
~ resultDone = "true"

->END

==INDRA_APP3B==

~ currentKnot = "INDRA_APPOINTMENT_3B"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "neutral"

Good day to you, potion seller. How are you?

Me? Fine enough-- the war's far in the distance.

I’m on my way home from the Senate. I met with a Senator, he invited me for a tour after attending our dinner party. No doubt to boast his knowledge of the architectural marvel.

I’m trying everything I can to make up for the disastrous impression I made at our dinner party.
I made sure to give a boost to his ego, ‘Oohing’ and ‘Aahing’ at the appropriate intervals.

It seemed to have no effect. He remained distant the entire outing. 

~ currentEmotion = "annoyed"

You won’t believe who I saw at the Senate. Dante! Having lunch with generals! They believe he can provide guidance for the war, and yet they won’t listen to Zaremir! 
 /*annoyed*/

~ currentEmotion = "surprised"

Wait. Did you hear that? /*surprised*/

…


~ currentEmotion = "frightened"

Was there someone in the window? I thought I saw…
This has been happening all week. I’m sure it’s nothing. /*frightened*/

~ currentEmotion = "neutral"

Anyway, Zaremir continues to work tirelessly, and he’s losing confidence he will ever be able to provide some kind of political impact. /*neutral*/

Do you have anything that can help him?


~ nextKnot = "APP3B_Con"

->APP3B_Con

==APP3B_Con==

~ currentKnot = "APP3B_Con"
~ canChoiceBeMade = "yes"

*   A Body potion?

    ~ currentEmotion = "content"

    I appreciate this greatly. You’ve yet to let me down. 
    ~ nextKnot = "INDRA_APP3B_RESULT_A" /*content*/

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect is the key this time then...

    ~ currentEmotion = "content"

    I appreciate this greatly. You’ve yet to let me down. 
    ~ nextKnot = "INDRA_APP3B_RESULT_B" /*content*/

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility? I need to be calm for this?

    ~ currentEmotion = "content"

    I appreciate this greatly. You’ve yet to let me down.
    ~ nextKnot = "INDRA_APP3B_RESULT_C" /*content*/

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   So the key is to be Charismatic?

    ~ currentEmotion = "content"

    I appreciate this greatly. You’ve yet to let me down. 
    ~ nextKnot = "INDRA_APP3B_RESULT_D" /*content*/

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body potion?: Appointment 3B Result A]
[Intellect is the key this time then...: Appointment 3B Result B]
[Tranquility? I need to be calm for this?: Appointment 3B Result C]
[So the key is to be Charismatic?: Appointment 3B Result D]
*/


==INDRA_APP3B_RESULT_A==

~ currentKnot = "INDRA_APP3B_RESULT_A"
~ currentEmotion = "sad"
~ appDone = "true"

The potion revived Zaremir’s body. Unfortunately, this has enabled him to spend time more time on his work.  /*sad*/

He’s more productive than ever, and at least his health is no longer deteriorating. /*neutral*/

    ~ currentEmotion = "frightened"

But, I worry about his safety. I think we’re being followed whenever we leave the house.  /*frightened*/

 I don’t know who to turn to for protection, especially if anyone in the Senate is involved. 

~ nextKnot = "INDRA_APP4"
~ resultDone = "true"

->END

==INDRA_APP3B_RESULT_B==

~ currentKnot = "INDRA_APP3B_RESULT_B"
~ currentEmotion = "sad"
~ appDone = "true"

It seems even your handiwork cannot thwart Zaremir's mountain of burden. /*sad*/

He stays later at that accursed Senate building now with endless meetings. And he gets more and more haggard with every passing day! 

    ~ currentEmotion = "frightened"

Now I can’t help but to worry about his safety too. I think we’re being followed whenever we leave the house. /*frightened*/

I don’t know who to turn to for protection, especially if anyone in the Senate is involved.

~ nextKnot = "INDRA_APP4"
~ resultDone = "true"

->END


==INDRA_APP3B_RESULT_C==

~ currentKnot = "INDRA_APP3B_RESULT_C"
~ currentEmotion = "content"
~ appDone = "true"

Zaremir fell right asleep once he finally relented to taking the potion. Now he sleeps peacefully through the night… and into most of the morning. /*content*/

The rest has been good for him. It’s helped clear his head and reignite the purpose he finds in his work. 

    ~ currentEmotion = "frightened"

But, I worry about his safety. I think we’re being followed whenever we leave the house. /*frightened*/

I don’t know who to turn to for protection, especially if anyone in parliament is involved. 

~ nextKnot = "INDRA_APP4"
~ resultDone = "true"

->END

==INDRA_APP3B_RESULT_D==

~ currentKnot = "INDRA_APP3B_RESULT_D"
~ currentEmotion = "content"
~ appDone = "true"

I was finally able to convince Zaremir to take the day off. He spent the morning resting at home. Now he’s at the marketplace, speaking with the community. /*content*/

It’s helped center him and remind him what his is working for. 

    ~ currentEmotion = "frightened"

But, I worry about his safety. I think we’re being followed whenever we leave the house. /*frightened*/

I don’t know who to turn to for protection, especially if anyone in parliament is involved. 

~ nextKnot = "INDRA_APP4"
~ resultDone = "true"

->END


/*  Appointment 4 has multiple people */


==INDRA_APP5A==

~ currentKnot = "INDRA_APPOINTMENT_5A"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "boastful"

What a victory! I’m sure you’ve heard by now, our first decisive victory on the battlefield. it’s been such a relief.. We’ve had the best night of sleep in months. 

The Senate is finally entertaining the idea of peace talks, especially given they’re in the most advantageous position. 

Zaremir has willingly been given the floor to speak without any objection from his peers. It’s exactly what we’ve been working for!

I’m so proud of him.

~ currentEmotion = "neutral"

Now we need to ensure we have a solid plan to gain support. 

Is there anything that could aid us?  
 


~ nextKnot = "APP5A_Con"

->APP5A_Con

==APP5A_Con==

~ currentKnot = "APP5A_Con"
~ canChoiceBeMade = "yes"

*   A Body potion?

    ~ currentEmotion = "boastful"

    I’m sure we’ve finally seen the end of this war!  /*boastful*/
    ~ nextKnot = "INDRA_APP5A_RESULT_A"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect is the key this time then...

    ~ currentEmotion = "boastful"

    I’m sure we’ve finally seen the end of this war!  /*boastful*/
    ~ nextKnot = "INDRA_APP5A_RESULT_B"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility? I need to be calm for this?

    ~ currentEmotion = "boastful"

    I’m sure we’ve finally seen the end of this war! /*boastful*/
    ~ nextKnot = "INDRA_APP5A_RESULT_C"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   So the key is to be Charismatic?

    ~ currentEmotion = "boastful"

    I’m sure we’ve finally seen the end of this war!  /*boastful*/
    ~ nextKnot = "INDRA_APP5A_RESULT_D"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body potion?: Appointment 5A Result A]
[Intellect is the key this time then...: Appointment 5A Result B]
[Tranquility? I need to be calm for this?: Appointment 5A Result C]
[So the key is to be Charismatic?: Appointment 5A Result D]
*/


==INDRA_APP5A_RESULT_A==

~ currentKnot = "INDRA_APP5A_RESULT_A"
~ currentEmotion = "content"
~ appDone = "true"

Zaremir stayed up every night leading to his speech, but his stamina never wavered. He developed a comprehensive plan and delivered it effortlessly to the Senate. 

    ~ currentEmotion = "boastful"

He didn’t show a single hint of anxiety. Everyone was so impressed. We’re gaining more and more supporters by the hour!

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP5A_RESULT_B==

~ currentKnot = "INDRA_APP5A_RESULT_B"
~ currentEmotion = "content"
~ appDone = "true"

Zaremir spent hours drafting a thoroughly thought out plan. The Senate was impressed by the logical appeal, every topic was covered in a simple comprehensible manner. 

    ~ currentEmotion = "boastful"

I don’t think anyone else could have made it seem so easy. We’re gaining more and more supporters by the hour!


~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END


==INDRA_APP5A_RESULT_C==

~ currentKnot = "INDRA_APP5A_RESULT_C"
~ currentEmotion = "content"
~ appDone = "true"

Zaremir spent hours drafting a thoroughly thought out plan, but what sold it was the poise with which he spoke.  


    ~ currentEmotion = "boastful"

The Senate was impressed by his calm, rational delivery. He never wavered in the face of questioning. We’re gaining more and more supporters by the hour! /*boastful*/

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP5A_RESULT_D==

~ currentKnot = "INDRA_APP5A_RESULT_D"
~ currentEmotion = "content"
~ appDone = "true"

Zaremir spent hours drafting a thoroughly thought out plan, but what sold it was the emotional depth with which he spoke. 

    ~ currentEmotion = "boastful"

The Senate was so impressed. We’re gaining more and more supporters by the hour!/*boastful*/

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP5B==

~ currentKnot = "INDRA_APPOINTMENT_5B"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "boastful"

What a victory! I’m sure you’ve heard by now, our first decisive victory on the battlefield. it’s been such a relief. We’ve had the best night of sleep in months. 

The Senate is finally entertaining the idea of peace talks, especially given they’re in the most advantageous position. 

~ currentEmotion = "surprised"

Zaremir has entered a bid to speak on the Senate floor, he has a plan prepared to present, but there have been objections from his peers. 

~ currentEmotion = "annoyed"

There is even suspicion that Zaremir is a spy. A traitor. Can you believe it! /*annoyed*/

~ currentEmotion = "neutral"

I’m proud of him for enduring all the opposition he’s faced. /*neutral*/

Now we need to assure the Senate of Zaremir’s allegiances and we’ll finally have support.

Is there anything that could aid us?  


~ nextKnot = "APP5B_Con"

->APP5B_Con

==APP5B_Con==

~ currentKnot = "APP5B_Con"
~ canChoiceBeMade = "yes"

*   A Body potion?

~ currentEmotion = "boastful"

    I’m sure we’ve finally seen the end of this war!  /*boastful*/
    ~ nextKnot = "INDRA_APP5B_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect is the key this time then...

~ currentEmotion = "boastful"

    I’m sure we’ve finally seen the end of this war!  /*boastful*/
    ~ nextKnot = "INDRA_APP5B_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility? I need to be calm for this?

~ currentEmotion = "boastful"

    I’m sure we’ve finally seen the end of this war! /*boastful*/
    ~ nextKnot = "INDRA_APP5B_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   So the key is to be Charismatic?

~ currentEmotion = "boastful"

    I’m sure we’ve finally seen the end of this war!  /*boastful*/
    ~ nextKnot = "INDRA_APP5B_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body potion?: Appointment 5B Result A]
[Intellect is the key this time then...: Appointment 5B Result B]
[Tranquility? I need to be calm for this?: Appointment 5B Result C]
[So the key is to be Charismatic?: Appointment 5B Result D]
*/


==INDRA_APP5B_RESULT_A==

~ currentKnot = "INDRA_APP5B_RESULT_A"
~ currentEmotion = "frightened"
~ appDone = "true"

Zaremir was called in for questioning to test his true allegiance. I’m sure he was stressed but he showed no sign of weakness. /*frightened*/

~ currentEmotion = "neutral"

He passed the grueling testing and cleared his name. /*neutral*/

~ currentEmotion = "frightened"

I’m not sure all accepted the results. His office in the Senate was broken into the next day. /*frightened*/

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP5B_RESULT_B==

~ currentKnot = "INDRA_APP5B_RESULT_B"
~ currentEmotion = "frightened"
~ appDone = "true"

Zaremir was called in for questioning to test his true allegiance. The questioning was grueling.  /*frightened*/

~ currentEmotion = "neutral"

You gave him an analytical boost and he was prepared for every question given. /*neutral*/

I fear this boost can be alienating and perceived as arrogance as not everyone seems to be convinced of his innocence.  /*frightened*/

Zaremir’s office at the Senate was broken into the next day. 

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END


==INDRA_APP5B_RESULT_C==

~ currentKnot = "INDRA_APP5B_RESULT_C"
~ currentEmotion = "frightened"
~ appDone = "true"

Zaremir was called in for questioning to test his true allegiance. The questioning was grueling, but he never wavered and faced the test with patience and determination. /*neutral*/

The parliament is split. We’ve gained the vocal support of a handful, but others have made their doubts clear as Zaremir’s office at the Senate was broken into the next day.  /*frightened*/

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP5B_RESULT_D==

~ currentKnot = "INDRA_APP5B_RESULT_D"
~ currentEmotion = "frightened"
~ appDone = "true"

Zaremir was called in for questioning to test his true allegiance. The questioning was grueling, and I was sure that with well articulated communication, Zaremir would easily clear his name. /*neutral*/

But not everyone seems to be convinced as Zaremir’s office at the Senate was broken into the next day.  /*frightened*/

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP6A==

~ currentKnot = "INDRA_APPOINTMENT_6A"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "shock"

I fear things are looking grim. Peace seemed so close only weeks ago. Now with the news of retaliation, it appears our enemy was stronger than we thought.

We continue to lose ground, we continue to lose soldiers, soon, we’ll be unable to recover. 

~ currentEmotion = "sad"

The public have finally turned against the Senate. I’m not surprised, as I understand our citizens have felt neglected for some time. I fear there were even shortcomings in my contributions. 

~ currentEmotion = "frightened"

There are rumors of a revolt on the senate building. It’s all been ignited by a recent report exposing how significantly some of the senators are profiting from the warfare. 

If a mob marches on the senate I fear they will not recognize those who supported their cause and harm them just the same.

~ currentEmotion = "sad"

 I begged Zaremir not to go in today, but he refused to ignore his duty, not that I should expect anything less. 
 
Is there anything I can do to help? I can’t bear the thought of the violence that could occur. 



~ nextKnot = "APP6A_Con"

->APP6A_Con

==APP6A_Con==

~ currentKnot = "APP6A_Con"
~ canChoiceBeMade = "yes"

*   A Body potion?

~ currentEmotion = "content"

    We’ll see if this can ease our situation.   /*boastful*/
    ~ nextKnot = "INDRA_APP6A_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect is the key this time then...

~ currentEmotion = "content"

    We’ll see if this can ease our situation.   /*boastful*/
    ~ nextKnot = "INDRA_APP6A_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility? I need to be calm for this?

~ currentEmotion = "content"

    We’ll see if this can ease our situation.  /*boastful*/
    ~ nextKnot = "INDRA_APP6A_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   So the key is to be Charismatic?

~ currentEmotion = "content"

    We’ll see if this can ease our situation.   /*boastful*/
    ~ nextKnot = "INDRA_APP6A_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body potion?: Appointment 6A Result A]
[Intellect is the key this time then...: Appointment 6A Result B]
[Tranquility? I need to be calm for this?: Appointment 6A Result C]
[So the key is to be Charismatic?: Appointment 6A Result D]
*/


==INDRA_APP6A_RESULT_A==

~ currentKnot = "INDRA_APP6A_RESULT_A"
~ currentEmotion = "frightened"
~ appDone = "true"

There hasn’t been much of an improvement in our situation since we last spoke. I’m just more restless, there is not much I can do but hope.

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP6A_RESULT_B==

~ currentKnot = "INDRA_APP6A_RESULT_B"
~ currentEmotion = "frightened"
~ appDone = "true"

I’ve been trying to find a better solution for days but I’m not making much progress. More late night meetings for Zaremir mean I’m not getting much rest either. 

I hope this all ends soon so that life can go back to normal. I miss worrying about if Zaremir was respected, now all I worry about is who is trying to get him killed.
 

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END


==INDRA_APP6A_RESULT_C==

~ currentKnot = "INDRA_APP6A_RESULT_C"
~ currentEmotion = "frightened"
~ appDone = "true"

I’m feeling some relief after your potion, but I know it won’t last long. I’m hoping Zaremir can rest soon.

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP6A_RESULT_D==

~ currentKnot = "INDRA_APP6A_RESULT_D"
~ currentEmotion = "frightened"
~ appDone = "true"

I have suggested to Zaremir that we arrange to talk to more people in the city to let them know that we are on their side. He’s hesitant, but I’m hopeful that we can calm people down.

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP6B==

~ currentKnot = "INDRA_APPOINTMENT_6B"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "shocked"

I fear things are looking grim. Peace seemed so close only weeks ago. Now with the news of retaliation, it appears our enemy was stronger than we thought. 

We continue to lose ground, we continue to lose soldiers, soon, we’ll be unable to recover. 

~ currentEmotion = "sad"

The public have finally turned against the Senate. I’m not surprised, as I understand our citizens have felt neglected for some time. 

I fear there were even shortcomings in my contributions. I’ve always tried to do my best, but things now seem so bleak.

We seem to be safe for now, but the protestors have been threatening violence on senators.

~ currentEmotion = "frightened"

They seem to be focusing their anger on those that blatantly support the war, but I do worry that when the time comes they may not see friend from foe. 

We’re on their side! Though Zaremir doesn’t wish to make it too public for fear of retaliation. 
The Senate must arrange for a ceasefire, before the violence gets any closer. 

What do you have that can help me support Zaremir to be courageous in this trying time? 



~ nextKnot = "APP6B_Con"

->APP6B_Con

==APP6B_Con==

~ currentKnot = "APP6B_Con"
~ canChoiceBeMade = "yes"

*   A Body potion?

~ currentEmotion = "content"

   Thank you, I hope this can aid us. 
    ~ nextKnot = "INDRA_APP6B_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect is the key this time then...

~ currentEmotion = "content"

    Thank you, I hope this can aid us. 
    ~ nextKnot = "INDRA_APP6B_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility? I need to be calm for this?

~ currentEmotion = "content"

    Thank you, I hope this can aid us. 
    ~ nextKnot = "INDRA_APP6B_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   So the key is to be Charismatic?

~ currentEmotion = "content"

    Thank you, I hope this can aid us. 
    ~ nextKnot = "INDRA_APP6B_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body potion?: Appointment 6B Result A]
[Intellect is the key this time then...: Appointment 6B Result B]
[Tranquility? I need to be calm for this?: Appointment 6B Result C]
[So the key is to be Charismatic?: Appointment 6B Result D]
*/


==INDRA_APP6B_RESULT_A==

~ currentKnot = "INDRA_APP6B_RESULT_A"
~ currentEmotion = "frightened"
~ appDone = "true"

There hasn’t been much of an improvement in our situation since we last spoke. I’m just more restless, there is not much I can do but hope.

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP6B_RESULT_B==

~ currentKnot = "INDRA_APP6B_RESULT_B"
~ currentEmotion = "frightened"
~ appDone = "true"

I’ve been trying to find a better solution for days but I’m not making much progress. More late night meetings for Zaremir mean I’m not getting much rest either. 

I hope this all ends soon so that life can go back to normal. 

I miss worrying about if Zaremir was respected, now all I worry about is who is trying to get him killed.
 

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END


==INDRA_APP6B_RESULT_C==

~ currentKnot = "INDRA_APP6B_RESULT_C"
~ currentEmotion = "frightened"
~ appDone = "true"

I’m feeling some relief after your potion, but I know it won’t last long. I’m hoping Zaremir can rest soon.

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP6B_RESULT_D==

~ currentKnot = "INDRA_APP6B_RESULT_D"
~ currentEmotion = "frightened"
~ appDone = "true"

I have suggested to Zaremir that we arrange to talk to more people in the city to let them know that we are on their side. He’s hesitant, but I’m hopeful that we can calm people down.

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP6C==

~ currentKnot = "INDRA_APPOINTMENT_6C"
~ appDone = "false"
~ resultDone = "false"
~ currentEmotion = "shocked"

I fear things are looking grim. Peace seemed so close only weeks ago. Now with the news of retaliation, it appears our enemy was stronger than we thought.

We continue to lose ground, we continue to lose soldiers, soon, we’ll be unable to recover.

~ currentEmotion = "sad"

The public have finally turned against the Senate. I’m not surprised, as I understand our citizens have felt neglected for some time. I fear there were even shortcomings in my contributions.

~ currentEmotion = "frightened"

Zaremir was followed home a few nights ago. With threats of violence coming from the public, and also, I believe, privately from within the senate, I fear for his life.

~ currentEmotion = "sad"

 I begged Zaremir not to go in today, but he refused to ignore his duty, not that I should expect anything less.
 
Is there anything you can do to help me? I can’t bear the thought of the violence that could occur.
 



~ nextKnot = "APP6C_Con"

->APP6C_Con

==APP6C_Con==

~ currentKnot = "APP6C_Con"
~ canChoiceBeMade = "yes"

*   A Body potion?

~ currentEmotion = "boastful"

   We’ll see if this can ease the situation. 
    ~ nextKnot = "INDRA_APP6C_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Intellect is the key this time then...

~ currentEmotion = "boastful"

    We’ll see if this can ease the situation. 
    ~ nextKnot = "INDRA_APP6C_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   Tranquility? I need to be calm for this?

~ currentEmotion = "boastful"

    We’ll see if this can ease the situation. 
    ~ nextKnot = "INDRA_APP6C_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   So the key is to be Charismatic?

~ currentEmotion = "boastful"

    We’ll see if this can ease the situation. 
    ~ nextKnot = "INDRA_APP6C_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body potion?: Appointment 6C Result A]
[Intellect is the key this time then...: Appointment 6C Result B]
[Tranquility? I need to be calm for this?: Appointment 6C Result C]
[So the key is to be Charismatic?: Appointment 6C Result D]
*/


==INDRA_APP6C_RESULT_A==

~ currentKnot = "INDRA_APP6C_RESULT_A"
~ currentEmotion = "frightened"
~ appDone = "true"

There hasn’t been much of an improvement in our situation since we last spoke. I’m just more restless, there is not much I can do but hope.

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP6C_RESULT_B==

~ currentKnot = "INDRA_APP6C_RESULT_B"
~ currentEmotion = "frightened"
~ appDone = "true"

I’ve been trying to find a better solution for days but I’m not making much progress. More late night meetings for Zaremir mean I’m not getting much rest either. 

I hope this all ends soon so that life can go back to normal. 

I miss worrying about if Zaremir was respected, now all I worry about is who is trying to get him killed.
 

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END


==INDRA_APP6C_RESULT_C==

~ currentKnot = "INDRA_APP6C_RESULT_C"
~ currentEmotion = "frightened"
~ appDone = "true"

I’m feeling some relief after your potion, but I know it won’t last long. I’m hoping Zaremir can rest soon.

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END

==INDRA_APP6C_RESULT_D==

~ currentKnot = "INDRA_APP6C_RESULT_D"
~ currentEmotion = "frightened"
~ appDone = "true"

I have suggested to Zaremir that we arrange to talk to more people in the city to let them know that we are on their side. He’s hesitant, but I’m hopeful that we can calm people down.

~ nextKnot = "INDRA_APP6"
~ resultDone = "true"

->END