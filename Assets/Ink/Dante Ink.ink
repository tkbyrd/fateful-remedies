//This start is an issue and needs to be changed so it doesn't keep running app1
->DANTE_APP1_INTRO

VAR currentKnot = "none"
VAR nextKnot = "none"
VAR currentEmotion = "none"
VAR appDone = "false"
VAR resultDone = "false"
VAR canChoiceBeMade = "no"

==DANTE_APP1_INTRO==

~ currentKnot = "DANTE_APP1_INTRO"
~ resultDone = "false"

~currentEmotion = "boastful"
Shh! Don't say a word! I am about to guess what your next action will be. Ahh... Yes, yes... Mm... You are going to say to me... /*Boastful*/ 

'Hello'.
Are you in awe of my brilliance? It's fine to say so. I often leave my crowds speechless. 

~currentEmotion = "annoyed"
...What? You've never heard of me? Ha! Surely you're kidding. /*annoyed*/

~currentEmotion = "boastful"
I am Dante, the greatest Seer the city of Echeveria has ever seen in its storied history. /*boastful*/
~ currentEmotion = "neutral"
I came to seek your assistance. I have heard that you are an apothecary of the most splendid skill.
I am holding another one of my revelation sessions in a short time, and I'd appreciate a 'bump' to my preaching prowess. You never know who could be in the crowd.
Do you think you can assist me? /*neutral*/

~ canChoiceBeMade = "yes"


~ nextKnot = "APP1_Con"

->APP1_Con

==APP1_Con==

~ currentKnot = "APP1_Con"


*   A Body Potion huh?
    ~ currentEmotion = "neutral"
    Splendid! Thank you for your gracious help! /*boastful*/
    ~currentEmotion = "boastful"

    I must go rest now. I’m attending an exclusive soiree this evening and I need time to get in touch with the fates. 
    
    There is a never-ending demand for my services at such events. 
 
    ~ nextKnot = "DANTE_APP1_RESULT_A"


    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
    
*   An Intellect Potion this time?
    ~ currentEmotion = "neutral"
    Splendid! Thank you for your gracious help! /*boastful*/
    ~currentEmotion = "boastful"

    I must go rest now. I’m attending an exclusive soiree this evening and I need time to get in touch with the fates. 
    
    There is a never-ending demand for my services at such events.
    ~ nextKnot = "DANTE_APP1_RESULT_B"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
    
*   A Tranquility Potion then? 
    ~ currentEmotion = "neutral"
    Splendid! Thank you for your gracious help! /*boastful*/
    ~currentEmotion = "boastful"

    I must go rest now. I’m attending an exclusive soiree this evening and I need time to get in touch with the fates. 
    
    There is a never-ending demand for my services at such events. 
    ~ nextKnot = "DANTE_APP1_RESULT_C"


    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
    
*   A Charisma Potion? 
    ~ currentEmotion = "neutral"
    Splendid! Thank you for your gracious help! /*boastful*/
    ~currentEmotion = "boastful"

    I must go rest now. I’m attending an exclusive soiree this evening and I need time to get in touch with the fates. 
    
    There is a never-ending demand for my services at such events.
    ~ nextKnot = "DANTE_APP1_RESULT_D"



    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body Potion huh?: Appointment 1 Result A]
[An Intellect Potion this time?: Appointment 1 Result B]
[A Tranquility Potion then?: Appointment 1 Result C]
[A Charisma Potion? : Appointment 1 Result D]
*/


==DANTE_APP1_RESULT_A==

~ currentKnot = "DANTE_APP1_RESULT_A"
~ appDone = "true"

~currentEmotion = "boastful"
Hello, friend! I had a great productive day! The markets were packed with people waiting to be enlightened. 


I felt invigorated and I found I could endure long hours on my feet and could sustain under the hot sun. /*boastful*/
~currentEmotion = "neutral"
I wish I could’ve stayed longer, but my spirits were starting to wear. Thank you for your help. /*neutral*/


~ resultDone = "true"
~ nextKnot = "DANTE_APP2"

->END

==DANTE_APP1_RESULT_B==

~ currentKnot = "DANTE_APP1_RESULT_B"
~ appDone = "true"
~currentEmotion = "boastful"
Hello, friend!  I was unbelievably inspired by the fates. The ideas I shared at session were like none my audience had ever heard before! Many stopped in awe of my brilliance. /*boastful*/


Though I have to say, I’m not sure I understood them myself. Thank you for your lovely potion.

~ currentEmotion = "neutral"
~ resultDone = "true"
~ nextKnot = "DANTE_APP2"


->END


==DANTE_APP1_RESULT_C==

~ currentKnot = "DANTE_APP1_RESULT_C"
~ appDone = "true"
~currentEmotion = "boastful"
Hello, friend! I was shining with confidence during my session, not that that’s unusual for me of course. /*boastful*/

But the marketplace found my presence even more irresistible. I gained many followers today! Thanks to you and your magnificent potions! 

~ currentEmotion = "neutral"
~ resultDone = "true"
~ nextKnot = "DANTE_APP2"


->END

==DANTE_APP1_RESULT_D==
~ currentEmotion = "boastful"
~ currentKnot = "DANTE_APP1_RESULT_D"
~ appDone = "true"

Hello, friend! Your magic has brought such illumination into my life! /*boastful*/

The crowd was absolutely enraptured by my session the other day!

The vigor and honesty which accompanied my spoken words were second to none. They fawned over me, as if every word I spoke offered salvation.

You can’t imagine the joy that derives from impacting the lives of so many. Not all are so lucky to experience it. Oh, well, maybe you are. Thanks for your help.
/* can you put the 'you' in 'maybe you are' in italics?  That's how it is in the script, but idk if it's super important */
 
~ currentEmotion = "neutral"
~ resultDone = "true"
~ nextKnot = "DANTE_APP2"


->END

==DANTE_APP2==
~ canChoiceBeMade = "no"
~ currentKnot = "DANTE_APP2"
~ appDone = "false"
~ resultDone = "false"

~ currentEmotion = "angry"
The most shocking thing has happened! It was during my last session, everything was going phenomenal, when a member of the audience cried out and insisted I was a fraud! /*angry*/

~currentEmotion = "annoyed"
 The nerve! I had my loyal supporters, but the disruption was enough to discourage those still needing to be enlightened. /*annoyed*/
 
 Apparently an article was published yesterday claiming that I have faked my visions and claimed my followers were all delusional fools. 
 
A young girl was listening in the crowd when an elderly woman dragged her away. Who would do such a thing?

Some people are so afraid of knowledge.

As you are aware, my speeches provide a great social service by creating healthy discourse.
And as with all geniuses, I’m not without my band of critics.

~currentEmotion = "nervous"

As a result, I’ve gained the attention of the market committee.

They’ve suggested that I stop my public speeches or they will revoke my permit to gather there. I can’t let that happen. They’re just embarrassed. 

Could you give me something that will help me defend my position?
 /*nervous*/


~ canChoiceBeMade = "yes"

~ nextKnot = "APP2_Con"

~ currentEmotion = "neutral"

->APP2_Con

==APP2_Con==
~currentEmotion = "boastful"
~ currentKnot = "APP2_Con"

*   A Body Potion huh?
~currentEmotion = "boastful"
    Thank you! Come by and listen to my brilliance! I won’t let anything stop me from enlightening the public. /*boastful*/
 
    ~ nextKnot = "DANTE_APP2_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"

   ->END
*   An Intellect Potion this time?
~currentEmotion = "boastful"
    Thank you! Come by and listen to my brilliance! I won’t let anything stop me from enlightening the public. /*boastful*/
    
    ~ nextKnot = "DANTE_APP2_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"

   ->END
*   A Tranquility Potion then?
~currentEmotion = "boastful"
    Thank you! Come by and listen to my brilliance! I won’t let anything stop me from enlightening the public. /*boastful*/
    
    ~ nextKnot = "DANTE_APP2_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"

   ->END
*   A Charisma Potion? 
~currentEmotion = "boastful"
    Thank you! Come by and listen to my brilliance! I won’t let anything stop me from enlightening the public. /*boastful*/
    
    ~ nextKnot = "DANTE_APP2_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"

   ->END

/*
[A Body Potion huh?: Appointment 2 Result A]
[An Intellect Potion this time?: Appointment 2 Result B]
[A Tranquility Potion then?: Appointment 2 Result C]
[A Charisma Potion? : Appointment 2 Result D]
*/


==DANTE_APP2_RESULT_A==
~currentEmotion = "boastful"
~ currentKnot = "DANTE_APP2_RESULT_A"
~ appDone = "true"

Hello, friend. Since I saw you last I have been focusing more on improving myself rather than listening to the nonsense spewing out these days.

I aim to look and feel glorious every day. Your potion has magnified my already outstanding beauty. 


~ currentEmotion = "neutral"
~ resultDone = "true"
 ~ nextKnot = "DANTE_APP3"

->END

==DANTE_APP2_RESULT_B==
~currentEmotion = "boastful"
~ currentKnot = "DANTE_APP2_RESULT_B"
~ appDone = "true"

Hello, friend.  The market committee is no longer concerned about the reaction at my session the other day. They realized I am a valuable asset to their community.

When I offered to relocate and take all of my prospective customers with me, they quickly changed their minds.

I am even encouraged to stay and bring in more crowds if I want to speak more often. 

Your potion seemed to reveal this offer to me, I knew they couldn’t refuse. Thanks again. 


~ currentEmotion = "neutral"
~ resultDone = "true"
~ nextKnot = "DANTE_APP3"

->END


==DANTE_APP2_RESULT_C==
~currentEmotion = "neutral"
~ currentKnot = "DANTE_APP2_RESULT_C"
~ appDone = "true"

Hello, friend. I wished to avoid a drawn-out confrontation with the committee. Upon taking your potion the solution was revealed to me. 

I have organized a petition to demonstrate the need for my presence at the market. My followers eagerly signed and supported me. 
~currentEmotion = "boastful"
The committee can’t deny a spot to someone as beloved as me. 

~ nextKnot = "DANTE_APP3"
~ resultDone = "true"
~currentEmotion = "neutral"

->END

==DANTE_APP2_RESULT_D==
~currentEmotion = "boastful"
~ currentKnot = "DANTE_APP2_RESULT_D"
~ appDone = "true"

Hello, friend. I made my case to the committee.

 Of course as usual I was nothing but complementary towards them, reminding them of how delightful they have been to work with over the years. 

They seemed moved by my words of gratitude and agreed to let me continue to meet, given that I’ve never caused a serious issue before. 

Your potion proved a little bit of flattery can go a long way. Thank you for the help. 


~ currentEmotion = "neutral"
~ resultDone = "true"
~ nextKnot = "DANTE_APP3"

->END



==DANTE_APP3A==
~ currentEmotion = "boastful"
~ currentKnot = "DANTE_APP3A"
~ appDone = "false"
~ resultDone = "false"
~ canChoiceBeMade = "no"

It's getting more and more difficult to keep the crowds off me these days. I am eternally grateful for your efforts. Which is why I wish to divine your entire course of life for free! /*boastful*/

No, no, no, please, I insist! 

...Hrm... Yes, yes. Ah! Gracious... I see... I see that you... You will... /*neutral*/
~ currentEmotion = "neutral"
You will get older and DIE! 

~ currentEmotion = "nervous"
...Pardon? How? When? Oh, erm... I'm afraid I didn't quite catch that part. /*nervous*/

~ currentEmotion = "neutral"
Enough of all that. A prominent government official is among those enraptured by my speaking prowess. /*neutral*/

~ currentEmotion = "boastful"
He wants me for an official court position! Imagine that! I'd be advising our government body, divining the very future of our nation! It's a dream come true! /*boastful*/

Oh, don’t worry about me. This ‘war’ is nothing to stress over. Just a little smoke and gunpowder to scare each other and convince everyone it's a bad idea, is all!
~ currentEmotion = "neutral"
I’m to start tomorrow on a trial period. Please, help me if you can. I've desired something like this position for ages. /*neutral*/


~ nextKnot = "APP3A_Con"


~ canChoiceBeMade = "yes"

->APP3A_Con

==APP3A_Con==
~ currentEmotion = "boastful"
~ currentKnot = "APP3A_Con"

*   A Body Potion huh?
    This should be more than enough, coupled with my innate wisdom! /*boastful*/
 
    ~ nextKnot = "DANTE_APP3A_RESULT_A"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
     ~ canChoiceBeMade = "no"
    ->END
*   An Intellect Potion this time?
    This should be more than enough, coupled with my innate wisdom! /*boastful*/
    
    ~ nextKnot = "DANTE_APP3A_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
     ~ canChoiceBeMade = "no"
    ->END
*   A Tranquility Potion then?
    This should be more than enough, coupled with my innate wisdom! /*boastful*/
    
    ~ nextKnot = "DANTE_APP3A_RESULT_C"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
     ~ canChoiceBeMade = "no"
    ->END
*   A Charisma Potion? 
    This should be more than enough, coupled with my innate wisdom! /*boastful*/
    
    ~ nextKnot = "DANTE_APP3A_RESULT_D"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body Potion huh?: Appointment 3A Result A]
[An Intellect Potion this time?: Appointment 3A Result B]
[A Tranquility Potion then?: Appointment 3A Result C]
[A Charisma Potion? : Appointment 3A Result D]
*/


==DANTE_APP3A_RESULT_A==

~ currentKnot = "DANTE_APP3A_RESULT_A"

Hello, friend! I’ve officially earned the position! Not that I ever doubted that I would. /*boastful*/

Not only were my predictions spectacular, but they were impressed with my health. I assured them I could handle the strain of the position. 

~ currentEmotion = "neutral"
~ resultDone = "true"
~ nextKnot = "DANTE_APP4"

->END

==DANTE_APP3A_RESULT_B==

~ currentKnot = "DANTE_APP3A_RESULT_B"

Hello, friend! I’ve officially earned the position! Not that I ever doubted that I would. /*boastful*/

My predictions were spectacular, but they were aided in their accuracy by my skillful deductions. I was able to better direct the fates to find what my clients were searching for. 
~ currentEmotion = "neutral"
~ resultDone = "true"
~ nextKnot = "DANTE_APP4"


->END


==DANTE_APP3A_RESULT_C==

~ currentKnot = "DANTE_APP3A_RESULT_C"

Hello, friend! I’ve officially earned the position! Not that I ever doubted that I would. /*boastful*/

Not only were my predictions spectacular, but they were impressed with my calm energy. No matter the interruption, no matter the news about of war developments. I wasn’t frayed. 

It’s exactly what they need at a time like this.  
~ currentEmotion = "neutral"
~ resultDone = "true"
~ nextKnot = "DANTE_APP4"


->END

==DANTE_APP3A_RESULT_D==

~ currentKnot = "DANTE_APP3A_RESULT_D"
~ nextKnot = "DANTE_APP4"


Hello, friend! I’ve officially earned the position! Not that I ever doubted that I would. /*boastful*/

Though, the Senate is no stranger to being charmed, and I worry I came across as disingenuous. But they were satisfied enough with my predictions to overlook that. /*neutral*/

~ currentEmotion = "neutral"
~ resultDone = "true"
~ nextKnot = "DANTE_APP4"

->END




==DANTE_APP3B==

~ currentKnot = "DANTE_APP3B"
~ appDone = "false"
~ resultDone = "false"
~ canChoiceBeMade = "no"

~ currentEmotion = "boastful"

It's getting more and more difficult to keep the crowds off me these days. I am eternally grateful for your efforts. Which is why I wish to divine your entire course of life for free! /*boastful*/

No, no, no, please, I insist! 

...Hrm... Yes, yes. Ah! Gracious... I see... I see that you... You will... /*neutral*/
~ currentEmotion = "neutral"
You will get older and DIE! 

~ currentEmotion = "nervous"
...Pardon? How? When? Oh, erm... I'm afraid I didn't quite catch that part. /*nervous*/

~ currentEmotion = "neutral"
Enough of all that. A prominent government official is among those enraptured by my speaking prowess. /*neutral*/

~ currentEmotion = "boastful"
He wants me for an official court position! Imagine that! I'd be advising our government body, divining the very future of our nation! It's a dream come true! /*boastful*/

Oh, don’t worry about me. This ‘war’ is nothing to stress over. Just a little smoke and gunpowder to scare each other and convince everyone it's a bad idea, is all!
~ currentEmotion = "neutral"
I’m to start tomorrow on a trial period. Please, help me if you can. I've desired something like this position for ages. /*neutral*/

And I don’t want to take away any attention from my followers. They’ve always had the utmost support for me. They deserve my undivided attention. 

~ nextKnot = "APP3B_Con"


~ canChoiceBeMade = "yes"

->APP3B_Con

==APP3B_Con==

~ currentKnot = "APP3B_Con"
*   A Body Potion huh?
    ~ currentEmotion = "boastful"
    This should be more than enough, coupled with my innate wisdom! /*boastful*/
 
    ~ nextKnot = "DANTE_APP3B_RESULT_A"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"

    ->END
*   An Intellect Potion this time?
    ~ currentEmotion = "boastful"
    This should be more than enough, coupled with my innate wisdom! /*boastful*/
    
    ~ nextKnot = "DANTE_APP3B_RESULT_B"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Tranquility Potion then?
    ~ currentEmotion = "boastful"
    This should be more than enough, coupled with my innate wisdom! /*boastful*/
    
    ~ nextKnot = "DANTE_APP3B_RESULT_C"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Charisma Potion? 
    ~ currentEmotion = "boastful"
    This should be more than enough, coupled with my innate wisdom! /*boastful*/
    
    ~ nextKnot = "DANTE_APP3B_RESULT_D"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body Potion huh?: Appointment 3B Result A]
[An Intellect Potion this time?: Appointment 3B Result B]
[A Tranquility Potion then?: Appointment 3B Result C]
[A Charisma Potion? : Appointment 3B Result D]
*/


==DANTE_APP3B_RESULT_A==

~ currentKnot = "DANTE_APP3B_RESULT_A"
~ currentEmotion = "neutral"

Hello, friend! My nights have been sleepless. My mind is tired, but my body is full of restless energy. I’ve started taking walks through the city at night. /*neutral*/

It’s inspired me! I will take the position, and use the benefits to assist my followers through these troubling times. 

~ currentEmotion = "boastful"
My notoriety would live on for sure! /*boastful*/



~ resultDone = "true"
~ nextKnot = "DANTE_APP4"

->END

==DANTE_APP3B_RESULT_B==

~ currentKnot = "DANTE_APP3B_RESULT_B"

~ currentEmotion = "neutral"

Hello, friend! I was struck by inspiration. In fact, I would call it a vision, brought to me by the fates themselves. /*neutral*/

~ currentEmotion = "boastful"

I will take the position, and use the benefits to assist my followers through these troubling times. My notoriety would live on for sure! /*boastful*/



~ resultDone = "true"
~ nextKnot = "DANTE_APP4"

->END


==DANTE_APP3B_RESULT_C==

~ currentKnot = "DANTE_APP3B_RESULT_C"

~ currentEmotion = "neutral"

Hello, friend! I was struck by inspiration. In fact, I would call it a vision, brought to me by the fates themselves. /*neutral*/

~ currentEmotion = "boastful"

I will take the position, and use the benefits to assist my followers through these troubling times. My notoriety would live on for sure! /*boastful*/


~ resultDone = "true"
~ nextKnot = "DANTE_APP4"

->END

==DANTE_APP3B_RESULT_D==

~ currentKnot = "DANTE_APP3B_RESULT_D"

~ currentEmotion = "nervous"

Hello, friend! I held a session at the marketplace. It’s become common knowledge that I was recruited by the Senate for my services. My followers were dubious of my intentions. /*nervous*/

~ currentEmotion = "neutral"

We spoke and I determined I will take the position, and use the benefits to assist my followers through these troubling times. /*neutral*/

~ currentEmotion = "boastful"

My notoriety would live on for sure! /*boastful*/


~ currentEmotion = "neutral"
~ resultDone = "true"
~ nextKnot = "DANTE_APP4"

->END



==DANTE_APP4A==

~ currentKnot = "DANTE_APP4A"
~ appDone = "false"
~ resultDone = "false"

~ currentEmotion = "boastful"

I'm finally being hailed as a genius! The Senate love me! /*boastful*/


It feels great to have meaningful work pursuing my true calling! I can’t believe it.

~ currentEmotion = "angry"

Not everyone is as thrilled as I. Yesterday, I held a marketplace session. You must have heard about my utter embarrassment. I still haven’t been able to wash away the scent of tomatoes.  /*angry*/

The citizens of Echveria are upset with my position at the Senate. 

~ currentEmotion = "boastful"

Me? Glum? Pah! Ridiculous! Nothing can bring Dante down! I've got boundless resources of energy. And piles of money. /*annoyed*/

~ currentEmotion = "neutral"

It’s time I embrace my new life as a court official and integrate myself into the social circles of high society. I’d say I’ve more than earned the part./*neutral*/ 

I’ve heard word Indra is hosting an exclusive dinner party. Just the affair I need to make my social debut. 

I’m sure my invitation is lost in the mail. Things are slower these days. 

Do you have any resources that could get me into the party? 


~ nextKnot = "APP4A_Con"

~ currentEmotion = "neutral"

~ canChoiceBeMade = "yes"

->APP4A_Con

==APP4A_Con==

~ currentKnot = "APP4A_Con"

*   A Body Potion huh?

    ~ currentEmotion = "boastful"

    I get invited to these kinds of soirees all the time to tell fortunes. I’m sure everyone will be thrilled to see me! /*boastful*/
 
    ~ nextKnot = "DANTE_APP4A_RESULT_A"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   An Intellect Potion this time?

    ~ currentEmotion = "boastful"

    I get invited to these kinds of soirees all the time to tell fortunes. I’m sure everyone will be thrilled to see me! /*boastful*/
    
    ~ nextKnot = "DANTE_APP4A_RESULT_B"

    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Tranquility Potion then?

    ~ currentEmotion = "boastful"

    I get invited to these kinds of soirees all the time to tell fortunes. I’m sure everyone will be thrilled to see me! /*boastful*/
    
    ~ nextKnot = "DANTE_APP4A_RESULT_C"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Charisma Potion? 

    ~ currentEmotion = "boastful"

    I get invited to these kinds of soirees all the time to tell fortunes. I’m sure everyone will be thrilled to see me! /*boastful*/
    
    ~ nextKnot = "DANTE_APP4A_RESULT_D"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body Potion huh?: Appointment 4A Result A]
[An Intellect Potion this time?: Appointment 4A Result B]
[A Tranquility Potion then?: Appointment 4A Result C]
[A Charisma Potion? : Appointment 4A Result D]
*/


==DANTE_APP4A_RESULT_A==

~ currentKnot = "DANTE_APP4A_RESULT_A"

~ currentEmotion = "sad"

Hello, friend. My attempt at socializing was a complete failure. I couldn’t even get into the party./*sad*/ 

I greeted the doorman, and before I could explain the issue with the invitation, I had crushed his hand with the firm grip of my handshake. I was chased off from the property immediately.  	



~ resultDone = "true"

->END

==DANTE_APP4A_RESULT_B==

~ currentKnot = "DANTE_APP4A_RESULT_B"

~ currentEmotion = "boastful"

Hello, friend! I was able to enter the party easily without my invitation. /*boastful*/

I met the man who sells the produce to the cook at the marketplace and volunteered my services to deliver the food the order to the house. I joined the party with no issue. 

I did have to leave early. It was… suggested later by the doorman, but I assure it’s only because I wasn’t feeling well.  /*neutral*/



~ resultDone = "true"

->END


==DANTE_APP4A_RESULT_C==

~ currentKnot = "DANTE_APP4A_RESULT_C"

~ currentEmotion = "adult"

Hello, friend. The party, I completely forgot. I’ve had a bought of laziness. I’ve spent my days sleeping on the couch.  /*adult*/



~ resultDone = "true"

->END

==DANTE_APP4A_RESULT_D==

~ currentKnot = "DANTE_APP4A_RESULT_D"

~ currentEmotion = "boastful"

Hello, friend! I had no problem getting into the party. I simply explained the issue with the invitation to the doorman and he happily let me in. /*boastful*/

I did have to leave early. It was… suggested later by the doorman, but I assure it’s only because I wasn’t feeling well.  



~ resultDone = "true"

->END




==DANTE_APP4B==

~ currentKnot = "DANTE_APP4B"
~ appDone = "false"
~ resultDone = "false"

~ currentEmotion = "boastful"

I'm finally being hailed as a genius! The Senate love me! /*boastful*/


It feels great to have meaningful work pursuing my true calling! I can’t believe it. 


What great success! Nothing can bring Dante down! I've got boundless resources of energy. And piles of money. /*boastful*/

~ currentEmotion = "neutral"

I’m ready to embrace my new life as a court official and integrate myself into the social circles of high society.

I’ll even use this as an opportunity to garner support for my efforts to help my dear devoted followers. I’m sure there are plenty of bored Senate spouses waiting for a project!   

I’ve heard word Indra is hosting an exclusive dinner party. Just the affair I need to make my social debut. I’m sure my invitation is lost in the mail. Things are slower these days. 
/*neutral*/

Do you have any resources that could get me into the party? 


~ nextKnot = "APP4B_Con"

~ currentEmotion = "neutral"
~ canChoiceBeMade = "yes"

->APP4B_Con

==APP4B_Con==

~ currentKnot = "APP4B_Con"

*   A Body Potion huh?

    ~ currentEmotion = "boastful"

    I get invited to these kinds of soirees all the time to tell fortunes. I’m sure everyone will be thrilled to see me! /*boastful*/
 
    ~ nextKnot = "DANTE_APP4B_RESULT_A"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   An Intellect Potion this time?

    ~ currentEmotion = "boastful"

    I get invited to these kinds of soirees all the time to tell fortunes. I’m sure everyone will be thrilled to see me!  /*boastful*/
    
    ~ nextKnot = "DANTE_APP4B_RESULT_B"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Tranquility Potion then?

    ~ currentEmotion = "boastful"

    I get invited to these kinds of soirees all the time to tell fortunes. I’m sure everyone will be thrilled to see me!  /*boastful*/
    
    ~ nextKnot = "DANTE_APP4B_RESULT_C"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Charisma Potion? 

    ~ currentEmotion = "boastful"

    I get invited to these kinds of soirees all the time to tell fortunes. I’m sure everyone will be thrilled to see me!  /*boastful*/
    
    ~ nextKnot = "DANTE_APP4B_RESULT_D"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body Potion huh?: Appointment 4B Result A]
[An Intellect Potion this time?: Appointment 4B Result B]
[A Tranquility Potion then?: Appointment 4B Result C]
[A Charisma Potion? : Appointment 4B Result D]
*/


==DANTE_APP4B_RESULT_A==

~ currentKnot = "DANTE_APP4B_RESULT_A"

    ~ currentEmotion = "sad"

Hello, friend. My attempt at socializing was a complete failure. I couldn’t even get into the party. /*sad*/ 

I greeted the doorman, and before I could explain the issue with the invitation, I had crushed his hand with the firm grip of my handshake. I was chased off from the property immediately. 	

~ currentEmotion = "neutral"
~ resultDone = "true"

->END

==DANTE_APP4B_RESULT_B==

~ currentKnot = "DANTE_APP4B_RESULT_B"

    ~ currentEmotion = "boastful"

Hello, friend! I was able to enter the party easily without my invitation. /*boastful*/

I met the man who sells the produce to the cook at the marketplace and volunteered my services to deliver the food the order to the house. I joined the party with no issue.  

    ~ currentEmotion = "neutral"

I did have to leave early. It was… suggested later by the doorman, but I assure it’s only because I wasn’t feeling well.  I’m sure my followers will understand. /*neutral*/

~ resultDone = "true"

->END


==DANTE_APP4B_RESULT_C==

~ currentKnot = "DANTE_APP4B_RESULT_C"

    ~ currentEmotion = "adult"

Hello, friend. The party, I completely forgot. I’ve had a bought of laziness. I’ve spent my days sleeping on the couch. /*adult*/


~ resultDone = "true"

->END

==DANTE_APP4B_RESULT_D==

~ currentKnot = "DANTE_APP4B_RESULT_D"

    ~ currentEmotion = "boastful"

Hello, friend! I had no problem getting into the party. I simply explained the issue with the invitation to the doorman and he happily let me in. /*boastful*/

I did have to leave early. It was… suggested later by the doorman, but I assure it’s only because I wasn’t feeling well.  I’m sure my followers will understand.



~ resultDone = "true"

->END



/* Appointment 5 has multiple people */



==DANTE_APP6A==

~ currentKnot = "DANTE_APP6A"
~ appDone = "false"
~ resultDone = "false"

~ currentEmotion = "fucked"

I…I’m so sorry. I’m in complete shock. You won’t believe what just happened. I had to stop by right away. 

I was stopped in the street on my way to work, that’s not unusual, I am always available for one of my followers seeking counsel. 

But today! A man threatened my life. Apparently, my work with the Senate is seen as betrayal. It seems I was the voice of the people. 

~ currentEmotion = "angry"

That was until I consented to being used as the Senate’s propaganda tool to support the war. The accusation! Thankfully the streets were quiet so no one else saw the interaction. 

~ currentEmotion = "sad"

It was never my intention to hurt anyone. In fact, I had hoped my participation would help avoid casualties.

I know everyone is on edge with the city’s safety threatened, but it appears the public has really turned against me.  

I still can’t think straight. What do you suggest? 



~ nextKnot = "APP6A_Con"

~ currentEmotion = "neutral"
~ canChoiceBeMade = "yes"

->APP6A_Con

==APP6A_Con==

~ currentKnot = "APP6A_Con"

*   A Body Potion huh?

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
 
    ~ nextKnot = "DANTE_APP6A_RESULT_A"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   An Intellect Potion this time?

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
    
    ~ nextKnot = "DANTE_APP6A_RESULT_B"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Tranquility Potion then?

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
    
    ~ nextKnot = "DANTE_APP6A_RESULT_C"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Charisma Potion? 

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
    
    ~ nextKnot = "DANTE_APP6A_RESULT_D"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body Potion huh?: Appointment 6A Result A]
[An Intellect Potion this time?: Appointment 6A Result B]
[A Tranquility Potion then?: Appointment 6A Result C]
[A Charisma Potion? : Appointment 6A Result D]
*/


==DANTE_APP6A_RESULT_A==

~ currentKnot = "DANTE_APP6A_RESULT_A"

    ~ currentEmotion = "sad"

Hello, friend. I have found myself simply quickening my pace through the streets, and taking longer routes to work. 

So far I have avoided any further altercations. I appreciate your potion giving me the strength to move on. 
	

~ currentEmotion = "neutral"
~ resultDone = "true"

->END

==DANTE_APP6A_RESULT_B==

~ currentKnot = "DANTE_APP6A_RESULT_B"

    ~ currentEmotion = "boastful"

Hello, friend! I have successfully addressed the concerns of some of my followers in a delightful speech. No doubt inspired by your fantastic potion.  

I was able to explain that the call of a seer is to the people, and the voice of the people can be best expressed in their governing body. 

I hope this work continues to grow in my favor. 

~ resultDone = "true"

->END


==DANTE_APP6A_RESULT_C==

~ currentKnot = "DANTE_APP6A_RESULT_C"

    ~ currentEmotion = "adult"

Hello, friend. I appreciate the work you do with your potions, as the one you gave me has granted me some peace. 

I have realized I don’t need to worry about the gripes of a few stray people. They don’t matter in the long run. I’m sure everything will work itself out. 



~ resultDone = "true"

->END

==DANTE_APP6A_RESULT_D==

~ currentKnot = "DANTE_APP6A_RESULT_D"

    ~ currentEmotion = "boastful"

Hello, friend! I have addressed the concerns of the people. 

They believed I had abandoned their interests in favor of the Senate, so I proved in my speech today that I am still their beloved Seer.

 I don’t think I’ll have any more unpleasant encounters in the streets, I’m certain my comforting words changed any ill will in their hearts. Thank you for your wonderful potion. 


~ resultDone = "true"

->END

==DANTE_APP6B==

~ currentKnot = "DANTE_APP6B"
~ appDone = "false"
~ resultDone = "false"

~ currentEmotion = "fucked"

I…I’m so sorry. I’m in complete shock. You won’t believe what just happened. I had to stop by right away. 

I was attending meetings at the Senate as usual when I was completely lambasted. They’ve accused me of inciting civil unrest. Stating I’d implied blame on the Senate.

~ currentEmotion = "angry"

All I did was use my talents to do exactly what they asked. I provided outcomes to war strategies while addressing fears of the public. 

I feel as if I’m nothing but a tool to them. Now I’m to blame for a riot! A crowd was already forming on the steps. I was lucky enough to sneak away with anyone noticing. 
It was never my intention for anyone to get hurt. In fact, I had hoped my participation would help avoid any unrest.

~ currentEmotion = "sad"

I know everyone is on edge with the city’s safety threatened, but it appears the Senate has really turned against me.  

I still can’t think straight. What do you suggest? 


~ nextKnot = "APP6B_Con"

~ currentEmotion = "neutral"
~ canChoiceBeMade = "yes"

->APP6B_Con

==APP6B_Con==

~ currentKnot = "APP6B_Con"

*   A Body Potion huh?

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
 
    ~ nextKnot = "DANTE_APP6B_RESULT_A"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   An Intellect Potion this time?

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
    
    ~ nextKnot = "DANTE_APP6B_RESULT_B"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Tranquility Potion then?

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
    
    ~ nextKnot = "DANTE_APP6B_RESULT_C"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Charisma Potion? 

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
    
    ~ nextKnot = "DANTE_APP6B_RESULT_D"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body Potion huh?: Appointment 6B Result A]
[An Intellect Potion this time?: Appointment 6B Result B]
[A Tranquility Potion then?: Appointment 6B Result C]
[A Charisma Potion? : Appointment 6B Result D]
*/


==DANTE_APP6B_RESULT_A==

~ currentKnot = "DANTE_APP6B_RESULT_A"

    ~ currentEmotion = "sad"

Hello, friend. I have found myself simply quickening my pace through the streets, and taking longer routes to work. 

So far I have avoided any further altercations. I appreciate your potion giving me the strength to move on. 

	

~ currentEmotion = "neutral"
~ resultDone = "true"

->END

==DANTE_APP6B_RESULT_B==

~ currentKnot = "DANTE_APP6B_RESULT_B"

    ~ currentEmotion = "boastful"

Hello, friend! I have successfully addressed the concerns of some of my followers in a delightful speech. No doubt inspired by your fantastic potion.  

I was able to explain that the call of a seer is to the people, and the voice of the people can be best expressed in their governing body. 

I hope this work continues to grow in my favor. 


~ resultDone = "true"

->END


==DANTE_APP6B_RESULT_C==

~ currentKnot = "DANTE_APP6B_RESULT_C"

    ~ currentEmotion = "adult"

Hello, friend. I appreciate the work you do with your potions, as the one you gave me has granted me some peace. 

I have realized I don’t need to worry about the gripes of a few stray people. They don’t matter in the long run. I’m sure everything will work itself out. 


~ resultDone = "true"

->END

==DANTE_APP6B_RESULT_D==

~ currentKnot = "DANTE_APP6B_RESULT_D"

    ~ currentEmotion = "boastful"

Hello, friend! I have addressed the concerns of the people.

They believed I had abandoned their interests in favor of the Senate, so I proved in my speech today that I am still their beloved Seer.

 I don’t think I’ll have any more unpleasant encounters in the streets, I’m certain my comforting words changed any ill will in their hearts. Thank you for your wonderful potion. 


~ resultDone = "true"

->END

==DANTE_APP6C==

~ currentKnot = "DANTE_APP6C"
~ appDone = "false"
~ resultDone = "false"

~ currentEmotion = "fucked"

I…I’m so sorry. I’m in complete shock. You won’t believe what just happened. I had to stop by right away. 

I  attended meetings at the Senate as usual. The mood was somber, reports just arrived from the recent battle, the casualties do nothing but increase. 

~ currentEmotion = "angry"

It was then that I was asked to aid in a cover up! The Senate wants me to deny the most recent information and provide a more ‘uplifting’ perspective. 

I’ve used my talents to do exactly what they asked. I provided outcomes to war strategies while addressing fears of the public. 

~ currentEmotion = "sad"

But that’s not the same as lying about current conditions, is it? 

I know everyone is on edge with the city’s safety threatened, but I feel the Senate has crossed a line. 

I still can’t think straight. What do you suggest? 


~ nextKnot = "APP6C_Con"

~ currentEmotion = "neutral"
~ canChoiceBeMade = "yes"

->APP6C_Con

==APP6C_Con==

~ currentKnot = "APP6C_Con"

*   A Body Potion huh?

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
 
    ~ nextKnot = "DANTE_APP6C_RESULT_A"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   An Intellect Potion this time?

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
    
    ~ nextKnot = "DANTE_APP6C_RESULT_B"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Tranquility Potion then?

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
    
    ~ nextKnot = "DANTE_APP6C_RESULT_C"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END
*   A Charisma Potion? 

    ~ currentEmotion = "boastful"

    You provide the greatest relief. 
    
    ~ nextKnot = "DANTE_APP6C_RESULT_D"
    ~ appDone = "true"
    ~ canChoiceBeMade = "no"
    ->END

/*
[A Body Potion huh?: Appointment 6C Result A]
[An Intellect Potion this time?: Appointment 6C Result B]
[A Tranquility Potion then?: Appointment 6C Result C]
[A Charisma Potion? : Appointment 6C Result D]
*/


==DANTE_APP6C_RESULT_A==

~ currentKnot = "DANTE_APP6C_RESULT_A"

    ~ currentEmotion = "sad"

Hello, friend. I have found myself simply quickening my pace through the streets, and taking longer routes to work. 

So far I have avoided any further altercations. I appreciate your potion giving me the strength to move on. 
	

~ currentEmotion = "neutral"
~ resultDone = "true"

->END

==DANTE_APP6C_RESULT_B==

~ currentKnot = "DANTE_APP6C_RESULT_B"

    ~ currentEmotion = "boastful"

Hello, friend! I have successfully addressed the concerns of some of my followers in a delightful speech. No doubt inspired by your fantastic potion.  

I was able to explain that the call of a seer is to the people, and the voice of the people can be best expressed in their governing body. 

I hope this work continues to grow in my favor. 

~ resultDone = "true"

->END


==DANTE_APP6C_RESULT_C==

~ currentKnot = "DANTE_APP6C_RESULT_C"

    ~ currentEmotion = "adult"

Hello, friend. I appreciate the work you do with your potions, as the one you gave me has granted me some peace. 

I have realized I don’t need to worry about the gripes of a few stray people. They don’t matter in the long run. I’m sure everything will work itself out. 

~ resultDone = "true"

->END

==DANTE_APP6C_RESULT_D==

~ currentKnot = "DANTE_APP6C_RESULT_D"

    ~ currentEmotion = "boastful"

Hello, friend! I have addressed the concerns of the people. 

They believed I had abandoned their interests in favor of the Senate, so I proved in my speech today that I am still their beloved Seer.

 I don’t think I’ll have any more unpleasant encounters in the streets, I’m certain my comforting words changed any ill will in their hearts. Thank you for your wonderful potion. 


~ resultDone = "true"

->END
