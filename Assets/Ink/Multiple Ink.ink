->MEILIN_INDRA_APP4

VAR currentKnot = "none"
VAR nextKnot = "none"
VAR currentEmotion = "none"
VAR appDone = "false"
VAR resultDone = "false"
VAR canChoiceBeMade = "no"
//changes name tag text
VAR whoIsSpeaking = "nobody"

//catalogues which characters are active
//gets set at the start of the scene
//if only two characters just set a and b (only do more if there's more people)
//(poppy and zinnia count as one person)
VAR characterA = "nobody" 
VAR characterB = "nobody" 
VAR characterC = "nobody" 
VAR characterD = "nobody"

VAR whoLeft = "nobody"

==MEILIN_INDRA_APP4==

~ currentKnot = "MEILIN_INDRA_APP4"
~ appDone = "false"
~ resultDone = "false"
~ characterA = "Mei Lin" 
~ characterB = "Indra" 

/* MEI LIN ENTERS */
~ whoIsSpeaking = "Mei Lin"
/* MEI LIN: */
The Homefront Action Movement has had an excellent yield of crops. We’ve been distributing produce across the city.

Still, it feels like we aren’t moving fast enough. People in the city are starving. 

More and more garden space is being donated to grow crops. We have a team educating people on gardening. But there must be more we can do.
~ whoIsSpeaking = "Indra"

/* INDRA: */
So good to see you Mei Lin! I don’t mean to interrupt, but I have an urgent matter. This will just take a moment. 

I am hosting a dinner party to solidify my position among the senators. They could all use a night of fun. Spirits have been down with war matters to tend to.

It was abnormally difficult to acquire enough provisions for a five-course meal, the prices have skyrocketed, but I was able to cope. 

Yet, I’m unable to cope with my staff. No matter what I do, no one will agree to work in the evenings anymore!

How can I inspire them to assist me for this crucial event?  


~ whoIsSpeaking = "Mei Lin"
/* MEI LIN: */
They don’t need inspiration! They need to do everything they can to survive. 

They need to be with their families before they are taken away. They need to tend to their gardens or there won’t be anything to eat. 

This city has been blocked from the rural territories. Food is no longer able to be shipped in.

Of course, that doesn’t affect those with big houses. You and your friends are the only ones who can afford what little resources are able to make it into the city. 

~ whoIsSpeaking = "Indra"

/* INDRA: */

You think I don’t know people are suffering? You think I don’t feel the weight every night? 

Can’t you see I’m trying to put an end to these unjust tragedies. With this dinner Zaremir could finally win influence over the generals and he will put an end to the war. 

That reminds me, the floral arrangements, will they be ready? 

~ whoIsSpeaking = "Mei Lin"
/* MEI LIN: */
You don’t have to worry. Your order will be completed on time and with the utmost care as
always.

/* INDRA: */
I’m immensely grateful Mei Lin. This dinner could change the course of the war.






~ nextKnot = "MEILIN_INDRA_APP4"

~ currentEmotion = "neutral"
->MEILIN_INDRA_APP4_Con1

==MEILIN_INDRA_APP4_Con1==

~ currentKnot = "MEILIN_INDRA_APP4_Con1"




/* [Give Potion to Mei Lin] */

*   You give The Body Potion
    /* [Give Potion to Indra] */
    I’m sure we are on our way to peace!

    /* INDRA LEAVES */

    ~ currentEmotion = "neutral"
    ->MEILIN_INDRA_APP4_Con2
*   You give The Intellect Potion

    /* INDRA: */
    I’m sure we are on our way to peace!
    ~ currentEmotion = "neutral"
    ->MEILIN_INDRA_APP4_Con2
*   You give The Tranquility Potion

    /* INDRA: */
    I’m sure we are on our way to peace!

    ~ currentEmotion = "neutral"
    ->MEILIN_INDRA_APP4_Con2
*   You give The Charisma Potion

    /* INDRA: */
    I’m sure we are on our way to peace!
    ~ currentEmotion = "neutral"
    ->MEILIN_INDRA_APP4_Con2
    


==MEILIN_INDRA_APP4_Con2==

~ currentKnot = "MEILIN_INDRA_APP4_Con2"
/* MEI LIN: */
Maybe I shouldn’t be so hard on them. I realize I’ve been asking too much of my volunteers
myself. What can I do to ease the pressure?

*   You give The Body Potion
    /* MEI LIN: */
    Thank you, I couldn’t do this without all your help.
    
    ~ nextKnot = "MEILIN_INDRA_APP4_RESULT_A"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Intellect Potion
    /* MEI LIN: */
    Thank you, I couldn’t do this without all your help.
    
    ~ nextKnot = "MEILIN_INDRA_APP4_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Tranquility Potion
    /* MEI LIN: */
    Thank you, I couldn’t do this without all your help.
    
    ~ nextKnot = "MEILIN_INDRA_APP4_RESULT_C"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Charisma Potion
    /* MEI LIN: */
    Thank you, I couldn’t do this without all your help.
    
    ~ nextKnot = "MEILIN_INDRA_APP4_RESULT_D"


    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END

/*
[You give The Body Potion: Appointment 1 Result A]
[You give The Intellect Potion: Appointment 1 Result B]
[You give The Tranquility Potion: Appointment 1 Result C]
[You give The Charisma Potion: Appointment 1 Result D]
*/

==MEILIN_INDRA_APP4_RESULT_A==

~ currentKnot = "MEILIN_INDRA_APP4_RESULT_A"

    /* INDRA: */
Conversation was going well at the party, everyone was relaxed. Then the staff put on a demonstration and walked out during service. 

I had to serve the final course myself, though I didn’t do half as well. 

Our guests assure me they haven’t seen it as a slight, and that events like this have been happening in grand homes across the city.


    /* MEI LIN: */
I’ve been so restless. My mind is tired, but my body doesn’t want to stop moving. My wife insists I rest, but I can’t. There is so much to do, I have to pick up the slack when others can’t. 


~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==MEILIN_INDRA_APP4_RESULT_B==

~ currentKnot = "MEILIN_INDRA_APP4_RESULT_B"

    /* INDRA: */
The party didn’t go as well planned despite my thorough organization. I assure you there was no fault on your part.

I worked with my staff to create a new schedule that would allow everyone more time at home, though I had to hire additional personnel.

That didn’t matter, the staff put on a demonstration and walked out during their service. It’s happening in grand homes across the city. 


    /* MEI LIN: */
   I’ve been more creative with my resources to keep from wearing out my volunteers. I recruited the distributors I work with for my flower shop to deliver food around the city. 

~ currentEmotion = "neutral"
~ resultDone = "true"
->END


==MEILIN_INDRA_APP4_RESULT_C==

~ currentKnot = "MEILIN_INDRA_APP4_RESULT_C"

    /* INDRA: */
I failed to attain enough staff for the dinner. I did my best to make sure no one noticed and served dinner myself. 

Zaremir told me he was proud and that our guest spoke in admiration of the confidence with which I handled service. 

Apparently, staff have been holding demonstrations in grand homes across the city. 


    /* MEI LIN: */
I’ve finally been able to rest and I’m more patient with my volunteers. They’ve been working so hard. We’re not going to be able to solve everyone’s problems at once. We’ve already made so much progress. 

~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==MEILIN_INDRA_APP4_RESULT_D==

~ currentKnot = "MEILIN_INDRA_APP4_RESULT_D"

    /* INDRA: */
I did my best to communicate with my staff and come to an agreement. I even offered additional pay for the evening. 

That didn’t matter. In the middle of dinner the staff walked out during their service. Apparently this is happening in grand homes across the city.


    /* MEI LIN: */
We held a gathering for the volunteers and their families. I inspired our volunteers during my address. Everyone was able to see the impact we have made and is eager to continue our work. We were able to work out a system to trade tasks for improved efficiency. 
 
~ currentEmotion = "neutral"
~ resultDone = "true"
->END


==DANTE_POPPY_APP5==

~ currentKnot = "DANTE_POPPY_APP5"
~ appDone = "false"
~ resultDone = "false"

/* [POPPY AND ZINNIA ENTER] */

/* ZINNIA: */
I’m glad to see you’re still open given how extreme the scarcity of supplies has becoming.
If it wasn’t for Mei Lin’s community initiative, this city would have crumbled long before the threat of invasion.
I have something sensitive I’m hoping you can help me with. Please keep this just between you and I. 
It looks like you have another appointment, we’ll resume our conversation once they leave.


/* [DANTE ENTERS] */

/* DANTE: */
You can’t imagine the stress I’m under, so many people are demanding my attention between the Senate and my loyal followers. Everyone has a problem for me to solve.
I hate to tell them that my connection to the fates is… weak, it’s from being surrounded by all the emotional turmoil, I’m sure you’ve been experiencing this yourself. 
Oh, I see you’re with another client. I didn’t mean to intrude, but this is urgent. 


/* POPPY: */
Mom! It’s the Seer from the market! I bet he could tell us how Dad is doing!

/* DANTE: */
Oh, a pair of devoted followers. I know, I know such personal interaction can be overwhelming,
please try to contain yourselves.

/* DANTE: */
Now young lady, tell me how the fates can serve and I will assist you.

/* POPPY: */
My Dad was sent to fight, he used to write us every day, but now the letters have stopped.

/* DANTE: */
Let me contact the fates, this will take only a moment.

/* ZINNIA: */
While they’re distracted, I must confide in you.  City Officials have been coming to the university, I’ve been told they were asking about me. 

It’s not unexpected, but yesterday, one of them showed up at my house. They claimed it was nothing but a wellness check that they were doing for the neighborhood.

I haven’t been able to shake the feeling I’m being followed ever since. 

If anything should happen to me, Poppy will be alone. Please, whatever you can do, please make sure she is safe. 

Do you have anything that could ease my mind? 


/* [ Give Zinnia Potion] */



/* [POPPY AND ZINNIA EXIT] */

/* DANTE: */
I hope their predictions proved to be satisfactory. I don’t need any more people complaining.
A small group are growing concerned, and unfortunately vocal, as more and more of my
predictions are proving to be… less than accurate.
I can’t let my devotees down. Could you help me center myself with the fates once again?

/* [Give Dante Potion] */




~ nextKnot = "DANTE_POPPY_APP5_Con"

->DANTE_POPPY_APP5_Con

==DANTE_POPPY_APP5_Con==

~ currentKnot = "DANTE_POPPY_APP5_Con"

*   You give The Body Potion

    /* ZINNIA: */
    Please, don’t forget what we’ve discussed.
    
    /* DANTE: */
    I believe the fates are saying… we will meet again!
    
    ~ nextKnot = "DANTE_POPPY_APP5_RESULT_A"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Intellect Potion
    
    /* ZINNIA: */
    Please, don’t forget what we’ve discussed.
    
    /* DANTE: */
    I believe the fates are saying… we will meet again!
    
    ~ nextKnot = "DANTE_POPPY_APP5_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Tranquility Potion

    /* ZINNIA: */
    Please, don’t forget what we’ve discussed.
    
    /* DANTE: */
    I believe the fates are saying… we will meet again!
    
    ~ nextKnot = "DANTE_POPPY_APP5_RESULT_C"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Charisma Potion

    /* ZINNIA: */
    Please, don’t forget what we’ve discussed.
    
    /* DANTE: */
    I believe the fates are saying… we will meet again!
    
    ~ nextKnot = "DANTE_POPPY_APP5_RESULT_D"


    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END

/*
[You give The Body Potion: Appointment 1 Result A]
[You give The Intellect Potion: Appointment 1 Result B]
[You give The Tranquility Potion: Appointment 1 Result C]
[You give The Charisma Potion: Appointment 1 Result D]
*/





==DANTE_POPPY_APP5_RESULT_A==

~ currentKnot = "DANTE_POPPY_APP5_RESULT_A"

    /* ZINNIA: */

Zinnia: Things haven’t changed since I last visited. I feel eyes on me everywhere I go.
I’ve started staying up long into the night, constantly watching the street. 

Luckily, I’ve had the stamina to sustain the long hours and still tend to my work the next day.                               

I don’t think Poppy has noticed yet.

Poppy: What haven’t I noticed? Is there a surprise?


    /* DANTE: */

Hello, friend. I am... struggling, in my current post. The Senate asked for my input concerning battle strategies.  
My mind was... blank. I lied to them. I told them it would be a glorious success, a turning point in the war-- all the hyperbole I could think of.
We had a victory, but it’s not without casualties. Casualties which I had a hand in. I'm hounded by constant visions of battle. I haven’t had a moment's rest even with your help.  


~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==DANTE_POPPY_APP5_RESULT_B==

~ currentKnot = "DANTE_POPPY_APP5_RESULT_B"

    /* ZINNIA: */

Zinnia: Things haven’t changed since I last visited. I feel eyes on me everywhere I go.

I’ve become socially withdrawn. I can’t stop thinking that any of my friends could be monitoring my movements. Everyday I feel like I could be taken away.”

This paranoia continues to eat at me while my mind continues to analyze the motive of those around me. I just hope Poppy hasn’t noticed.

Poppy: What haven’t I noticed? Is there a surprise?


    /* DANTE: */

Hello, friend. I am... struggling, in my current post. The Senate asked for my input concerning battle strategies.  
My mind was... blank. I lied to them. I told them it would be a glorious success, a turning point in the war-- all the hyperbole I could think of.
We had a victory, but it’s not without casualties. Casualties which I had a hand in. I'm hounded by constant visions of battle. Even with your help I can’t rationalize my role in these events.   


~ currentEmotion = "neutral"
~ resultDone = "true"
->END


==DANTE_POPPY_APP5_RESULT_C==

~ currentKnot = "DANTE_POPPY_APP5_RESULT_C"

    /* ZINNIA: */

Zinnia: Things haven’t changed since I last visited. I feel eyes on me everywhere I go.

I wait and wait, but no one has approached me. I never thought I could be so patient, not when my family is at stake.

I shouldn’t say too much. I don’t know who is listening.

Poppy: It will be okay Mom! 


    /* DANTE: */

Hello friend. I am... struggling, in my current post. The Senate asked for my input concerning battle strategies.  
My mind was... blank. I lied to them. I told them it would be a glorious success, a turning point in the war-- all the hyperbole I could think of.
We had a victory, but it’s not without casualties. Casualties which I had a hand in. I'm hounded by constant visions of battle. I haven’t had a moment's rest even with your help.  


~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==DANTE_POPPY_APP5_RESULT_D==

~ currentKnot = "DANTE_POPPY_APP5_RESULT_D"

    /* ZINNIA: */
    
Zinnia: Things haven’t changed since I last visited. I feel eyes on me everywhere I go.
    
School has become tense. I was asked to volunteer at a special university wide assembly to support the war effort.

I convinced another faculty member to take my place. She hasn’t been seen since.

I shouldn’t say too much. I don’t know who is listening.

Poppy: It will be okay Mom!


    /* DANTE: */

Hello, friend. I am... struggling, in my current post. The Senate asked for my input concerning battle strategies.  
My mind was... blank. I lied to them. I told them it would be a glorious success, a turning point in the war-- all the hyperbole I could think of.
We had a victory, but it’s not without casualties. Casualties which I had a hand in. I'm hounded by constant visions of battle. Even with your help I can’t talk my way out of this quandary. 

 
~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==POPPY_MEILIN_APP6==

~ currentKnot = "POPPY_MEILIN_APP6"
~ appDone = "false"
~ resultDone = "false"

/* [POPPY ENTERS W/OUT ZINNIA] */

/* POPPY: */
I…

I don’t know where my mom is. I… I came home from school, and someone had gone through our stuff. 

A man came to the house, so I hid.

I… I don’t know where to go. 



/* [MEI LIN ENTERS] */

/* MEI LIN: */
Poppy, there you are! I’ve been looking everywhere for you.

I was asked by your mom’s colleague from the university to help find you. I saw the state of the house and I’ve been worried. 

I’m sorry dear, but your mom isn’t coming home tonight. Don’t worry though. You will stay with me and I’ll take care of you. 

/* [TO PLAYER] */

Is there anything you can give Poppy to help her sleep tonight? 

/* [GIVE POTION TO POPPY] */

Zinna was arrested today at the university. Her students protested, but that only caused them to get arrested too. It’s happening in schools and colleges across the city.

The Senate denies how poorly they are treating their people. They’re scared of anyone who tells them otherwise. 

I’m horrified at what is happening to our children. Both her parents have been taken away.  I have to put on a good front for Poppy. What do you recommend? 

/* [GIVE POTION TO MEI LIN] */



~ nextKnot = "POPPY_MEILIN_APP6_Con"

->POPPY_MEILIN_APP6_Con

==POPPY_MEILIN_APP6_Con==

~ currentKnot = "POPPY_MEILIN_APP6_Con"

*   You give The Body Potion

    Thank You. I’m going to try to get Poppy settled in now. 
    
    ~ nextKnot = "POPPY_MEILIN_APP6_RESULT_A"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Intellect Potion
    
    Thank You. I’m going to try to get Poppy settled in now. 
    
    ~ nextKnot = "POPPY_MEILIN_APP6_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Tranquility Potion

    Thank You. I’m going to try to get Poppy settled in now. 
    
    ~ nextKnot = "POPPY_MEILIN_APP6_RESULT_C"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Charisma Potion

    Thank You. I’m going to try to get Poppy settled in now. 
    
    ~ nextKnot = "POPPY_MEILIN_APP6_RESULT_D"


    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END

/*
[You give The Body Potion: Appointment 1 Result A]
[You give The Intellect Potion: Appointment 1 Result B]
[You give The Tranquility Potion: Appointment 1 Result C]
[You give The Charisma Potion: Appointment 1 Result D]
*/





==POPPY_MEILIN_APP6_RESULT_A==

~ currentKnot = "POPPY_MEILIN_APP6_RESULT_A"

    /* POPPY: */

Hi! Mei Lin is really nice. I’m glad I can stay with her. 

I like to help in the garden. Mei Lin wanted me to take breaks, but I don’t like to. I don’t like not having something to do. 

I hope my mom is okay. 



    /* MEI LIN: */

Nice to see you. 

Poppy hasn’t been sleeping easily. She wakes up with nightmares throughout the night and is reluctant to go to sleep. Thankfully I have enough energy to stay up with her. 

Poppy is such a good kid. I hate to see her go through this. I wish there was more I could do for her. 



~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==POPPY_MEILIN_APP6_RESULT_B==

~ currentKnot = "POPPY_MEILIN_APP6_RESULT_B"

    /* POPPY: */

Hi. I want to know more of what is happening. I keep asking questions, some people don’t know what to say. They tell me to go play. 

I can’t stop thinking. But Mei Lin is really nice and listens to all my questions.

I hope my mom is okay. 



    /* MEI LIN: */

Nice to see you. After the arrest at the university, I determined the Homefront Action Movement needed to prepare to fight against further unjust arrests in our community.

I organized a new watch system to help alert those under suspicion. We can’t let people be afraid of the truth. 

I just wish we had been able to help Zinnia. 



~ currentEmotion = "neutral"
~ resultDone = "true"
->END


==POPPY_MEILIN_APP6_RESULT_C==

~ currentKnot = "POPPY_MEILIN_APP6_RESULT_C"

    /* POPPY: */

Hi. I’ve been really lazy. I feel tired all the time. It’s strange, I don’t even feel like playing with the other kids. 

Mei Lin suggested I work in the garden. It’s okay. I’m glad people get food though.

I hope my mom is okay. 



    /* MEI LIN: */

Nice to see you. Poppy has been anxious, she hasn’t left my side since we’ve taken her in.  I’ve made sure to show her that she always has my time to talk about what is happening. 

I’ve let her know that even though things are different, it's okay to be sad about it, but she’s  safe with us.

I know this was never going to be an easy time for her, but I hope I’ve been able to make it better. 



~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==POPPY_MEILIN_APP6_RESULT_D==

~ currentKnot = "POPPY_MEILIN_APP6_RESULT_D"

    /* POPPY: */
    
Hi! Mei Lin has been really nice to me. I’m glad I get to stay with her. It’s fun to help people. 

I’m going to make sure everyone is helped! Like our neighbors, I take bread to them everyday. 

I hope my mom is okay. 


    /* MEI LIN: */

Nice to see you. Despite speaking with as many of my contacts as I could, I wasn’t able to find anything about Zinna. No amount of convincing could help. No one knew anything. 

I’ve been focusing on making sure Poppy feels at home. I don’t have a lot to keep her entertained, but a few Homefront Action Movement members donated some toys. 

Thankfully Poppy enjoys being around the flowers.


 
~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==INDRA_DANTE_APP7==

~ currentKnot = "INDRA_DANTE_APP7"
~ appDone = "false"
~ resultDone = "false"

// INDRA ENTERS

// INDRA:

Sorry I’m late for my appointment. I accompanied Zaremir to the Senate today, he was making his latest plea. The session ran over due to the intense debate. 

With enemy troops inching closer and closer to the city, this was our last chance to sway the Senate to pursue negotiations with our foes. It was futile. The vote remained divided. 

Now, I’m afraid it’s too late. Our last line of defense was broken just this afternoon. The city could be invaded any day now. 

// DANTE ENTERS 

// DANTE: 

I’m glad you’re still open. I’m having the most difficult time. No matter how I act, I feel like I’m letting someone down. 


// INDRA: 

Dante! You’re just the person who could help! 


// DANTE:

This is quite a shift from being ungraciously asked to leave your dinner party. I mean, I’m sure it was a simple mistake. 

// INDRA:

Those were petty disagreements. None of that matters now. 

We might be able to save the city.  All you need to do is go to the Senate with a new vision. Tell them you see an invasion, tell them you see death, the Senate destroyed.  

They’ll listen to you. 


// DANTE: 

You’re asking me to lie? 

// INDRA: 

It won’t be a lie. If we don’t act now, I’m afraid that’s exactly where our future will lead. 

// DANTE: 

I have a reputation. I won’t betray the trust of those who support me. 

// INDRA: 

You have the reputation as a fraud. You’re capitalizing off of fading glory, doing nothing but preaching from street corners to those too misguided to see your true nature. 

// DANTE: 

I’m afraid I can’t assist you. 

// INDRA: 

Why do I remain the only one who refuses to be content with the ruin of our city? How do I possibly move forward? Do you have a potion that can help me find some peace?

// [Give Potion to Indra] 


// INDRA LEAVES 

// DANTE:

I… I had no idea this is what people think of me. I only intended to provide inspiration and comfort for those who have nowhere to turn. 

I must confess, I know I haven’t always been truthful. 

When I was young, I had a vision, sincerely a real vision. I told my parents, and we were able to save our neighborhood from an impending flood. 

After that, everyone wanted a prediction. I was afraid to let everyone down. I felt pressured.
I can’t continue to compromise my principles. How do I continue to help people? 

// [Give Potion to Dante]




~ nextKnot = "INDRA_DANTE_APP7_Con"

->INDRA_DANTE_APP7_Con

==INDRA_DANTE_APP7_Con==

~ currentKnot = "INDRA_DANTE_APP7_Con"

*   You give The Body Potion

    // INDRA:

    I hope there will still be time to meet again. 
    
    // DANTE: 

    I trust you’ll keep this discussion between us. You’ve been a great confidant. 
    
    ~ nextKnot = "INDRA_DANTE_APP7_RESULT_A"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Intellect Potion
    
    // INDRA:

    I hope there will still be time to meet again.
    
    // DANTE: 

    I trust you’ll keep this discussion between us. You’ve been a great confidant.
    
    ~ nextKnot = "INDRA_DANTE_APP7_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Tranquility Potion

    // INDRA:

    I hope there will still be time to meet again.
    
    // DANTE: 

    I trust you’ll keep this discussion between us. You’ve been a great confidant.
    
    ~ nextKnot = "INDRA_DANTE_APP7_RESULT_C"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Charisma Potion

    // INDRA:

    I hope there will still be time to meet again.
    
    // DANTE: 

    I trust you’ll keep this discussion between us. You’ve been a great confidant.
    
    ~ nextKnot = "INDRA_DANTE_APP7_RESULT_D"


    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END

/*
[You give The Body Potion: Appointment 1 Result A]
[You give The Intellect Potion: Appointment 1 Result B]
[You give The Tranquility Potion: Appointment 1 Result C]
[You give The Charisma Potion: Appointment 1 Result D]
*/





==INDRA_DANTE_APP7_RESULT_A==

~ currentKnot = "INDRA_DANTE_APP7_RESULT_A"

    // INDRA:
    
I’m sorry to report I am not feeling much better. I’m more restless than ever. I do hope we find some way out of this soon.  



    // DANTE:

Hello, friend. I find myself more and more restless in recent days. Hopefully things can begin to calm down soon in spite of recent events. 


~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==INDRA_DANTE_APP7_RESULT_B==

~ currentKnot = "INDRA_DANTE_APP7_RESULT_B"

    // INDRA:

I’m sorry to report that there is not much improvement in our situation. 

No matter how I reason with other members of the senate or their families, they refuse to believe that this could hurt them too. 




    // DANTE:

Hello, friend. I sought out some words of wisdom on my situation in the vaguest way I could, however the responses were conflicting. 

It seems there is not an easy answer to this question. THough I thank you for your potion. 
 


~ currentEmotion = "neutral"
~ resultDone = "true"
->END


==INDRA_DANTE_APP7_RESULT_C==

~ currentKnot = "INDRA_DANTE_APP7_RESULT_C"

    // INDRA:

Thanks to your excellent potion I’ve been able to put the most troubling thoughts from my mind. I hope that all may soon feel the relations 



    // DANTE:

Hello, friend! Your potion has granted me some peace in this difficult time.  I have determined to meditate deeply on this issue. I think spending time to ponder each possibility could work wonders. 

Decided to meditate deeply and try to  avoid action as long as he can. 
 


~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==INDRA_DANTE_APP7_RESULT_D==

~ currentKnot = "INDRA_DANTE_APP7_RESULT_D"

    // INDRA:
    
Hello again. I appreciate your work even at this time. Your potion has helped me to encourage others to seek the end of the war. 

I have been reaching out to the families and spouses of the other Senators, and while they are hesitant, I hope I can get them to listen. 
 


    // DANTE:

Hello, friend! I have been encouraged by your excellent alchemical prowess. 

I began including words of caution in my daily words to my audiences, and a few seemed to embrace them. 

Though I am still afraid of trying to bring my message to the Senate.


 
~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==APP8==

~ currentKnot = "APP8"
~ appDone = "false"
~ resultDone = "false"

//ALL CHARACTERS PRESENT

//DANTE: 

Forgive the late hour. You have an audience awaiting your guidance. There is no time to waste. 

//INDRA: 

It’s just what we all feared. Enemy troops have entered the city. They could be here any minute. Our army is spread too thin. Soon the city will be under their control. 

//MEI LIN:

The Homefront Action Movement has been preparing for this. We aren’t giving up this city without a fight. 

//DANTE:

My followers demand to know why my predictions of victory have failed them.

I have one last chance to explain myself and convince them to leave, but I fear either staying or fleeing could be a death sentence. 

This would mean definitively turning my back on the Senate and the safety they can provide. 

//INDRA:

I assure you; the Senate is no longer capable of promising safety. I already pleaded for their assistance tonight. 

An attempt was made on my husband’s life. An assassin hid in our home. We barely escaped with our lives. 

We asked for the Senate’s protection but they’re thinking of nothing but preserving their expensive treasures. There is nowhere safe for us. 

//MEI LIN: 

Then leave the city. The Homefront Action Movement scouted out viable routes. We’ve already started getting people out as quick as we can. 

I want Poppy to leave the city, but I need to stay here to manage our efforts. If I could entrust her to you my mind would be at ease. 

//POPPY: 

No! I don’t want to leave! I want to stay with you Mei Lin. What if my mom comes back? How will she find me?

//MEI LIN:

Your mother would want what’s best for you. You’re not safe here. 

//INDRA: 

I don’t know if I can convince Zaremir to abandon the city he loves, and I won’t leave without him. 

//MEI LIN:

What about you Dante? What will you do?

//DANTE: 

The best option hasn’t revealed itself to me.

//MEI LIN:

You’re young and fit. The Homefront Action Movement could use you! You could stay and help the people who are unable to flee. 

//DANTE:

I’m unsure what my alliances will allow. 

//MEI LIN:

If you don’t want to stay then help with the evacuations. Spread the word and guide people to safety out of the city. You said yourself it's exactly what people need.

You can redeem yourself.  You can save lives. You can take Poppy and keep her safe!

//DANTE: 

Who would listen to me now? Who’s to say fleeing is the safest path? It's miles in the desert until you reach even the smallest settlement. 

//MEI LIN:

It will take endurance, but so will anything we face tonight.

//[VARIATION OF ENDINGS DEPENDING ON PROGRAMMING: EVERYONE GETS A POTION]

//DANTE:

Please, anything you have that could help us. I need clarity on where my efforts are best placed.

//[Give Potion]

//POPPY:

I want to find my mom again.

//[Give Potion]

//INDRA:

I want to keep Zaremir safe.

//[Give Potion]

//MEI LIN:

I need help to defend our city.

//[Give Potion]

//MEI LIN:

Find safety.

//[VARIATION OF ENDINGS DEPENDING ON PROGRAMMING: ONLY ONE PERSON GETS A POTION]

//DANTE:

Please, anything you have that could help us. I need clarity on where my efforts are best placed.

//POPPY:

I want to find my mom again.

//INDRA:

I want to keep Zaremir safe.

//MEI LIN:

I need help to defend our city.

//[Give Potions]

//MEI LIN:

Find safety.


~ nextKnot = "APP8_Con"

->APP8_Con

==APP8_Con==

~ currentKnot = "APP8_Con"

*   You give The Body Potion

    // INDRA:

    I hope there will still be time to meet again. 
    
    // DANTE: 

    I trust you’ll keep this discussion between us. You’ve been a great confidant. 
    
    ~ nextKnot = "APP8_RESULT_A"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Intellect Potion
    
    // INDRA:

    I hope there will still be time to meet again.
    
    // DANTE: 

    I trust you’ll keep this discussion between us. You’ve been a great confidant.
    
    ~ nextKnot = "APP8_RESULT_B"
    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Tranquility Potion

    // INDRA:

    I hope there will still be time to meet again.
    
    // DANTE: 

    I trust you’ll keep this discussion between us. You’ve been a great confidant.
    
    ~ nextKnot = "APP8_RESULT_C"

    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END
*   You give The Charisma Potion

    // INDRA:

    I hope there will still be time to meet again.
    
    // DANTE: 

    I trust you’ll keep this discussion between us. You’ve been a great confidant.
    
    ~ nextKnot = "APP8_RESULT_D"


    ~ currentEmotion = "neutral"
    ~ appDone = "true"
    ->END

/*
[You give The Body Potion: Appointment 1 Result A]
[You give The Intellect Potion: Appointment 1 Result B]
[You give The Tranquility Potion: Appointment 1 Result C]
[You give The Charisma Potion: Appointment 1 Result D]
*/





==APP8_RESULT_A==

~ currentKnot = "APP8_RESULT_A"

    // INDRA:
    
I’m sorry to report I am not feeling much better. I’m more restless than ever. I do hope we find some way out of this soon.  



    // DANTE:

Hello, friend. I find myself more and more restless in recent days. Hopefully things can begin to calm down soon in spite of recent events. 


~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==APP8_RESULT_B==

~ currentKnot = "APP8_RESULT_B"

    // INDRA:

I’m sorry to report that there is not much improvement in our situation. 

No matter how I reason with other members of the senate or their families, they refuse to believe that this could hurt them too. 




    // DANTE:

Hello, friend. I sought out some words of wisdom on my situation in the vaguest way I could, however the responses were conflicting. 

It seems there is not an easy answer to this question. THough I thank you for your potion. 
 


~ currentEmotion = "neutral"
~ resultDone = "true"
->END


==APP8_RESULT_C==

~ currentKnot = "APP8_RESULT_C"

    // INDRA:

Thanks to your excellent potion I’ve been able to put the most troubling thoughts from my mind. I hope that all may soon feel the relations 



    // DANTE:

Hello, friend! Your potion has granted me some peace in this difficult time.  I have determined to meditate deeply on this issue. I think spending time to ponder each possibility could work wonders. 

Decided to meditate deeply and try to  avoid action as long as he can. 
 


~ currentEmotion = "neutral"
~ resultDone = "true"
->END

==APP8_RESULT_D==

~ currentKnot = "APP8_RESULT_D"

    // INDRA:
    
Hello again. I appreciate your work even at this time. Your potion has helped me to encourage others to seek the end of the war. 

I have been reaching out to the families and spouses of the other Senators, and while they are hesitant, I hope I can get them to listen. 
 


    // DANTE:

Hello, friend! I have been encouraged by your excellent alchemical prowess. 

I began including words of caution in my daily words to my audiences, and a few seemed to embrace them. 

Though I am still afraid of trying to bring my message to the Senate.


 
~ currentEmotion = "neutral"
~ resultDone = "true"
->END